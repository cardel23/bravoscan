package com.peoplewalking.warehouse.tasks;

import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.RetrofitError;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.Utils;

public class LoginTask  extends TaskWithCallback<Void, Void, Boolean>{
	private static final String TAG = "LoadUsersTask";
	private String mUsername;
	private String mPassword;

	public LoginTask(Context context, String username, String password) {
		super(context);
		this.mUsername = username;
		this.mPassword = password;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
//		progress = ProgressDialog.show(this.context, "", app.getString(R.string.loading));
	}
	@Override
	protected Boolean doInBackground(Void... params) {
		
		this.errorMessages = new ArrayList<String>();
		User user = null;
		app.setUser(null);
		app.setObService(null);
		try {
			if(!app.isConnected()){
				List<User> users =  User.find(User.class, "username = ?" , mUsername);
				if(users != null && !users.isEmpty()){
					user = users.get(0);
					if(user.getPassword().equals(mPassword)){
						app.setUser(user);
//						progress.dismiss();
						return true;
					} else {
						errorMessages.add(context.getString(R.string.offline_mode_try_last_pass));
					}			
				}else{
					errorMessages.add(context.getString(R.string.user_not_exists_in_local));
				}			
			}else { 
				try {
					OpenbravoService service = OBServiceClient.initializeService(mUsername, mPassword);
					
					List<User> userList = service.getUserList("username='"+mUsername+"'");				
					if(userList == null || userList.isEmpty()){
						errorMessages.add(context.getString(R.string.error_incorrect_username));
					}else{
						user = userList.get(0);
						String test;
						if(user != null && user.getUsername() != null)
							test = user.getUsername();
						else
							test = mUsername;
						List<User> users =  User.find(User.class, "username = ?" , test);
						if(users != null && !users.isEmpty()){
						   user.setId(users.get(0).getId());
						}

						user.setUsername(test);
						user.setPassword(mPassword);
						user.save();
						app.setUser(user);
						
						app.setStorageBins(service.getStorageBinList("warehouse='"+app.getUser().getDefaultWarehouseId()+"'"));
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("_where", "documentCategory='MMR'");
						map.put("_selectedProperties", "name,printText,documentCategory,organization,id");
						app.setInboundDocList(service.getDocTypeList(map));
						map.put("_where", "documentCategory='MMS'");
						app.setOutboundDocList(service.getDocTypeList(map));
						map.put("_where", "warehouse='"+app.getUser().getDefaultWarehouseId()+"'");
						map.put("_selectedProperties", "id,warehouse,rowX,stackY,levelZ,searchKey");
						map.put("_orderBy", "searchKey");
						app.setStorageBins(service.getStorageBinList(map));
						app.setObService(service);
//						progress.dismiss();
						return true;
					}					
				}  catch (RetrofitError error ) {
					if(error.getResponse() != null && error.getResponse().getStatus() == OpenbravoService.UNAUTHORIZED){
						    errorMessages.add(context.getString(R.string.error_bad_credentials));
					} else if(error.getMessage() != null){
					            errorMessages.add(Utils.subString(error.getMessage(), Constants.TOAST_MESSAGE_LENGHT));
						} else{
						    errorMessages.add(context.getString(R.string.retrofit_error_getting_data));	
						}

					Log.e(TAG, error.getMessage(), error);		
				} catch (Exception e) {
				    if(e.getMessage() != null){
					errorMessages.add(Utils.subString(e.getMessage(), Constants.TOAST_MESSAGE_LENGHT));
				    }else{
					errorMessages.add(context.getString(R.string.unexpected_error));
				    }
				    Log.e(TAG, e.getMessage(), e);	
				} 
				
			}
		} catch (Error e ) {	
		    if(e.getMessage() != null){
			errorMessages.add(Utils.subString(e.getMessage(), Constants.TOAST_MESSAGE_LENGHT));	
		    } else{
			errorMessages.add(context.getString(R.string.unexpected_error));
		    }
		    Log.e(TAG, e.getMessage(), e);
		}
//		progress.dismiss();
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean response) {
		// TODO Auto-generated method stub
		super.onPostExecute(response);
	}

}

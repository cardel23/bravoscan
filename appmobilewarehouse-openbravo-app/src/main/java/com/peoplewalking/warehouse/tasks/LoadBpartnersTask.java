package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RetrofitError;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.util.Constants;

public class LoadBpartnersTask extends TaskWithCallback<Void, Void, List<BusinessPartner>>{
    	private static final String TAG = "LoadBpartnerTask";
    	private Integer startRow;
    	private Integer endRow;
    	private Map<String, Object> filterParams;
    	private ProgressDialog progress;
    	
    
    	
    	
	public LoadBpartnersTask(Context context,Map<String, Object> filterParams, Integer startRow, Integer endRow) {
	    super(context);
	    this.startRow = startRow;
	    this.endRow = endRow;
	    this.filterParams = filterParams;
	    
	}
	
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    progress = ProgressDialog.show(this.context, "", "Cargando...");
	}
	@Override
	protected List<BusinessPartner> doInBackground(Void... params) {
	    	this.errorMessages = new ArrayList<String>();
	    	
		List<BusinessPartner> partnerList = null;
		try {		
		    Integer havingPendingOrders = (Integer) this.filterParams.get(Constants.FILTER_PARTNER_PENDING_ORDERS);
		    Integer partnerCategory = (Integer) this.filterParams.get(Constants.FILTER_PARTNER_CATEGORY);
		    String partnerName = (String) this.filterParams.get(Constants.FILTER_PARTNER_NAME);
		    
		    if(this.filterParams != null){
			partnerName = (String) this.filterParams.get(Constants.FILTER_PARTNER_NAME);
			if(havingPendingOrders == null){
			    havingPendingOrders = Constants.PARTNER_PENDING_ORDER_ANY;
			}
			if(partnerCategory == null){
			    partnerCategory = Constants.PARTNER_CATEGORY_ANY;
			}			
		    }
		        
		    	Map<String, String> parameters = new HashMap<String, String>();
		    	StringBuilder partnersWithPendingOrdersQuery = new StringBuilder();
		    	//Las líneas comentadas se cambiaron porque daban problemas en Preproducción

//		    	partnersWithPendingOrdersQuery.append("as bp where 1=1 ");
		    	partnersWithPendingOrdersQuery.append("1=1");
		    	if(partnerName != null && !"".equals(partnerName)){
//		    	    partnersWithPendingOrdersQuery.append("AND lower(bp.name) like '" + partnerName.toLowerCase() + "%' ");
		    	    partnersWithPendingOrdersQuery.append("AND lower(e.name) like '" + partnerName.toLowerCase() + "%' ");
		    	}
		    	
		    	if(partnerCategory.equals(Constants.PARTNER_CATEGORY_VENDOR)){
//			    		partnersWithPendingOrdersQuery.append("AND bp.vendor = true ");
		    		partnersWithPendingOrdersQuery.append("AND e.vendor = true ");
			    } else if(partnerCategory.equals(Constants.PARTNER_CATEGORY_CLIENT)){
//			    		partnersWithPendingOrdersQuery.append("AND bp.customer = true ");
			    	partnersWithPendingOrdersQuery.append("AND e.customer = true ");
			    }
		    	
		    	if(havingPendingOrders.equals(Constants.PARTNER_PENDING_ORDER_YES)){	
		    	   	
//		    	    partnersWithPendingOrdersQuery.append("AND bp in (")
		    		partnersWithPendingOrdersQuery.append("AND e in (")
        			.append("SELECT distinct bp FROM BusinessPartner bp ")
        			.append("left join bp.orderList o ")
        			.append("left join o.orderLineList ol ")
        			.append("left join ol.procurementPOInvoiceMatchList pom ")
        			.append("left join o.documentType dt ")
        			.append("WHERE o.documentStatus = 'CO' ");
		    	    if(partnerCategory.equals(Constants.PARTNER_CATEGORY_VENDOR)){
		    	    	partnersWithPendingOrdersQuery.append("AND o.salesTransaction = 'N' ");
		    	    } else if(partnerCategory.equals(Constants.PARTNER_CATEGORY_CLIENT)){
		    	    	partnersWithPendingOrdersQuery.append("AND o.salesTransaction = 'Y' ");
		    	    }
		    	    partnersWithPendingOrdersQuery.append("AND dt.return = 'N' ")
        			.append("GROUP BY bp, o, ol, ol.orderedQuantity ")
        			.append("HAVING ol.orderedQuantity > sum(coalesce(pom.quantity,0)))");
		    	}
		    	parameters.put("_where", partnersWithPendingOrdersQuery.toString());
		    	parameters.put("_startRow", String.valueOf(startRow));
		    	parameters.put("_endRow", String.valueOf(endRow));
		    	parameters.put("_selectedProperties", "id,name,description,searchKey,priceList,purchasePricelist");
			partnerList = app.getObService().getBpartnerList(parameters);	
			if(partnerList == null || partnerList.isEmpty()){
			    errorMessages.add(app.getString(R.string.no_partners_found));
			}
		} catch (RetrofitError e) {
		    	errorMessages.add(e.getMessage());
			Log.e(TAG, e.getMessage(), e);
		} catch (Exception e) {
		    	errorMessages.add(app.getString(R.string.unexpected_error));
			Log.e(TAG, e.toString());
		}
		progress.dismiss();
		return partnerList;
	}	
}
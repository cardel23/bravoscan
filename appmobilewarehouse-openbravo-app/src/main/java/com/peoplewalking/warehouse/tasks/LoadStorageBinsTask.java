package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;

import android.content.Context;
import android.os.AsyncTask;


public class LoadStorageBinsTask extends TaskWithCallback<Void, Void, List<StorageBin>> {
	
	private OnPostExecuteListener<List<StorageBin>> onPostExecuteListener;
	private String warehouseId;
	private ArrayList<String> errorMessages;
	
	public LoadStorageBinsTask(Context context, String warehouseId) {
		super(context);
		this.warehouseId = warehouseId;

	}

	
	protected List<StorageBin> doInBackground(Void... params) {

		errorMessages = new ArrayList<String>();
		try {
			queryParams = new HashMap<String, String>();
			queryParams.put("_where", "warehouse='"+warehouseId+"'");
			queryParams.put("_selectedProperties", "id,warehouse,rowX,stackY,levelZ,searchKey");
			List<StorageBin> bins = OBServiceClient.getOBService().getStorageBinList(queryParams);
			return bins;
		} catch (Exception e) {
			errorMessages.add(e.getMessage());
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	protected void onPostExecute(List<StorageBin> bins) {			
		if(this.onPostExecuteListener != null){
		    this.onPostExecuteListener.onPostExecute(bins, errorMessages);
		}
	}
	
	public void setOnPostExecuteListener(
    	OnPostExecuteListener<List<StorageBin>> onPostExecuteListener) {
        this.onPostExecuteListener = onPostExecuteListener;
    } 

}

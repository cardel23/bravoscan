package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

import retrofit.RetrofitError;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.ProductImage;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;

public class LoadProductTask extends TaskWithCallback<String, Void, Product>{
    	private static final String TAG = "LoadProductTask";
	public LoadProductTask(Context context) {
	    super(context);
	}
	@Override
	protected Product doInBackground(String... productBarcodes) {
		app.setLocaleConfig();
	    return executeSynchronous(productBarcodes);
	}
	
	@Override
	protected void onPostExecute(Product resultProduct) {			
		super.onPostExecute(resultProduct);
		if(this.onPostExecuteListener != null){
		    this.onPostExecuteListener.onPostExecute(resultProduct, errorMessages);
		}
	}
	public ArrayList<String> getErrorMessages() {
	    return errorMessages;
	}
	public void setErrorMessages(ArrayList<String> errorMessages) {
	    this.errorMessages = errorMessages;
	}
	
	@Override
	public Product executeSynchronous(String... productBarcodes){
		errorMessages = new ArrayList<String>();
		try {
	    	if(productBarcodes == null || productBarcodes.length == 0){
	    	    throw new Exception(app.getString(R.string.no_barcode_provided));
	    	}
	    	String productBarcode = productBarcodes[0];
	    	
	    	// Search in local DDBB
        	Product product = null;
	    	List<Product> p = Product.find(Product.class, "upcean=?", productBarcode);
	    	
            	if(p.size() > 0){
            	    product =  p.get(0);
            	} 
            	else{
				    // Save product if not exists
				    product = new Product();
				    product.setuPCEAN(productBarcode);
				}
	            	
            	// If app is connected and product is not syncronized with OB,  get product from OB
            	if(app.isConnected() && app.getObService() != null && product.getObId() == null){	
            		query = new StringBuilder("uPCEAN='"+ productBarcode +"'");
            		queryParams = new HashMap<String, String>();
            		queryParams.put("_where", query.toString());
            		queryParams.put("_selectedProperties","id,name,description,uOM,image,uPCEAN,searchKey,storageBin");
            		List<Product> productList = app.getObService().getProductList(queryParams);
    			if(productList == null || productList.isEmpty()){
    				String errorMsg= app.getString(R.string.product_not_found, productBarcode);
    				throw new Exception(errorMsg);
    			} else {
    			    product.updateFields(productList.get(0));
    			    ProductImage obProductImage;
    			    Bitmap productImageBitmap = null;
    			    
    			    // Try get product image from OB
    			    try {
	    				obProductImage = app.getObService().getProductImage(product.getImageId());
	    				productImageBitmap = obProductImage.getBitmapImage();
	    				
	    				//try save the image in local storage
	    				if(productImageBitmap != null){				    
	    				    product.setRelativeLocalImagePath(app.saveImageToInternalSorage(productImageBitmap,
	    						Constants.PRODUCT_IMAGES_DIRNAME, product.getuPCEAN() + ".jpg"));
						}
    			    } 
    			    catch (Exception e) {
	    				errorMessages.add(app.getString(R.string.cant_dowload_product_image));
	    				Log.e(TAG, e.getMessage(), e);
    			    }	
    			}
			}
        	product.save();
        	return product;
		} catch (RetrofitError e) {
			errorMessages.add(e.getMessage());
			Log.e(TAG, e.getMessage(), e);
			return null;
		} catch (Exception e) {
			errorMessages.add(e.getMessage());
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
		
	}
}

package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RetrofitError;
import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.util.Constants;

public class LoadOrdersTask  extends TaskWithCallback<Void, Void, List<OBOrder>>{
    private static final String TAG = "LoadOrdersTask";
    private Map<String, Object> filterParams;

    public LoadOrdersTask(Context context, Map<String, Object> filterParams) {
	super(context);
	this.filterParams = filterParams;
    }

    @Override
    protected List<OBOrder> doInBackground(Void... args ) {
	List<OBOrder> orderList = null;
	this.errorMessages = new ArrayList<String>();
	try {	    	
	    	StringBuilder ordersQuery = new StringBuilder();
	    		ordersQuery.append("e in (")
//    	    	ordersQuery.append("as o where o in (") esta es la línea del problema
            	.append("SELECT distinct o FROM Order o ");
    	   		    	    		
    	  	ordersQuery.append("left join o.orderLineList ol ")
          		.append("left join ol.procurementPOInvoiceMatchList pom ")
            		.append("left join o.documentType dt ")
            		.append("WHERE 1=1 "); 
    	  	
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_TYPE)){
    	  	    if(filterParams.get(Constants.FILTER_ORDER_TYPE).equals(OBOrder.PURCHASE_TYPE)) {
    	  		ordersQuery.append("AND o.salesTransaction = 'N' "); 
		    } else{
        	  	ordersQuery.append("AND o.salesTransaction = 'Y' "); 
    	  	    }
    	  	}    	    		
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_PARTNER)){
    	  	    BusinessPartner selectedPartner = (BusinessPartner) filterParams.get(Constants.FILTER_ORDER_PARTNER);
    	  	    if(selectedPartner != null){
    	  		ordersQuery.append("AND o.businessPartner='" + selectedPartner.getObId() + "' ");
    	  	    }    	  	    
    	    	}    	  	
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_DATE_FROM)){
    	  	    String dateFrom = (String)filterParams.get(Constants.FILTER_ORDER_DATE_FROM);
    	  	    if(dateFrom != null && !"".equals(dateFrom)){
    	  		ordersQuery.append("AND o.creationDate > '" + dateFrom + "' ");
    	  	    }
    	  	}
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_DATE_TO)){
    	  	    String dateTo = (String)filterParams.get(Constants.FILTER_ORDER_DATE_TO);
    	  	    if(dateTo != null && !"".equals(dateTo)){
    	  		ordersQuery.append("AND o.creationDate < '" + dateTo + "' ");
    	  	    }
    	  	}
    	  	
    	    	ordersQuery.append("AND o.documentStatus = 'CO' ")
               		.append("AND dt.return = 'N' ")
               		.append("GROUP BY o, ol,ol.orderedQuantity ")
               		.append("HAVING ol.orderedQuantity > sum(coalesce(pom.quantity,0)))");
    	    	
    	    	Map<String, String> parameters = new HashMap<String, String>();
    	    	parameters.put("_where", ordersQuery.toString());
    	    	
    	        if(filterParams.containsKey(Constants.FILTER_START_ROW)){
    	    		parameters.put("_startRow", String.valueOf((Integer)filterParams.get(Constants.FILTER_START_ROW)));
    	        }
    	        if(filterParams.containsKey(Constants.FILTER_END_ROW)){
    	    		parameters.put("_endRow", String.valueOf((Integer)filterParams.get(Constants.FILTER_END_ROW)));
    	        }	
    	        parameters.put("_selectedProperties", "id,documentNo,processed,summedLineAmount,businessPartner,documentType,currency");
    	    	orderList = app.getObService().getOrderList(parameters);	
    		if(orderList == null || orderList.isEmpty()){
    			this.errorMessages.add(app.getString(R.string.no_orders_found_with_filters));
  			Log.d(TAG, app.getString(R.string.no_orders_found_with_filters));
  		}	
	} catch (RetrofitError e) {
		this.errorMessages.add(app.getString(R.string.retrofit_error_getting_data));
		Log.e(TAG, e.toString());
	} catch (Exception e) {
	        this.errorMessages.add(app.getString(R.string.unexpected_error));
		Log.e(TAG, e.toString());
	}
	return orderList;
    }

}

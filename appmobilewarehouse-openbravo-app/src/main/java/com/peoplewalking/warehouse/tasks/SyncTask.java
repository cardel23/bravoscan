package com.peoplewalking.warehouse.tasks;

import java.util.List;

import retrofit.RetrofitError;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class SyncTask extends TaskWithCallback<Void, Void, Boolean> {
	private static final String TAG = "SyncTask";
	
	public SyncTask(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public SyncTask(Context context, String loadingMessage) {
		super(context, loadingMessage);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			
			User user;
			List<User> userList = app.getObService().getUserList("username='"+app.getUser().getUsername()+"'");				
			if(userList == null || userList.isEmpty()){
				errorMessages.add(context.getString(R.string.error_incorrect_username));
				return false;
			}else{
				user = userList.get(0);
				user.setId(app.getUser().getId());
				user.save();
				app.setUser(user);
				app.updatePreferences();
				Product.deleteAll(Product.class);
//				app.setObService(service);
//				progress.dismiss();
//				return null;
				return true;
			}
			
		}  catch (RetrofitError error ) {
			if(error.getResponse() != null && error.getResponse().getStatus() == OpenbravoService.UNAUTHORIZED){
				    errorMessages.add(context.getString(R.string.error_bad_credentials));
			} else 
				if(error.getMessage() != null){
			        errorMessages.add(Utils.subString(error.getMessage(), Constants.TOAST_MESSAGE_LENGHT));
				} else{
				    errorMessages.add(context.getString(R.string.retrofit_error_getting_data));	
				}

			Log.e(TAG, error.getMessage(), error);
			return false;
		} catch (Exception e) {
		    if(e.getMessage() != null){
			errorMessages.add(Utils.subString(e.getMessage(), Constants.TOAST_MESSAGE_LENGHT));
		    }else{
			errorMessages.add(context.getString(R.string.unexpected_error));
		    }
		    Log.e(TAG, e.getMessage(), e);	
		    return false;
		} 
		
	}

}

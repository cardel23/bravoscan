package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartnerLocation;

public class LoadBpartnerLocationsTask extends TaskWithCallback<String, Void, List<BusinessPartnerLocation>>{
	private List<BusinessPartnerLocation> bParnerLocationList;
	private static final String TAG = "LoadBpartnerLocationsTask";
	
	public LoadBpartnerLocationsTask(Context context) {
	    super(context);
	}
	@Override
	protected List<BusinessPartnerLocation> doInBackground(String... bPartnerIds) {
	    	errorMessages = new ArrayList<String>();
		try {
			if(bPartnerIds != null){
				bParnerLocationList = app.getObService().getBPartnerLocationList("businessPartner = '" + bPartnerIds[0] + "'");
				if(bParnerLocationList == null || bParnerLocationList.isEmpty()){
				         String errorMessage = context.getString(R.string.no_bpartner_location_found,bPartnerIds[0]);
					errorMessages.add(errorMessage);
					Log.d(TAG, errorMessage);
				}
			}
		} catch (RetrofitError e) {
			errorMessages.add(context.getString(R.string.retrofit_error_getting_data));
			Log.e(TAG, e.toString(),e);
		} catch (Exception e) {
			errorMessages.add(context.getString(R.string.unexpected_error));
			Log.e(TAG, e.toString(),e);
		}
		return bParnerLocationList;
	}	
}
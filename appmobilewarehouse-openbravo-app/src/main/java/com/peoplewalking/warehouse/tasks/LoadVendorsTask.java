package com.peoplewalking.warehouse.tasks;

import java.util.HashMap;
import java.util.List;

import retrofit.RetrofitError;
import android.content.Context;

import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.util.NovoventApp;

/**
 * Tarea asíncrona para cargar la lista de Terceros que son vendedores
 * @author Carlos Delgadillo
 * fecha: 29/07/2015
 *
 */
public class LoadVendorsTask extends TaskWithCallback<Void, Void, List<BusinessPartner>> {

	public LoadVendorsTask(Context context) {
		super(context);
		app = (NovoventApp) context.getApplicationContext();
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	@Override
	protected List<BusinessPartner> doInBackground(Void... params) {
		// TODO Auto-generated method stub
		return executeSynchronous(params);
	}
	
	@Override
	protected List<BusinessPartner> executeSynchronous(Void... params) {
		try {HashMap<String,String> queryParams = new HashMap<String, String>();
			
			query = new StringBuilder("organization='"+app.getUser().getOrganizationId()+"' AND vendor=true");
			//Optimización de resultados en la consulta
			queryParams.put("_where", query.toString());
			queryParams.put("_selectedProperties","id,name,searchKey,purchasePricelist");
			queryParams.put("_sortBy","name");
			List<BusinessPartner> vendorsList = OBServiceClient.getOBService().getBpartnerList(queryParams);
			app.setVendors(vendorsList);
			return vendorsList;
		} catch (RetrofitError e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}

	}
	
	@Override
	protected void onPostExecute(List<BusinessPartner> response) {
		// TODO Auto-generated method stub
		super.onPostExecute(response);
	}


}

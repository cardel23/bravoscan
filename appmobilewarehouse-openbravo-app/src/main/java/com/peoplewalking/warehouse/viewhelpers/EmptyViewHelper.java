package com.peoplewalking.warehouse.viewhelpers;

import com.peoplewalking.warehouse.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EmptyViewHelper extends ViewHelper{
    private static final String TAG = "EmptyViewHelper";
    LinearLayout emptyLayout;
    View container;
    ViewGroup parentContainer;
    TextView mainText;
    TextView secondaryText;
	
    public EmptyViewHelper(Context context, View viewToHide) {
	super(context);
	try {
	    this.container = viewToHide;
	    this.emptyLayout = (LinearLayout) LayoutInflater.from(this.context).inflate(R.layout.template_empty_content, null);
	    mainText = (TextView) emptyLayout.findViewById(R.id.tv_main_text);
	    secondaryText = (TextView) emptyLayout.findViewById(R.id.tv_secondary_text);
	    
	    this.parentContainer = (ViewGroup) this.container.getParent();
	    if(parentContainer != null){
		 this.parentContainer.addView(this.emptyLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	    }
	   
	} catch (Exception e) {
	    Toast.makeText(context, "Error while loading layout", Toast.LENGTH_LONG).show();
	    Log.e(TAG, e.getMessage(), e);
	    e.printStackTrace();
	}
    }
    
    public void setMessages(String mainMessage, String secondaryMessage){
	mainText.setText(mainMessage);
	secondaryText.setText(secondaryMessage);
    }

    public void showEmptyView(final boolean show) {	    
	if(show){
	    this.replaceView(container, emptyLayout);
	} else{
	    this.replaceView(emptyLayout, container);
	}
    }	

}

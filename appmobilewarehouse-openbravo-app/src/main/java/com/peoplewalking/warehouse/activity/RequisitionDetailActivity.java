package com.peoplewalking.warehouse.activity;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.adapters.RequisitionLineAdapter;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.RequisitionLine;
import com.peoplewalking.warehouse.tasks.TaskWithCallback;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RequisitionDetailActivity extends ListActivity implements OnClickListener {

	private ListView requisitionLineList;
	private Button button1, button2;
	private TextView text;
	private NovoventApp app;
	private Requisition requisition;
	private List<RequisitionLine> list;
	RequisitionLineAdapter adapter;
	MenuItem mMenuItemEdit;
	protected int selectedListIndex;
	private static final boolean ON = true;
    private static final boolean OFF = false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_requisition_detail);
		if(app == null)
			app = (NovoventApp) getApplicationContext();
		setTitle(app.getCurrentRequisition().toString());
		requisitionLineList = (ListView) findViewById(android.R.id.list);
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		text = (TextView) findViewById(R.id.textView1);
		initRequisitionDetail();
		button1.setOnClickListener(this);
		button2.setOnClickListener(this);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	private void initRequisitionDetail(){
		try {
			requisition = app.getCurrentRequisition();
			if(requisition.getRequisitionLines() == null){
				list = RequisitionLine.find(RequisitionLine.class, "requisition_id = ?", String.valueOf(requisition.getId()));
				requisition.setRequisitionLines(list);
				
			}
			if(requisition.getRequisitionLines().size() > 0)
				toggleList(ON);
			else
				toggleList(OFF);
			adapter = new RequisitionLineAdapter(app, requisition.getRequisitionLines());
			requisitionLineList.setAdapter(adapter);
			
			requisitionLineList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			requisitionLineList.setOnItemClickListener(new OnItemClickListener() {

				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					
				}
			});
			requisitionLineList.setMultiChoiceModeListener(new MultiChoiceModeListener() {
				private int nr = 0;
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					return false;
				}
				
				public void onDestroyActionMode(ActionMode mode) {
					adapter.removeSelection();
					
				}
				
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					nr = 0;
					MenuInflater inflater = getMenuInflater();
					inflater.inflate(R.menu.action_list_menu, menu);
					menu.findItem(R.id.action_delete).setTitle(R.string.delete);
//					mMenuItemEdit = (MenuItem) menu.findItem(R.id.action_edit);
//					mMenuItemEdit.setVisible(false);
					return true;
				}
				
				public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
					switch (item.getItemId()) {
					case R.id.action_delete:
					SparseBooleanArray selected = adapter.getSelectedIds();
            		               		   for(int i = selected.size()-1;i>=0;i--){
            		               			   if(selected.valueAt(i)){
            		               				   RequisitionLine line = adapter.getItem(selected.keyAt(i));
//            		               				   for(DeliveryListItem deliveryListItem : DeliveryListItem.find(
//            		               						   DeliveryListItem.class,"list_id = ?",String.valueOf(
//            		               								   line.getId()))){
//            		               					   deliveryListItem.delete();
//            		               				   }
            		               				   line.delete();
            		               				   adapter.remove(line);
            		               			   }
            		               		   }
	               		   		mode.finish();
	               		   		if(adapter.getCount() == 0)
	               		   			toggleList(OFF);
	               		   	   return true;
					case R.id.action_edit:
					    mode.finish();
//			               	    onItemEdit(selectedListIndex);               	       
			               	    return true;
					default:
					    return false;
					}
				}
				
				public void onItemCheckedStateChanged(ActionMode mode, int position,
						long id, boolean checked) {
				   if (checked) {
    		                       nr++;
    		                       adapter.toggleSelection(position);                   
    		                   } else {
    		                       nr--;
    		                       adapter.toggleSelection(position);                
    		                   }
    		                   mode.setTitle(getString(R.string.selected, nr));
    		                   selectedListIndex = position;
//    		                   if(nr == 1){
//    		                       mMenuItemEdit.setVisible(false);
//    		                   } else{
//    		                       mMenuItemEdit.setVisible(false);
//    		                   }
    				}
			});
	        
			requisitionLineList.setLongClickable(true);
		} catch (Exception e) {
			// TODO: handle exception
			toggleList(OFF);
		}
	}
	
	private void toggleList(boolean on){
		if(!on){
			requisitionLineList.setVisibility(View.GONE);
			text.setVisibility(View.VISIBLE);
			text.setText(getString(R.string.no_products_in_delivery_list, requisition.toString()));
			button2.setEnabled(false);
			button2.setClickable(false);
			button2.setAlpha(new Float(0.5));
		}
		else {
			requisitionLineList.setVisibility(View.VISIBLE);
			text.setVisibility(View.GONE);
			text.setText(getString(R.string.no_products_in_delivery_list, requisition.toString()));
			button2.setEnabled(true);
			button2.setClickable(true);
			button2.setAlpha(new Float(1));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.requisition_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			Intent intent = new Intent(this, NewRequisitionLineActivity.class);
			startActivity(intent);
			break;
		case R.id.button2:
			SaveRequisitionTask saveTask = new SaveRequisitionTask(RequisitionDetailActivity.this);
			saveTask.execute(requisition);
			break;
		default:
			break;
		}
	}
	
	private class SaveRequisitionTask extends TaskWithCallback<Requisition, Void, Boolean>{
		
		private ArrayList<String> errorMessages = new ArrayList<String>();
		private String errorMessage;
		private String TAG = "SaveRequisitionTask";
		private Requisition current;
		
		public SaveRequisitionTask(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		
		public SaveRequisitionTask(Context context, String loadingMessage) {
			// TODO Auto-generated constructor stub
			super(context, loadingMessage);
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
		@Override
		public Boolean executeSynchronous(Requisition... params) {
			// TODO Auto-generated method stub
			try {
				Requisition requisition = params[0];
				List<Requisition> response = OBServiceClient.getOBService().saveRequisition(requisition);
				if(response == null || response.size() == 0)
					throw new Exception();
				current = response.get(0);
				int count = 1;
				for(RequisitionLine line : requisition.getRequisitionLines()){
					line.setRequisition(current.getObId());
					line.setLineNo(count);
					count ++;
				}
				List<RequisitionLine> listResponse = OBServiceClient.getOBService().saveRequisitionLines(requisition.getRequisitionLines());
				if(listResponse == null || listResponse.size() == 0)
					throw new Exception();
				return true;
				
			}
			
			catch (RetrofitError e) {
				errorMessage = getString(R.string.retrofit_error_getting_data);
				errorMessages.add(errorMessage);
				Log.e(TAG, e.getMessage(), e);
				return false;
			} 
			
			catch (Exception e) {
				errorMessage = getString(R.string.unexpected_error);
				errorMessages.add(errorMessage);
				Log.e(TAG, e.getMessage(), e);
				return false;
			}
		}

		@Override
		protected Boolean doInBackground(Requisition... params) {
			// TODO Auto-generated method stub
			return executeSynchronous(params);
		}
		
		@Override
		protected void onPostExecute(Boolean response) {
			// TODO Auto-generated method stub
			super.onPostExecute(response);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RequisitionDetailActivity.this);

			if(!errorMessages.isEmpty()){
				String[] errors = errorMessages.toArray(new String[errorMessages.size()]);
				alertDialogBuilder.setTitle(getString(R.string.operation_failed))
						  .setItems(errors, null)
						  .setCancelable(true)
						  .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
			} else{
				try {
					app.getCurrentRequisition().delete();
				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
					Toast.makeText(RequisitionDetailActivity.this, "Error deleting list", Toast.LENGTH_LONG).show();
				}
			    	
				app.setCurrentRequisition(null);
//			    app.setDeliveryList(null);
        		alertDialogBuilder.setCancelable(false)
        					.setTitle(getString(R.string.operation_success));
        					if(current.getDocumentNo()!=null && !current.getDocumentNo().equals(""))
        						alertDialogBuilder.setMessage(getString(R.string.save_goodreceipt_success, current.getDocumentNo()));
        					else
        						alertDialogBuilder.setMessage(getString(R.string.operation_success));
        					alertDialogBuilder.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
        						public void onClick(DialogInterface dialog, int which) {
        							navigateUpTo(app.getHomeIntent());
        						}
        					});
									
			}
			alertDialogBuilder.create().show();
		}
		
	}
	
}

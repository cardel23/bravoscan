package com.peoplewalking.warehouse.activity.fragment;

import java.util.ArrayList;
import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.NewRequisitionActivity;
import com.peoplewalking.warehouse.activity.RequisitionDetailActivity;
import com.peoplewalking.warehouse.openbravo.adapters.RequisitionAdapter;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.RequisitionLine;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class RequisitionListFragment extends PlaceholderFragment {
	
	private ListView itemsList;
	private TextView textView;
	private Button newRequisitionButton;
	private List<Requisition> requisitionList;
	private RequisitionAdapter requisitionAdapter;
	private MenuItem mMenuItemEdit;

	public RequisitionListFragment() {
		// TODO Auto-generated constructor stub
		this.LAYOUT_TEMPLATE = R.layout.activity_new_requisition;
	}

	public RequisitionListFragment(int layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.rootView = super.onCreateView(inflater, container, savedInstanceState);
//		initListDisplayActivity();
		initRequisitionList();
		return rootView;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(data != null){
	    	Requisition requisition = (Requisition) data.getSerializableExtra(Constants.ID_EXTRA_NEW_REQUISITION);
	    	if(requisitionList == null)
	    		requisitionList = new ArrayList<Requisition>();
    		requisitionList.add(requisition);
	    	if(requisitionAdapter != null)
	    		requisitionAdapter.notifyDataSetChanged();
	    	app.setCurrentRequisition(requisition);
	    	app.setCurrentBusinessPartner(requisition.getPartnerRef());
	    	openRequisitionList();
    	}
	}
	
	private void initRequisitionList(){
		if(app == null)
			app = (NovoventApp)getActivity().getApplicationContext();
		newRequisitionButton = (Button)rootView.findViewById(R.id.button1);
		itemsList = (ListView)rootView.findViewById(android.R.id.list);
		textView = (TextView)rootView.findViewById(R.id.textView1);
		
		try {
			
			requisitionList = Requisition.find(Requisition.class, "requester = ?", new String[]{String.valueOf(app.getUser().getObId())}, null, "sugarid DESC", null);
			if(requisitionList.size() == 0){
				toggleList(OFF);
			}
			else{
				toggleList(ON);
				itemsList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
				itemsList.setLongClickable(true);

				itemsList.setMultiChoiceModeListener(new MultiChoiceModeListener() {
					private int nr = 0;
					public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
						return false;
					}
					
					public void onDestroyActionMode(ActionMode mode) {
						requisitionAdapter.removeSelection();
						
					}
					
					public boolean onCreateActionMode(ActionMode mode, Menu menu) {
						nr = 0;
						MenuInflater inflater = getActivity().getMenuInflater();
						inflater.inflate(R.menu.action_list_menu, menu);
						menu.findItem(R.id.action_delete).setTitle(R.string.delete);
						mMenuItemEdit = (MenuItem) menu.findItem(R.id.action_edit);
						mMenuItemEdit.setVisible(false);
						return true;
					}
					
					public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
						switch (item.getItemId()) {
						case R.id.action_delete:
						SparseBooleanArray selected = requisitionAdapter.getSelectedIds();
               		   	for(int i = selected.size()-1;i>=0;i--){
               		   		if(selected.valueAt(i)){
               		   			Requisition requisition = requisitionAdapter.getItem(selected.keyAt(i));
               		   			for(RequisitionLine requisitionLine : RequisitionLine.find(
               		   					RequisitionLine.class,"requisition_id = ?",String.valueOf(
               								   requisition.getId()))){
               		   				requisitionLine.delete();
               		   			}
               		   			requisition.delete();
               		   			requisitionAdapter.remove(requisition);
               		   		}
               		   	}
               		   	if(requisitionAdapter.getCount() == 0)
               		   		toggleList(OFF);
           		   		mode.finish();
           		   		return true;
						case R.id.action_edit:
						    mode.finish();
//				               	    onItemEdit(selectedListIndex);               	       
				               	    return true;
						default:
						    return false;
						}
					}
					
					public void onItemCheckedStateChanged(ActionMode mode, int position,
							long id, boolean checked) {
					   if (checked) {
        		                       nr++;
        		                       requisitionAdapter.toggleSelection(position);                   
        		                   } else {
        		                       nr--;
        		                       requisitionAdapter.toggleSelection(position);                
        		                   }
        		                   mode.setTitle(getString(R.string.selected, nr));
        		                   selectedListIndex = position;
//        		                   if(nr == 1){
//        		                       mMenuItemEdit.setVisible(false);
//        		                   } else{
//        		                       mMenuItemEdit.setVisible(false);
//        		                   }
        				}
				});
 		        
				requisitionAdapter = new RequisitionAdapter(app, requisitionList);
				setListAdapter(requisitionAdapter);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		newRequisitionButton.setOnClickListener(this);
	}

	@Override
	protected void toggleList(boolean on) {
		if(!on){
			textView.setVisibility(View.VISIBLE);
			itemsList.setVisibility(View.GONE);
		} else if (on){
			textView.setVisibility(View.GONE);
			itemsList.setVisibility(View.VISIBLE);
		}
	}
	private void openRequisitionList(){
		Intent intent = new Intent(this.ctx, RequisitionDetailActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.button1){
			Intent intent = new Intent(app, NewRequisitionActivity.class);
//			startActivity(intent);
			startActivityForResult(intent, Constants.REQUEST_CODE_REQUISITION);
		}
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Requisition currentRequisition = requisitionAdapter.getItem(position);
		app.setCurrentRequisition(currentRequisition);
		app.setCurrentBusinessPartner(currentRequisition.getPartnerRef());
		openRequisitionList();
	}

}

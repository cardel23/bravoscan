package com.peoplewalking.warehouse.activity.fragment;

import java.util.Locale;

import com.peoplewalking.warehouse.R;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

/**
 * 
 * @author Carlos Delgadillo
 * fecha 17/04/2015
 *
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

	public SectionsPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	public static final int[] sections = {R.string.title_section1, R.string.title_section2, R.string.title_section3};

	
	public Fragment getItem(int fragment) {
		Fragment f;
		int layout;
		switch (fragment) {
		case 0:
			layout = R.layout.delivery_list_fragment;
			return new InOutListFragment(layout);
		case 1:
			layout = R.layout.requisition_list_fragment;
			return new RequisitionListFragment(layout);
		default:
			return new PlaceholderFragment();
		}
	}
	
//	@Override
//	public CharSequence getPageTitle(int position) {
//		// TODO Auto-generated method stub
//		Locale l = Locale.getDefault();
//		switch (position) {
//		case 0:
//			return getString(R.string.title_section1).toUpperCase(l);
//		case 1:
//			return getString(R.string.title_section2).toUpperCase(l);
//		case 2:
//			return getString(R.string.title_section3).toUpperCase(l);
//		}
////		return super.getPageTitle(position);
//	}

	@Override
	public int getCount() {
		return 2;
	}	
}

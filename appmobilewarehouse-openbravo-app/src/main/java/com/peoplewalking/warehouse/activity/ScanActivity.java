package com.peoplewalking.warehouse.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.RetrofitError;

import com.google.zxing.integration.Android.IntentIntegrator;
import com.google.zxing.integration.Android.IntentResult;
import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.StorageBinAdapter;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.ProductImage;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.openbravo.model.StorageDetail;
import com.peoplewalking.warehouse.tasks.LoadProductTask;
import com.peoplewalking.warehouse.tasks.LoadStorageBinsTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.NumberPickerBuilder;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Carlos Delgadillo
 * fecha 13/04/2015
 *
 */
public class ScanActivity extends Activity implements OnClickListener,  OnItemClickListener {
    public static final String TAG = "ScanActivity";
    private Button addBtn;
    private TextView title, subtitle, barcode, amountText;
    private ImageView mProductImage; 
    private NovoventApp app;
    private Product myProduct;
    private NumberPicker numberPicker;
    private LoadingViewHelper loadingHelper;
    private LinearLayout mainContainer;
    private Spinner spinner;
    private EditText movement;
    private StorageBin bin;
    Thread timeoutThread;
    
    private int type = 1;
	protected ActionMode mActionMode;
	    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
    	app = (NovoventApp)getApplicationContext();
		app.setLocaleConfig();
    	super.onCreate(savedInstanceState);
    	type = app.getDeliveryList().getListType();
    	if(type != DeliveryList.INVENTORY){ 
	    	setContentView(R.layout.activity_scan_2);
	    	setTitle(R.string.title_activity_scan);
	    	title = (TextView) findViewById(R.id.textItem3);
	        subtitle = (TextView) findViewById(R.id.textView2);
	        barcode = (TextView) findViewById(R.id.TextView01);
	        amountText = (TextView) findViewById(R.id.textView4);
	    	addBtn = (Button) findViewById(R.id.addProduct_button);
	    	mProductImage = (ImageView) findViewById(R.id.product_image);
	    	mainContainer = (LinearLayout) findViewById(R.id.main_container);
	    	loadingHelper = new LoadingViewHelper(this, mainContainer);
	    	NumberPickerBuilder numberPickerBuilder = new NumberPickerBuilder(findViewById(R.id.numberPicker1));
			addBtn.setOnClickListener(this);
			title.setText(getString(R.string.waiting_product_info));
			amountText.setText(getString(R.string.amount));
			addBtn.setText(getString(R.string.add_to_list));
			
	        numberPickerBuilder.setmMin(0);
	        numberPickerBuilder.setmMax(2147483647);
	        numberPickerBuilder.setmDefault(1);
	        numberPicker = numberPickerBuilder.create();
	        
	        spinner = (Spinner) findViewById(R.id.spinner1);
	        spinner.setPopupBackgroundResource(R.drawable.spinner);
	        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					bin = (StorageBin) parent.getItemAtPosition(position);
					myProduct.setStorageBin(bin.getObId());
					myProduct.setStorageBinIdentifier(bin.getIdentifier());
					
				}
	
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
    	else{
    		setContentView(R.layout.activity_scan_1);
	    	setTitle(R.string.title_activity_scan);
	    	title = (TextView) findViewById(R.id.textItem3);
	        subtitle = (TextView) findViewById(R.id.textView2);
	        barcode = (TextView) findViewById(R.id.TextView01);
	        amountText = (TextView) findViewById(R.id.textView4);
	    	final Button addBtn = (Button) findViewById(R.id.addProduct_button);
	    	mProductImage = (ImageView) findViewById(R.id.product_image);
	    	mainContainer = (LinearLayout) findViewById(R.id.main_container);
	    	loadingHelper = new LoadingViewHelper(this, mainContainer);
	    	movement = (EditText) findViewById(R.id.editText1);
			addBtn.setOnClickListener(this);
			title.setText(getString(R.string.waiting_product_info));
			amountText.setText(getString(R.string.amount));
			addBtn.setText(getString(R.string.add_to_list));
	        
	        spinner = (Spinner) findViewById(R.id.spinner1);
	        spinner.setPopupBackgroundResource(R.drawable.spinner);
	        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
	
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					bin = (StorageBin) parent.getItemAtPosition(position);					
				}
	
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
			});
    	}
        if(getIntent().hasExtra("barcode")){
        	final String scanContent = getIntent().getStringExtra("barcode");
        	searchResults(scanContent);
        }
        else{
        	IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        	scanIntegrator.initiateScan();
        }
    }

    public void onClick(View v) {
        if(v.getId() == R.id.addProduct_button){
        	try {
        		Integer receivedAmount;
        		if(type == DeliveryList.INVENTORY)
        			receivedAmount = Integer.valueOf(movement.getText().toString());
        		else
        			receivedAmount = Integer.valueOf(numberPicker.getValue());
        	    DeliveryListItem deliveryListItem = app.getDeliveryList().getListItemByUpcean(myProduct.getuPCEAN(), myProduct.getStorageBin());
        	    if(bin.getObId() == null)
        	    	throw new Exception(getString(R.string.bin_is_null));
        	    if(deliveryListItem == null ){
            	    deliveryListItem = new DeliveryListItem(myProduct);
            		deliveryListItem.setListId(app.getDeliveryList());
            		deliveryListItem.setReceivedAmount(receivedAmount);
            		deliveryListItem.setBin(bin.getObId());
            		deliveryListItem.setBinId(bin.getIdentifier());
            		deliveryListItem.save(); 
            		app.getDeliveryList().getProducts().add(deliveryListItem);
            		app.getDeliveryList().save();
    		    }else{    			
    		    	deliveryListItem.setReceivedAmount(deliveryListItem.getReceivedAmount() + receivedAmount);
    		    	deliveryListItem.save(); 
    		    }
    		    navigateUpTo(getParentActivityIntent());
    		    finish();
		} catch (Exception e) {
		    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
		    Log.e(TAG, e.getMessage(), e);
		    e.printStackTrace();
		}
        }
    }
    
    @Override
	public boolean onNavigateUp() {
		// TODO Auto-generated method stub
		return super.onNavigateUp();
	}
    
    private void searchResults(final String scanContent){
    	try {
        	final LoadProductTask loadProductTask = new LoadProductTask(this);
        	loadProductTask.setOnPostExecuteListener(new OnPostExecuteListener<Product>() {
        		
			    public void onPostExecute(Product productFound, List<String> errorMessages) {
					loadingHelper.showProgress(false);
						if(productFound == null){
							 Toast.makeText(app, getString(R.string.product_not_found, scanContent), Toast.LENGTH_SHORT).show();
							 finish();
						}else{
						    myProduct = productFound;
						    StorageBinAdapter adapter = new StorageBinAdapter(app, app.getStorageBins());
						    String productBin = myProduct.getStorageBinIdentifier();
						    int position = adapter.getPosition(productBin);
							spinner.setAdapter(adapter);
							spinner.setSelection(position);
						    showProductInfo(myProduct);
						}
						
						if(errorMessages != null && !errorMessages.isEmpty()){
						    for(String errorMsg : errorMessages){
						    	if(!errorMsg.trim().equals("")){
						    		Toast.makeText(app, errorMsg, Toast.LENGTH_SHORT).show();
						    	}
						    }
						}
				    }
				});

        	loadProductTask.execute(scanContent);	        
	        
		} catch (Exception e) {
		    Toast.makeText(app,
	            e.getMessage(), Toast.LENGTH_SHORT).show();
		    e.printStackTrace();
		}
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {     
    	Toast toast;
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        
        if(scanningResult.getContents() != null){
            final String scanContent = scanningResult.getContents();
//            NovoventApp app = (NovoventApp)getApplicationContext();
            searchResults(scanContent);
        }
        else{
        	app.setLocaleConfig();
        	finish();
            toast = Toast.makeText(app, getString(R.string.no_scan_data), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
        
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.scan, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	
    private void showProductInfo(Product myProduct) {
    	if(myProduct != null){
		String productTitle = myProduct.getTitle() != null ? myProduct.getTitle() : app.getString(R.string.not_available);
		String productDescription= myProduct.getDescription() != null ? myProduct.getDescription() : app.getString(R.string.not_available);
	    	title.setText(productTitle);
	    	subtitle.setText(productDescription);
	    	barcode.setText(getString(R.string.product_barcode, myProduct.getuPCEAN()));
	    	mProductImage.setImageBitmap(app.loadImageFromInternalStorage(myProduct.getRelativeLocalImagePath()));
    	}
    }
    
}

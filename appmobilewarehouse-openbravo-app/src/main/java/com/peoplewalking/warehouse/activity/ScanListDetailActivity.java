package com.peoplewalking.warehouse.activity;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.R.layout;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.ProductListAdapter;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.tasks.LoginTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.NumberPickerBuilder;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;

public class ScanListDetailActivity extends ListActivity implements OnClickListener,  OnPostExecuteListener<Boolean>{
	private Button mBtnScan, mBntGenReceipt;
    private ListView codeList;
    private TextView mViewNoProducts;
    private ProductListAdapter productListAdapter;
    private NovoventApp app;
    private List<DeliveryListItem> productList;
    private MenuItem mMenuItemEdit;
    private int selectedProductIndex;
    private LinearLayout mainContainer;
    private LoadingViewHelper loadingViewHelper;
    private static final boolean ON = true;
    private static final boolean OFF = false;
    LoginTask mAuthTask;
    
	protected ActionMode mActionMode;
	    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
    	app = (NovoventApp)getApplicationContext();
    	app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_detail);
		setTitle(R.string.products);
		initActivity();
    }
        
    @Override
    public void onContentChanged() {
    	if(productListAdapter != null)
    		productListAdapter.notifyDataSetChanged();
    }
    
    private void initActivity(){
    	if(app==null){
        	app = (NovoventApp)getApplicationContext();
        }
    	mBtnScan = (Button) findViewById(R.id.btn_scan);
    	mBntGenReceipt = (Button)findViewById(R.id.btn_gen_goodreceipt);
        codeList = (ListView) findViewById(R.id.scan_list);
        mViewNoProducts = (TextView) findViewById(R.id.tv_no_products);
        mainContainer = (LinearLayout) findViewById(R.id.main_container);
        loadingViewHelper = new LoadingViewHelper(this, mainContainer);
        displayList();
        buildLayout();
    }
    
    /**
     * Controla el flujo de la lista de productos, en caso de que se produzcan excepciones
     * @author Carlos Delgadillo
     * fecha 20/05/2015
     */
    private void displayList(){
    	try {
        	productList = DeliveryListItem.find(DeliveryListItem.class,"list_id = ?",String.valueOf(app.getDeliveryList().getId()));
        	app.getDeliveryList().setProducts(productList);
        	displayItems(productList);
		} catch (Exception e) {
			displayItems(productList);
			Toast.makeText(app, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
        
    }
    
    /**
     * Construye el layout de la actividad
     */
    private void buildLayout(){
    	mBtnScan.setOnClickListener(this);
        mBntGenReceipt.setOnClickListener(this);
        mBtnScan.setText(R.string.scan);
        if(app.getDeliveryList().getListType() == DeliveryList.INVENTORY)
        	mBntGenReceipt.setText(getString(R.string.title_activity_new_inventory));
        else
        	mBntGenReceipt.setText(R.string.title_activity_register_receipt);
        codeList.setLongClickable(true);
        codeList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL); //Prueba de elementos de selección multiple en la lista
        codeList.setOnItemLongClickListener(new OnItemLongClickListener() {
        	public boolean onItemLongClick (AdapterView<?> parent, View view, int position, long id) {
        	     System.out.println("Long click");
        	     //startActionMode(modeCallBack);
        	     view.setSelected(true);
        	     return true;
        	   }
        });
        codeList.setMultiChoiceModeListener(new MultiChoiceModeListener() {
        	
           private int nr = 0;
           public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
               return false;
           }
            
           
           public void onDestroyActionMode(ActionMode mode) {
        	   productListAdapter.removeSelection();
           }
           
           public boolean onCreateActionMode(ActionMode mode, Menu menu) {
               nr = 0;
               MenuInflater inflater = getMenuInflater();
               inflater.inflate(R.menu.action_list_menu, menu);
               menu.getItem(1).setTitle(getString(R.string.delete));
               mMenuItemEdit = (MenuItem) menu.findItem(R.id.action_edit);
               return true;
           }
           
           public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
               switch (item.getItemId()) {
               	   case R.id.action_delete:
               		   SparseBooleanArray selected = productListAdapter.getSelectedIds();
               		   for(int i = selected.size()-1;i>=0;i--){
               			   if(selected.valueAt(i)){
               				   DeliveryListItem selectedProduct = productListAdapter.getItem(selected.keyAt(i));
               				   selectedProduct.delete();
               				   productListAdapter.remove(selectedProduct);
               			   }
               		   }
               		   if(productListAdapter.getCount() == 0){
               			   toggleList(OFF);
               		   }
               		   mode.finish();
               		   return true;
               	   case R.id.action_edit:
               	       mode.finish();
               	       onItemEdit(selectedProductIndex);               	       
               	       return true;
                   default:
                	   return false;
               }
           }
           
           public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                if (checked) {
                   nr++;
                   productListAdapter.toggleSelection(position);                   
               } else {
                   nr--;
                   productListAdapter.toggleSelection(position);                
               }
               mode.setTitle(getString(R.string.selected, nr));
               selectedProductIndex = position;
               if(nr == 1){
                   mMenuItemEdit.setVisible(true);
               } else{
                   mMenuItemEdit.setVisible(false);
               }
           }
       });
    }
    
    private void displayItems(List<DeliveryListItem> items){
    	if(items == null || items.isEmpty() ){
        	toggleList(OFF);
        }
        else{
        	toggleList(ON);
        }
    }
    
    public void toggleList(boolean on){
    	if(!on){
    		codeList.setVisibility(View.GONE);
        	mBntGenReceipt.setEnabled(false);
        	mBntGenReceipt.setClickable(false);
        	mBntGenReceipt.setAlpha(new Float(0.5));
        	mViewNoProducts.setVisibility(View.VISIBLE);
        	mViewNoProducts.setText(getString(R.string.no_products_in_delivery_list, app.getDeliveryList().getListName()));
    	}
    	else {
    		codeList.setVisibility(View.VISIBLE);
        	mViewNoProducts.setVisibility(View.GONE);
        	productListAdapter = new ProductListAdapter(this, app.getDeliveryList().getProducts());
            codeList.setAdapter(productListAdapter);
    	}
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scan, menu);
		return true;
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
		switch(id){
			case R.id.scanItem:
				Intent intent = new Intent(this, ScanActivity.class);
//				Bundle bundle = new Bundle();
	        	startActivity(intent);
	        	break;
			case R.id.typeCode:
				View typeDialog = getLayoutInflater().inflate(R.layout.dialog_type_barcode, null);
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setView(typeDialog);
				final EditText newBarcode = (EditText) typeDialog.findViewById(R.id.editText1);
				newBarcode.setHint(getString(R.string.type_barcode_hint));
				builder.setTitle(getString(R.string.search_product));
				builder.setMessage(getString(R.string.type_barcode_message));
				builder.setCancelable(true);
				builder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent(ScanListDetailActivity.this, ScanActivity.class);
								intent.putExtra("barcode", newBarcode.getText().toString());
								dialog.dismiss();
					        	startActivity(intent);
							}

						})
						.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}

						});
				AlertDialog dialog = builder.create();
				dialog.show();
				break;
					
		}

        return super.onOptionsItemSelected(item);
    }
    
    private void onItemEdit(final int position){
    	
		LayoutInflater layoutInflater = LayoutInflater.from(ScanListDetailActivity.this);
		View dialogEdit = layoutInflater.inflate(R.layout.numberpicker_dialog, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScanListDetailActivity.this);
		alertDialogBuilder.setView(dialogEdit);
		NumberPickerBuilder pickerBuilder = new NumberPickerBuilder(dialogEdit.findViewById(R.id.numberPicker2));
		pickerBuilder.setmDefault((int) productList.get(position).getReceivedAmount());
		final NumberPicker editAmount = pickerBuilder.create();
	
		// setup a dialog window
		alertDialogBuilder.setTitle(productList.get(position).getProduct().toString())
				.setCancelable(true)
				.setPositiveButton(getString(R.string.edit), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					    DeliveryListItem p = productList.get(position);
					    p.setReceivedAmount(editAmount.getValue());
					    p.save();
					    productListAdapter.notifyDataSetChanged();
					}
				})
				.setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {
						dialog.cancel();
					}
				});
		alertDialogBuilder.create().show();
    }
    
    public void onClick(View button) {
    	Intent targetIntent;
    	if(button.getId() == R.id.btn_scan){
           targetIntent = new Intent(app, ScanActivity.class);
           startActivity(targetIntent);
        }        
        if(button.getId() == R.id.btn_gen_goodreceipt){
        	
    		if(!app.isConnected()){
    			Toast.makeText(this, R.string.no_network_connection, Toast.LENGTH_LONG).show();
    		} else{

    			if(app.getObService() == null ){
    			    mAuthTask = new LoginTask(this, app.getUser().getUsername(), app.getUser().getPassword());
    			    mAuthTask.setOnPostExecuteListener(this);
    			    loadingViewHelper.showProgress(true);
    			    mAuthTask.execute((Void) null);
    			} else{
    				if(app.getDeliveryList().getListType() == DeliveryList.INVENTORY)
        				startNewInventory();
    				else
    					startGenReceiptActivity();
    			}
    		} 
        }
    }

    public void onPostExecute(Boolean success, List<String> errorMessages) {
		this.mAuthTask = null;
		loadingViewHelper.showProgress(false);
		if(errorMessages != null){
		    for(String errorMsg : errorMessages){
		    	Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
		    }
		}	
		if (success) {
		    startGenReceiptActivity(); 		
		} else{
		    Toast.makeText(this, getString(R.string.username_pass_changed_in_ob), Toast.LENGTH_LONG).show();
		}
    }
    
    private void startGenReceiptActivity(){
		Intent  targetIntent = new Intent (app, GenReceiptActivity.class);
		app.setObOrder(null);
		startActivity(targetIntent);
    }
    
    private void startNewInventory(){
    	Intent  targetIntent = new Intent (app, NewInventoryActivity.class);
		app.setObOrder(null);
		startActivity(targetIntent);
    }
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
//    	super.onBackPressed();
    	navigateUpTo(getParentActivityIntent());
    }
    
    @Override
    public boolean onNavigateUp() {
    	// TODO Auto-generated method stub
    	return super.onNavigateUp();
    }

}

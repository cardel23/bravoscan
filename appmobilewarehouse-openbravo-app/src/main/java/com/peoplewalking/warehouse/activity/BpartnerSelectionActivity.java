package com.peoplewalking.warehouse.activity;

import java.util.List;
import java.util.Map;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.dialogs.FilterPartnerDialog;
import com.peoplewalking.warehouse.dialogs.FilterPartnerDialog.OnFilterPartnerListener;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.BusinessPartnerAdapter;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.tasks.LoadBpartnersTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.widget.AbsListView.LayoutParams;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class BpartnerSelectionActivity extends Activity implements OnPostExecuteListener<List<BusinessPartner>>, OnItemClickListener, OnFilterPartnerListener {
    private static final String TAG = "BpartnerSelectionActivity";
    //private List<BusinessPartner> bPartnersWithPendingOrders;   
    private BusinessPartnerAdapter bPartnerAdapter;
    private ListView mViewBpartners;
    private LoadBpartnersTask loadBPartnerTask;
    private Integer startRow, endRow;
    private Integer rowIncrement= 50;
    private Button btnLoadMore;
    private NovoventApp app;
    private FilterPartnerDialog dialogFilterPartner;
    private Map<String, Object> filterParams;
    private Button btnFilter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_bpartner_selection);
         this.mViewBpartners = (ListView) findViewById(R.id.lv_bPartners);
         this.mViewBpartners.setOnItemClickListener(this);
         this.btnLoadMore = new Button(this);
         this.btnLoadMore.setBackgroundColor(R.color.app_base_color);
         this.btnLoadMore.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
         this.btnLoadMore.setText("Load more");
         this.btnLoadMore.setVisibility(View.GONE);         
         this.btnLoadMore.setOnClickListener(new View.OnClickListener() {	    
		    public void onClick(View v) {
			startRow = endRow + 1;
			endRow += rowIncrement; 
			loadBpartners(filterParams); 
	    }
         });
         this.mViewBpartners.addFooterView(this.btnLoadMore);
         this.app = (NovoventApp) getApplicationContext();
         
         this.bPartnerAdapter = new BusinessPartnerAdapter(this, null);
         this.mViewBpartners.setAdapter(bPartnerAdapter);

         dialogFilterPartner = new FilterPartnerDialog();
         btnFilter = (Button) findViewById(R.id.btn_filter);
         btnFilter.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				 dialogFilterPartner.show(getFragmentManager(), "FilterPartnerDialog");
			}
		});
        if(app.getDeliveryList().isInbound()){
        	dialogFilterPartner.setPartnerCategory(Constants.PARTNER_CATEGORY_VENDOR);
        } else{
        	dialogFilterPartner.setPartnerCategory(Constants.PARTNER_CATEGORY_CLIENT);
        }
        dialogFilterPartner.show(getFragmentManager(), "FilterPartnerDialog");
    }
    
    

    /**
     * Load partners from OB 
     * @param withPendingOrders if true then load parther with pending orders, else load all partners
     */
    private void loadBpartners(Map<String, Object> filterParams){
		try {
		    //withPendingOrders, app.getDeliveryList().isInbound()
			loadBPartnerTask = new LoadBpartnersTask(this, filterParams, startRow, endRow);
			loadBPartnerTask.setOnPostExecuteListener(this);
//			Thread r = new Thread("timeoutThread") {
//				public void run() {
//					try {
//						loadBPartnerTask.get(10000, TimeUnit.MILLISECONDS);
//						System.out.print("TODO BIEN");
//					} catch (Exception e) {
//						// TODO: handle exception
//						System.out.print("SE CANCELA");
//						loadBPartnerTask.cancel(true);                           
//	                    runOnUiThread(new Runnable()
//	                    { 
//	                        public void run()
//	                         {
//	                        	System.out.print("FINALIZA ACTIVIDAD");
//	                            Toast.makeText(app, "Timeout.", Toast.LENGTH_LONG).show();
//	                            finish(); //will close the current activity comment if you don't want to close current activity.                                
//	                         }
//	                    });
//					}
//				}
//			};
	        loadBPartnerTask.execute();
//	        r.start();
	        startRow = endRow + 1;
	        endRow += rowIncrement;
		} catch (Exception e) {
		    Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
		    Log.e(TAG, e.getMessage(), e);
		}	
    }

    public void onPostExecute(List<BusinessPartner> bPartnerList,
	    List<String> errorMessages) {
    	if(bPartnerList != null){
    	    try {
    	    	this.bPartnerAdapter.addAll(bPartnerList);
    		    if(bPartnerList.size() < rowIncrement){
    			this.btnLoadMore.setVisibility(View.GONE);
    		    } else{
    			this.btnLoadMore.setVisibility(View.VISIBLE);
    		    }
    	    } catch (Exception e) {
    		Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
    		Log.e(TAG, e.getMessage(), e);
    	    }
    	
    	}	
		if(errorMessages != null){
		    for(String errorMsg:errorMessages){
			Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
		    }
		}
    }

    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
	try {
	    BusinessPartner selectedBpartner = (BusinessPartner) adapter.getItemAtPosition(position);	
	    Intent intent = new Intent();
	    Bundle mBundle = new Bundle(); 
	    mBundle.putSerializable(Constants.ID_EXTRA_SELECTED_BPARTNER, selectedBpartner); 
	    intent.putExtras(mBundle);
	    setResult(Constants.REQUEST_CODE_BPARTNER, intent);
	    finish();
	} catch (Exception e) {
	    Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
	    Log.e(TAG, e.getMessage(), e);
	}
	
    }



    public void onFilter(DialogInterface dialog,
	    Map<String, Object> filterParams) {
    	startRow = 0;
    	endRow = startRow + rowIncrement;
    	this.bPartnerAdapter.clear();
	    loadBpartners(filterParams);
    }
    
    
    
    

}

package com.peoplewalking.warehouse.activity;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.adapters.DocumentTypeAdapter;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ListView;

/**
 * Actividad para seleccionar el tipo de documento del albarán de entrada o salida
 * @author Carlos Delgadillo
 * @fecha 20/05/2015
 *
 */
public class DocTypeSelectionActivity extends Activity implements OnItemSelectedListener {

	private ListView listView;
	LoadingViewHelper loadingHelper;
	private NovoventApp app;
	private DocumentTypeAdapter docTypeAdapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(app == null)
			app = (NovoventApp) getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doc_type_selection);
		listView = (ListView) findViewById(android.R.id.list);
		loadingHelper = new LoadingViewHelper(this, listView);
		
		if(app.getDeliveryList().isInbound()){
			docTypeAdapter = new DocumentTypeAdapter(app, app.getInboundDocList());
		} else{
			docTypeAdapter = new DocumentTypeAdapter(app, app.getOutboundDocList());
		}
		listView.setAdapter(docTypeAdapter);
		listView.setOnItemSelectedListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent();
				Bundle mBundle = new Bundle(); 
				mBundle.putSerializable(Constants.ID_EXTRA_SELECTED_DOCTYPE, docTypeAdapter.getItem(position)); 
				intent.putExtras(mBundle);
				setResult(Constants.REQUEST_CODE_DOCTYPE, intent);
				finish();
				
			}
		});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doc_type_selection, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent();
		Bundle mBundle = new Bundle(); 
		mBundle.putSerializable(Constants.ID_EXTRA_SELECTED_DOCTYPE, docTypeAdapter.getItem(position)); 
		intent.putExtras(mBundle);
		setResult(Constants.REQUEST_CODE_DOCTYPE, intent);
		finish();
	}

	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
}

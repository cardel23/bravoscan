package com.peoplewalking.warehouse.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit.RetrofitError;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.BusinessPartnerAdapter;
import com.peoplewalking.warehouse.openbravo.adapters.PriceListAdapter;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.ProductCategory;
import com.peoplewalking.warehouse.openbravo.model.RequisitionLine;
import com.peoplewalking.warehouse.tasks.LoadCategoriesTask;
import com.peoplewalking.warehouse.tasks.LoadPriceListsTask;
import com.peoplewalking.warehouse.tasks.LoadVendorsTask;
import com.peoplewalking.warehouse.tasks.TaskWithCallback;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


/**
 * 
 * @author Carlos Delgadillo
 *
 */
public class NewRequisitionLineActivity extends Activity implements OnClickListener, OnEditorActionListener, OnScrollListener {

	private Spinner productSpinner, categorySpinner, priceListSpinner;
	private EditText quantityTxt, priceTxt, listPriceTxt, discountTxt, descriptionTxt;
	private Button addButton, finishButton;
	private NovoventApp app;
	private RequisitionLine currentRequisitionLine;
	private List<Product> productList;
	private BusinessPartner currentBusinessPartner;
	private ProductCategory currentCategory;
	private float currentPrice;
	private float currentListPrice;
	private float currentDiscount;
	private int startRow = 0;
	private int endRow = 499;
	private ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_requisition_line);
		addButton = (Button) findViewById(R.id.button1);
		finishButton = (Button) findViewById(R.id.button2);
		addButton.setOnClickListener(this);
		finishButton.setOnClickListener(this);
//		LoadFrameTask task = new LoadFrameTask(NewRequisitionLineActivity.this);
//		task.execute();
		initActivity();
	}
	
	private void initActivity(){
		if(app == null)
			app = (NovoventApp) getApplicationContext();
		currentRequisitionLine = new RequisitionLine("ProcurementRequisitionLine");
		currentBusinessPartner = app.getCurrentRequisition().getPartnerRef();
		progressDialog = ProgressDialog.show(this, "", getString(R.string.loading));
		setCategorySpinner();
//		setProductSpinner();
		setPriceListSpinner();
		setPriceAndDiscountText();
		descriptionTxt = (EditText) findViewById(R.id.editText5);
	}
	
	/**
	 * 
	 */
	private void setProductSpinner(){
		productSpinner = (Spinner) findViewById(R.id.spinner2);
		try {
			asyncProductsCall();
		} catch (RetrofitError e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	private void setCategorySpinner(){
		categorySpinner = (Spinner) findViewById(R.id.spinner1);
		final ArrayAdapter<ProductCategory> categoryAdapter = new ArrayAdapter<ProductCategory>(this, android.R.layout.simple_list_item_1);
		if(app.getCategoryList() == null){
			asyncCategoriesCall(categoryAdapter);
			setCategoryAdapter(categoryAdapter);
		}
		else{
			categoryAdapter.addAll(app.getCategoryList());
			setCategoryAdapter(categoryAdapter);
		}
	}
	
	private void setCategoryAdapter(final ArrayAdapter<ProductCategory> categoryAdapter){
		categorySpinner.setAdapter(categoryAdapter);
		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				ArrayAdapter<ProductCategory> adapter = (ArrayAdapter<ProductCategory>) categorySpinner.getAdapter();
				currentCategory = adapter.getItem(position);
				setProductSpinner();
			}
			
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
	}
	
	private void asyncCategoriesCall(final ArrayAdapter<ProductCategory> categoryAdapter){
		LoadCategoriesTask categoriesTask = new LoadCategoriesTask(NewRequisitionLineActivity.this);
		categoriesTask.setOnPostExecuteListener(new OnPostExecuteListener<List<ProductCategory>>() {
			
			public void onPostExecute(List<ProductCategory> response,
					List<String> errorMessages) {
				ProductCategory p = new ProductCategory();
				p.setName("");
				app.setCategoryList(response);
				categoryAdapter.addAll(response);
				
			}
		});
		categoriesTask.execute();
	}
	
	private void asyncProductsCall(){
		ProductListTask productListTask = new ProductListTask(NewRequisitionLineActivity.this);
		productListTask.setOnPostExecuteListener(new OnPostExecuteListener<List<Product>>() {
			
			public void onPostExecute(List<Product> response, List<String> errorMessages) {
				// TODO Auto-generated method stub
				setProductAdapter(productSpinner, response);
			}
		});
		productListTask.execute();
	}
	
	/**
	 * 
	 */
	private void setPriceListSpinner(){
		try {
			priceListSpinner = (Spinner) findViewById(R.id.spinner3);
			if(app.getPriceLists() == null){
				LoadPriceListsTask priceListsTask = new LoadPriceListsTask(this);
//				app.setPriceLists(priceListsTask.executeSynchronous());
				priceListsTask.setOnPostExecuteListener(new OnPostExecuteListener<List<PriceList>>() {
					
					public void onPostExecute(List<PriceList> response,
							List<String> errorMessages) {
						// TODO Auto-generated method stub
						app.setPriceLists(response);
						setPriceListAdapter(priceListSpinner, response, currentBusinessPartner);
					}
				});
				priceListsTask.execute();
			}
			else{
				setPriceListAdapter(priceListSpinner, app.getPriceLists(), currentBusinessPartner);
			}
			
			priceListSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					PriceListAdapter adapter = (PriceListAdapter) priceListSpinner.getAdapter();
					PriceList priceList = adapter.getItem(position);
					currentRequisitionLine.setPriceList(priceList.getObId());
				}

				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	private void setPriceAndDiscountText(){
		
		quantityTxt = (EditText) findViewById(R.id.editText1);
		quantityTxt.setText(null);
		priceTxt = (EditText) findViewById(R.id.editText2);
		listPriceTxt = (EditText) findViewById(R.id.editText3);
		discountTxt = (EditText) findViewById(R.id.editText4);
		priceTxt.setOnEditorActionListener(this);
		listPriceTxt.setOnEditorActionListener(this);
		quantityTxt.addTextChangedListener(new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				try {
					currentRequisitionLine.setQuantity(Integer.valueOf(quantityTxt.getText().toString()));
				} catch (Exception e) {
					// TODO: handle exception
					currentRequisitionLine.setQuantity(0);
				}
			}
			
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		discountTxt.setOnEditorActionListener(new OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				currentListPrice = Float.valueOf(listPriceTxt.getText().toString());
				currentDiscount = Float.valueOf(discountTxt.getText().toString());
				try {
					currentListPrice = calculateUnitPrice(currentListPrice, currentDiscount);
					priceTxt.setText(String.valueOf(currentListPrice));
					currentRequisitionLine.setUnitPrice(currentListPrice);
				} catch (Exception e) {
					priceTxt.setText(null);
					currentRequisitionLine.setUnitPrice(null);
				}
				return false;
			}
		});
		progressDialog.dismiss();
	}
	
	
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		// TODO Auto-generated method stub
		currentPrice = Float.valueOf(priceTxt.getText().toString());
		currentListPrice = Float.valueOf(listPriceTxt.getText().toString());
		try {
			currentDiscount = calculateDiscount(currentPrice, currentListPrice);
			discountTxt.setText(String.valueOf(currentDiscount));
			currentRequisitionLine.setDiscount(currentDiscount);
		} catch (Exception e) {
			// TODO: handle exception
			discountTxt.setText(null);
			currentRequisitionLine.setDiscount(null);
		}
		return false;
	}
	
	/**
	 * 
	 * @param currentPrice
	 * @param currentListPrice
	 * @return
	 */
	private float calculateDiscount(float currentPrice, float currentListPrice){
		float calculatedDiscount;
		calculatedDiscount = (1-(currentPrice/currentListPrice))*100;
		return calculatedDiscount;
	}
	
	/**
	 * 
	 * @param currentListPrice
	 * @param currentDiscount
	 * @return
	 */
	private float calculateUnitPrice(float currentListPrice, float currentDiscount){
		float calculatedPrice;
		calculatedPrice = currentListPrice*(1-currentDiscount);
		return calculatedPrice;
	}
	
	/**
	 * 
	 * @param productSpinner
	 * @param productList
	 */
	private void setProductAdapter(final Spinner productSpinner, List<Product> productList){
		ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(this, android.R.layout.simple_list_item_1, productList);
		productSpinner.setAdapter(adapter);
		productSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@SuppressWarnings("unchecked")
			ArrayAdapter<Product> adapter = (ArrayAdapter<Product>) productSpinner.getAdapter();
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				Product product = adapter.getItem(position);
				currentRequisitionLine.setProduct(product.getObId());
				currentRequisitionLine.setName(product.getTitle());
			}

			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
	}
	
	/**
	 * 
	 * @param categorySpinner
	 * @param objects
	 */
	private void setBusinessPartnerAdapter(Spinner partnerSpinner, List<BusinessPartner> objects){
		BusinessPartnerAdapter spinnerAdapter = new BusinessPartnerAdapter(this, objects);
		partnerSpinner.setAdapter(spinnerAdapter);
		return;
	}
	
	/**
	 * 
	 * @param priceListSpinner
	 * @param priceLists
	 * @param businessPartner
	 */
	private void setPriceListAdapter(Spinner priceListSpinner, List<PriceList> priceLists, BusinessPartner businessPartner){
		PriceListAdapter priceListAdapter = new PriceListAdapter(this, priceLists);
		priceListSpinner.setAdapter(priceListAdapter);
		int position = priceListAdapter.getPositionByBusinessPartner(businessPartner);
		priceListSpinner.setSelection(position);
	}
	private boolean isValidLine(RequisitionLine line){
		if(line.getNeedByDate() == null){
			Toast.makeText(app, "Fecha no puede ser null", Toast.LENGTH_SHORT).show();
			return false;
		}
		else
		if(line.getQuantity() == 0){
			Toast.makeText(app, "Cantidad no puede ser 0", Toast.LENGTH_SHORT).show();
			return false;
		}
		else
		return true;
	}
	
	private boolean save(){
		currentRequisitionLine.setDescription(descriptionTxt.getText().toString());
		currentRequisitionLine.setRequisitionId(app.getCurrentRequisition().getId());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		String date = dateFormat.format(new Date());
		currentRequisitionLine.setNeedByDate(date);
		if(isValidLine(currentRequisitionLine)){
//			currentRequisitionLine.setObId(Utils.generateObId());
//			currentRequisitionLine.setRequisition(app.getCurrentRequisition().getObId());
			currentRequisitionLine.save();
			app.getCurrentRequisition().getRequisitionLines().add(currentRequisitionLine);
			app.getCurrentRequisition().save();
			return true;
		}
		return false;
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.new_requisition_line, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.button1:
			if(save())
				initActivity();
			break;
		case R.id.button2:
			if(save()){
				initActivity();
				onNavigateUp();
			}
			break;
		default:
			break;
		}
	}
	
	private class LoadFrameTask extends TaskWithCallback<Void, Void, Void>{

		public LoadFrameTask(Context context){
			super(context);
		}
		public LoadFrameTask(Context context, String loadingMessage) {
			super(context, loadingMessage);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				public void run() {
					initActivity();
				}
			});
			return null;
		}
		
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		
	}

	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
		
	}
	
	public class ProductListTask extends TaskWithCallback<Void, Void, List<Product>> {

		public ProductListTask(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected List<Product> doInBackground(Void... params) {
			// TODO Auto-generated method stub
			return executeSynchronous(params);
		}
		
		@Override
		protected List<Product> executeSynchronous(Void... params) {
			// TODO Auto-generated method stub
			try {
				queryParams = new HashMap<String, String>();
				query = new StringBuilder("organization='"+app.getUser().getOrganizationId() + 
						"' AND storageBin.warehouse='" + app.getUser().getDefaultWarehouseId() + 
						"' AND productCategory='" + currentCategory.getObId() + "' AND purchase=TRUE");
				queryParams.put("_where", query.toString());
				queryParams.put("_selectedProperties","id,name");
				queryParams.put("_sortBy","name");
//				queryParams.put("_startRow", String.valueOf(startRow));
//				queryParams.put("_endRow", String.valueOf(endRow));
				
				productList = OBServiceClient.getOBService().getProductList(queryParams);
				return productList;
//				setProductAdapter(productSpinner, productList);
			} catch (RetrofitError e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return super.executeSynchronous(params);
		}

	}
}

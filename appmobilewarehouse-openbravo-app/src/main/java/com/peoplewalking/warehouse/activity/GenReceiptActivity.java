package com.peoplewalking.warehouse.activity;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.exceptions.InvalidArgumentException;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.BusinessPartnerLocationAdapter;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartnerLocation;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.tasks.LoadBpartnerLocationsTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

public class GenReceiptActivity extends Activity implements OnPostExecuteListener<List<BusinessPartnerLocation>>{
	public static final String TAG = "GenerateReceiptActivity";
	private TextView mViewSelectedList;
	private TextView mViewListNumProducts;
	private TextView mViewSelectedOrder;
	private TextView mViewOrderPartner;
	private TextView mViewOrderDateCreated;
	private TextView mViewOrderAmount, mSelectedList;
	private TextView mViewSelectedBpartner;
	private TextView mViewSelectedBpartnerLocation;
	private TextView mViewSelectedDocumentType;
	private DeliveryList selectedDeliveryList;
	private BusinessPartner selectedBpartner;
	private GoodsReceipt currentGoodReceipt;
	private NovoventApp app;
	private LoadBpartnerLocationsTask loadBLocationsTask;
	private LoadingViewHelper loadingBPartnerLocationHelper;
	private List<BusinessPartnerLocation> bPartnerLocationList;
	private Dialog loadingBPartnerLocationDialog;
	private ImageView imgSelectedDocType, imgSelectedList,
	    	imgSelectedOrder, imgSelectedBpartner, imgSelectedBpartnerLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		app = (NovoventApp)getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_generate_receipt);
		
		imgSelectedDocType = (ImageView) findViewById(R.id.image_doc_type);
		imgSelectedList = (ImageView) findViewById(R.id.image_scanned_list);
		imgSelectedOrder = (ImageView) findViewById(R.id.image_order);
		imgSelectedBpartner = (ImageView) findViewById(R.id.image_bPartner);
		imgSelectedBpartnerLocation = (ImageView) findViewById(R.id.image_bPartner_location);
		
		loadingBPartnerLocationHelper = new LoadingViewHelper(this, imgSelectedBpartnerLocation);
		
		mViewSelectedDocumentType = (TextView) findViewById(R.id.tv_doc_type);
		//DisplayList section
		mSelectedList = (TextView) findViewById(R.id.textView1);
		mViewSelectedList = (TextView) findViewById(R.id.tv_selected_list);
		mViewListNumProducts = (TextView) findViewById(R.id.tv_list_num_products);
		
		//Bpartner section
		mViewSelectedBpartner = (TextView) findViewById(R.id.tv_selected_bpartner);
		
		//BpartnerLocation section
		mViewSelectedBpartnerLocation = (TextView) findViewById(R.id.tv_selected_bpartner_location);
		
		//Order section
		mViewSelectedOrder = (TextView) findViewById(R.id.tv_selected_order);
		mViewOrderPartner = (TextView) findViewById(R.id.tv_order_partner);
		mViewOrderDateCreated = (TextView) findViewById(R.id.tv_order_date);
		mViewOrderAmount = (TextView) findViewById(R.id.tv_order_total_amount);
		
		try {
		    currentGoodReceipt = new GoodsReceipt();
//		    currentGoodReceipt.setDocumentNo("10000555");
//			currentGoodReceipt.setDocTypeId("BBC17DD5FD25470BB2752D096B72D412");
			currentGoodReceipt.setWarehouseId(app.getWarehouse());
			currentGoodReceipt.setMovementDate(Utils.dateToString(new Date()));
			currentGoodReceipt.setAccountingDate(Utils.dateToString(new Date()));
			
		    	selectedDeliveryList = app.getDeliveryList();
        		setSelectedDeliveryList(selectedDeliveryList);
        		setSelectedOrder(null);
        		setSelectedDocType(null);
        		setSelectedBpartner(null);
        		setSelectedBpartnerLocation(null);
        		
        		mSelectedList.setText(R.string.selected_list);
        		
        		if(selectedDeliveryList.isInbound()){
        			if(app.getDefaultInboundPreference() != null){
        				setSelectedDocType(app.getDefaultInboundFromList(app.getDefaultInboundPreference()));
        			}
        		}
        		else if(!selectedDeliveryList.isInbound()){
        			if(app.getDefaultOutboundPreference() != null){
        				setSelectedDocType(app.getDefaultOutboundFromList(app.getDefaultOutboundPreference()));
        			}
        		}
        		
		} catch (Exception e) {
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
			Log.e(TAG, e.getMessage(), e);
		}
		loadingBPartnerLocationDialog = new AlertDialog.Builder(this)
		.setTitle("Loading")
		.setView(getLayoutInflater().inflate(R.layout.template_loading_progressbar, null))
		.setCancelable(true)
		.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {			    
		    public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
			loadBLocationsTask.cancel(true);
		    }
		}).create();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.generate_receipt, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onViewClick(View button){
		Intent targetIntent;
		Bundle extras;
		switch (button.getId()) {
		case R.id.select_list_container :
//			  targetIntent = new Intent(this ,ScanListsActivity.class);  
//			  startActivityForResult(targetIntent, Constants.REQUEST_CODE_DELIVERY_LIST);
			break;		
		case R.id.select_order_container:
			 targetIntent = new Intent(this ,OrderSelectionActivity.class);  	
			 extras = new Bundle();
			 extras.putSerializable(Constants.ID_EXTRA_SELECTED_BPARTNER, selectedBpartner);
			 targetIntent.putExtras(extras);
			 startActivityForResult(targetIntent, Constants.REQUEST_CODE_ORDER);
			break;
		case R.id.select_bpartner_container:
			 targetIntent = new Intent(this ,BpartnerSelectionActivity.class); 			
			 startActivityForResult(targetIntent, Constants.REQUEST_CODE_BPARTNER);
			break;
		case R.id.select_bpartner_location_container:
			showBpartnerLocationSelectionDialog();
			break;
		case R.id.select_doctype_container:
			targetIntent = new Intent(app, DocTypeSelectionActivity.class);
			startActivityForResult(targetIntent, Constants.REQUEST_CODE_DOCTYPE);
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
	    if(resultCode != 0 && data != null){
		try {
			switch (requestCode) {
    		   	case Constants.REQUEST_CODE_DELIVERY_LIST:
            		    selectedDeliveryList = (DeliveryList) data.getSerializableExtra(Constants.ID_EXTRA_SELECTED_LIST);
            		    app.setDeliveryList(selectedDeliveryList);
            		    break;
    			case Constants.REQUEST_CODE_ORDER:
    			    setSelectedOrder((OBOrder) data.getSerializableExtra(Constants.ID_EXTRA_SELECTED_ORDER));			    
    			    break;
    			case Constants.REQUEST_CODE_BPARTNER:
    			    setSelectedBpartner( (BusinessPartner) data.getSerializableExtra(Constants.ID_EXTRA_SELECTED_BPARTNER));
    			    setSelectedOrder(null);
    			    break;
    			case Constants.REQUEST_CODE_DOCTYPE:
    			    setSelectedDocType((DocumentType) data.getSerializableExtra( Constants.ID_EXTRA_SELECTED_DOCTYPE ));
    			    break;
    			default:
    			    break;
    		     } 
		} catch (Exception e) {
		    	Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
			Log.e(TAG, e.getMessage(), e);
		}
		
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void setReceiptDetails(View view){
	    try {
			if(currentGoodReceipt.getDocTypeId() == null){
			    throw new InvalidArgumentException(getString(R.string.select_doc_type));
			}
			if(selectedDeliveryList == null){
			    throw new InvalidArgumentException(getString(R.string.select_list));
			}
			if(currentGoodReceipt.getPartnerId() == null){
			    throw new InvalidArgumentException(getString(R.string.prompt_select_partner));
			}
			if(currentGoodReceipt.getPartnerLocationId() == null){
			    throw new InvalidArgumentException(getString(R.string.select_bpartner_location));
			}
			app.setGoodReceipt(currentGoodReceipt);
			Intent targetIntent = new Intent(this,GenReceiptDetailActivity.class);
			Bundle extras = new Bundle();
			extras.putSerializable(Constants.ID_EXTRA_CURRENT_GOODRECEIPT, currentGoodReceipt);
			targetIntent.putExtras(extras);
			startActivity(targetIntent);
		
	    } catch(InvalidArgumentException exception){
			Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
			Log.e(TAG, exception.getMessage(), exception);
	    }catch (Exception e) {
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
			Log.e(TAG, e.getMessage(), e);
	    }
	}
		
	private void setSelectedDeliveryList(DeliveryList deliveryList){
	    	selectedDeliveryList = deliveryList;
	    	app.setDeliveryList(deliveryList);
		if (deliveryList != null) {
			mViewSelectedList.setText(deliveryList.getListName());
			mViewListNumProducts.setVisibility(View.VISIBLE);
			mViewListNumProducts.setText(getString(R.string.n_products,
					deliveryList.getProducts().size()));
			imgSelectedList.setImageResource(R.drawable.icon_ok);
		} else {
			mViewSelectedList.setText(getString(R.string.select_list));
			mViewListNumProducts.setVisibility(View.GONE);
			imgSelectedList.setImageResource(R.drawable.icon_required);
		}
	}
	
	private void setSelectedDocType(DocumentType documentType){
		if(documentType != null){
			mViewSelectedDocumentType.setText(documentType.toString());
			currentGoodReceipt.setDocTypeId(documentType.getObId());
			currentGoodReceipt.setDocTypeIdentifier(documentType.getIdentifier());
			imgSelectedDocType.setImageResource(R.drawable.icon_ok);
		}else{
			mViewSelectedDocumentType.setText(getString(R.string.select_doc_type));
			currentGoodReceipt.setDocTypeId(null);
			currentGoodReceipt.setDocTypeIdentifier(null);
			imgSelectedDocType.setImageResource(R.drawable.icon_required);
		}
		
	}
	private void setSelectedBpartner(BusinessPartner bPartner){
	    if(bPartner != null){
			if(!bPartner.getObId().equalsIgnoreCase(mViewSelectedBpartner.getText().toString())){
			    currentGoodReceipt.setPartnerId(bPartner.getObId());
			    currentGoodReceipt.setPartnerIdentifier(bPartner.getIdentifier());
			    mViewSelectedBpartner.setText(bPartner.getIdentifier());			    
			    imgSelectedBpartner.setImageResource(R.drawable.icon_ok);
			    
			    //Load locations
			    loadBLocationsTask = new LoadBpartnerLocationsTask(this);
			    loadBLocationsTask.setOnPostExecuteListener(this);
			    loadBLocationsTask.execute(bPartner.getObId());	
			    loadingBPartnerLocationHelper.showProgress(true);
			}
	    } else {
			currentGoodReceipt.setPartnerId(null);
			currentGoodReceipt.setPartnerIdentifier(null);
			mViewSelectedBpartner.setText(getString(R.string.prompt_select_partner));
			imgSelectedBpartner.setImageResource(R.drawable.icon_required);
	    }
	    selectedBpartner = bPartner;
	}
	
	
	private void setSelectedBpartnerLocation(BusinessPartnerLocation businessPartnerLocation){
	    if(businessPartnerLocation != null ){
			currentGoodReceipt.setPartnerLocationId(businessPartnerLocation.getObId());
			currentGoodReceipt.setPartnerLocationIdentifier(businessPartnerLocation.getBusinessPartnerIdentifier());
			mViewSelectedBpartnerLocation.setText(businessPartnerLocation.getIdentifier());
			imgSelectedBpartnerLocation.setImageResource(R.drawable.icon_ok);
	    } else{
			currentGoodReceipt.setPartnerLocationId(null);
			currentGoodReceipt.setPartnerLocationIdentifier(null);
			mViewSelectedBpartnerLocation.setText(getString(R.string.select_bpartner_location));
			imgSelectedBpartnerLocation.setImageResource(R.drawable.icon_required);
	    }
	}
	
	private void setSelectedOrder(OBOrder order){
	    try {
	    	app.setObOrder(order);
	    	if(order == null){
		        currentGoodReceipt.setSalesOrderId(null);
				currentGoodReceipt.setSalesOrderIdentifier(null);
				
				mViewSelectedOrder.setText(getString(R.string.select_order));
				mViewOrderPartner.setVisibility(View.GONE);
				mViewOrderDateCreated.setVisibility(View.GONE);
				mViewOrderAmount.setVisibility(View.GONE);
				imgSelectedOrder.setImageResource(R.drawable.icon_optional);
			} else{
				currentGoodReceipt.setSalesOrderId(order.getObId());
				currentGoodReceipt.setSalesOrderIdentifier(order.getIdentifier());
				currentGoodReceipt.setPartnerId(order.getBusinessPartnerId());	
				loadBLocationsTask = new LoadBpartnerLocationsTask(this);
				loadBLocationsTask.setOnPostExecuteListener(this);
				loadBLocationsTask.execute(order.getBusinessPartnerId());
				
				mViewOrderPartner.setVisibility(View.VISIBLE);
				mViewOrderDateCreated.setVisibility(View.VISIBLE);
				mViewOrderAmount.setVisibility(View.VISIBLE);
				mViewSelectedOrder.setText(order.getIdentifier());
				mViewOrderPartner.setText(order.getBusinessPartnerIdentifier());
				mViewOrderDateCreated.setText(Utils.splitDateTime(order.getCreationDate()));
				mViewOrderAmount.setText(order.getCurrencyIdentifier() + " " + String.valueOf(order.getSummedLineAmount()));
				
				imgSelectedOrder.setImageResource(R.drawable.icon_ok);
				
				BusinessPartner orderBpartner = new BusinessPartner();
				orderBpartner.setObId(order.getBusinessPartnerId());
				orderBpartner.setIdentifier(order.getBusinessPartnerIdentifier());
				setSelectedBpartner(orderBpartner);
			}
	    } catch (Exception e) {
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
			Log.e(TAG, e.getMessage(), e);
	    }
	    	
	}

	public void onPostExecute(List<BusinessPartnerLocation> bPartnerLocationList,
		List<String> errorMessages) {	
	    	loadingBPartnerLocationHelper.showProgress(false);
		if(errorMessages != null && !errorMessages.isEmpty()){
		    for(String errorMsg: errorMessages){
			Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
		    }
		}
		if(loadingBPartnerLocationDialog.isShowing()){
		    loadingBPartnerLocationDialog.dismiss();
		}
		this.bPartnerLocationList = bPartnerLocationList;
		if(bPartnerLocationList != null && !bPartnerLocationList.isEmpty()){
		    setSelectedBpartnerLocation(bPartnerLocationList.get(0));
		} else{
		    setSelectedBpartnerLocation(null);
		}
	}
	
	private void showBpartnerLocationSelectionDialog(){
        try {
    	    if(currentGoodReceipt.getPartnerId() == null){
    		Toast.makeText(this, getString(R.string.prompt_select_partner), Toast.LENGTH_LONG).show();
    	    } else if(this.bPartnerLocationList == null){
    		 loadingBPartnerLocationDialog.show();
    		 loadBLocationsTask = new LoadBpartnerLocationsTask(this);
    		 loadBLocationsTask.setOnPostExecuteListener(this);
    		 loadBLocationsTask.execute(currentGoodReceipt.getPartnerId());
    	    } else{
    		final BusinessPartnerLocationAdapter  bPartnerLocationAdapter = new BusinessPartnerLocationAdapter(this, this.bPartnerLocationList);
    		
    		new AlertDialog.Builder(this)
    			.setTitle(getString(R.string.select_bpartner_location))
    			.setAdapter(bPartnerLocationAdapter, new DialogInterface.OnClickListener() {			    
    			    public void onClick(DialogInterface dialog, int position) {
    				BusinessPartnerLocation selectedBpartnerLocation = bPartnerLocationAdapter.getItem(position);
    				setSelectedBpartnerLocation(selectedBpartnerLocation);
    			    }})
    			.setCancelable(true)
    			.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {			    
    			    public void onClick(DialogInterface dialog, int which) {
    				dialog.dismiss();
    			    }
    			}).show();
    			
    	    }
        } catch (Exception e) {
        	Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
        	Log.e(TAG, e.getMessage(), e);
        }	    
	}
	
}

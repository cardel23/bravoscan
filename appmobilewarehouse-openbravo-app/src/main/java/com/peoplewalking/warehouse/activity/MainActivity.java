/**
 * 
 */
package com.peoplewalking.warehouse.activity;

import java.util.Locale;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

/**
 * @author Darlon Espinoza
 *	This is the main menu activity
 */
public class MainActivity extends Activity {
	Locale locale;
	Configuration config;
	NovoventApp app;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		app = (NovoventApp)getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		if(app.getHomeIntent() == null)
			app.setIntent(getIntent());
		setContentView(R.layout.activity_main);
		setTitle(R.string.title_activity_main);
	}
	
	
	public void onClickOptionMenu(View button){
		Intent targetActivity = null;
		switch (button.getId()) {
		case R.id.btnNewList:
			targetActivity = new Intent(app, NewScanListActivity.class);
			break;
		case R.id.btnShowLists:
			targetActivity = new Intent(app, ListTabsActivity.class);

			break;
		case R.id.btnGenReceipt:
			targetActivity = new Intent(app, SettingsActivity.class);
			break;
		case R.id.btnExit:
			targetActivity = new Intent(app, LoginActivity.class);
			finish();
			app.logout();
			break;
		default:
			break;
		}
		if(targetActivity != null){
			startActivity(targetActivity);
		}
	}

}

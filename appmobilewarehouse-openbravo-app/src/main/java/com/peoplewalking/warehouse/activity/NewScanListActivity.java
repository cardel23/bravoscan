package com.peoplewalking.warehouse.activity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.w3c.dom.ls.LSInput;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.adapters.BusinessPartnerAdapter;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

/**
 * 
 * @author Carlos Delgadillo
 * fecha 14/04/2015
 *
 */
public class NewScanListActivity extends Activity implements OnClickListener {
	
	private Button createBtn;
	private EditText listNameTxt;
	private TextView listName;
	private CheckBox listType;
	private NovoventApp app;
	private boolean flag;
	private Spinner spinner;
	DeliveryList deliveryList;
	List<BusinessPartner> businessPartners;
	BusinessPartnerAdapter businessPartnerAdapter;
	MenuItem miActionProgressItem;
	ProgressBar v;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (NovoventApp)getApplicationContext();
//		app.setLocaleConfig();
		setContentView(R.layout.activity_new_list);
		setTitle(R.string.title_activity_new_list);
		initNewListActivity();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	private void initNewListActivity(){
		createBtn = (Button)findViewById(R.id.button1);
		listNameTxt = (EditText)findViewById(R.id.editText1);
		listName = (TextView)findViewById(R.id.textItem3);
		listName.setText(R.string.create_new_list);
		createBtn.setOnClickListener(this);
		createBtn.setText(R.string.accept);
//		listType = (CheckBox) findViewById(R.id.checkBox1);
//		listType.setText(getString(R.string.is_inbound_list));
		listNameTxt.setImeActionLabel(getString(R.string.accept), R.id.newListDone);
		deliveryList = new DeliveryList();
		listNameTxt.setText(Utils.generateScanListName(this, false));
		listNameTxt.selectAll();
		listNameTxt.requestFocus();
		
		spinner = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> listTypeAdapter =  ArrayAdapter.createFromResource(this,
					R.array.options_list_type,
					android.R.layout.simple_spinner_item);
		listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	  
		spinner.setAdapter(listTypeAdapter);
		listNameTxt.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE)
				{
				    createList();
				    return true;
				}
				return false;
			}
		});
		
//		listType.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				flag = isChecked;
//			}
//		});
	}
	
	@Override
	public boolean onNavigateUp() {
	    this.finish();
	    return true;
	}
	
	public void onClick(View v) {
		if(v.getId() == R.id.button1){
		    createList();
		}
	}
	
	private void createList(){
		deliveryList.setListName(listNameTxt.getText().toString());
		try {
			if(deliveryList.getListName() == null || deliveryList.getListName().equals("")){
			    throw new Exception(getString(R.string.no_list_name));
			}
			List<DeliveryList> deliveryLists = DeliveryList.find(DeliveryList.class, "list_name = ?", deliveryList.getListName());
			if(deliveryLists != null && !deliveryLists.isEmpty()){
			    throw new Exception(getString(R.string.list_already_exists,deliveryList.getListName()));
			}
			if(spinner.getSelectedItemPosition() == DeliveryList.INBOUND)
				deliveryList.setInbound(true);
			else
				deliveryList.setInbound(false);
			
			deliveryList.setListType(spinner.getSelectedItemPosition());
			deliveryList.setUser(app.getUser());
			deliveryList.save();
			
			app.setDeliveryList(deliveryList);
			if(getCallingActivity() == null){
				Intent intent = new Intent(app, ScanListDetailActivity.class);
				finish();
				startActivity(intent);
			}
			else{
//			Intent intent = new Intent(app, ScanListDetailActivity.class);
				Intent intent = new Intent();
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constants.ID_EXTRA_NEW_LIST, deliveryList);
				intent.putExtras(bundle);
				setResult(Constants.REQUEST_CODE_DELIVERY_LIST, intent);
				finish();
			}
		} catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(),
	        e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_list, menu);
//		v =  (ProgressBar) MenuItemCompat.getActionView(miActionProgressItem);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

package com.peoplewalking.warehouse.activity;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.BusinessPartnerAdapter;
import com.peoplewalking.warehouse.openbravo.adapters.PriceListAdapter;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.tasks.LoadPriceListsTask;
import com.peoplewalking.warehouse.tasks.LoadVendorsTask;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class NewRequisitionActivity extends Activity implements OnClickListener {

	private Spinner spinner1, spinner2;
	private TextView currency;
	private NovoventApp app;
	private BusinessPartner businessPartner;
	private PriceList priceList;
	private Button button;
	
	public NewRequisitionActivity() {
		// TODO Auto-generated constructor stub
	}
	
	public NewRequisitionActivity(Boolean isEdit){
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.button1){
			Requisition r = new Requisition(Utils.generateRequisitionName());
			businessPartner.save();
			app.setCurrentBusinessPartner(businessPartner);
			r.setPartnerRef(app.getCurrentBusinessPartner());
			r.setBusinessPartner(businessPartner.getObId());
			r.setCurrency(priceList.getCurrency());
			r.setOrganization(app.getUser().getDefaultOrganizationId());
			r.setRequester(app.getUser().getObId());
			r.setUser(app.getUser());
			r.save();
			Intent intent = new Intent();
			app.setCurrentRequisition(r);
			intent.setClass(this, RequisitionDetailActivity.class);
			startActivity(intent);
			finish();
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_requisition);
		app = (NovoventApp) getApplicationContext();
		
//		spinner2 = (Spinner) findViewById(R.id.spinner2);
		currency = (TextView) findViewById(R.id.textView5);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);
		setSpinner1();
		
	}
	
	private void setBusinessPartnerAdapter(Spinner spinner, List<BusinessPartner> objects){
		BusinessPartnerAdapter spinnerAdapter = new BusinessPartnerAdapter(this, objects);
		spinner.setAdapter(spinnerAdapter);
		return;
	}
	
	private void setPriceListAdapter(Spinner spinner2, List<PriceList> priceLists){
		PriceListAdapter priceListAdapter = new PriceListAdapter(this, priceLists);
		spinner2.setAdapter(priceListAdapter);
		int position = priceListAdapter.getPositionByBusinessPartner(businessPartner);
		spinner2.setSelection(position);
	}
	
	private void setSpinner1(){
		spinner1 = (Spinner)findViewById(R.id.spinner1);
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				businessPartner = (BusinessPartner) spinner1.getAdapter().getItem(position);
				setSpinner2(businessPartner);
			}

			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});
		if(app.getVendors() == null){
			LoadVendorsTask vendorsTask = new LoadVendorsTask(NewRequisitionActivity.this);
			vendorsTask.setOnPostExecuteListener(new OnPostExecuteListener<List<BusinessPartner>>() {
				public void onPostExecute(List<BusinessPartner> response,
						List<String> errorMessages) {
					app.setVendors(response);
					setBusinessPartnerAdapter(spinner1, response);
					// TODO Auto-generated method stub		
				}
			});
			vendorsTask.execute();
		}
		else {
			setBusinessPartnerAdapter(spinner1, app.getVendors());
		}
	}
	
	private void setSpinner2(BusinessPartner businessPartner) {
		if(spinner2 == null)
			spinner2 = (Spinner) findViewById(R.id.spinner2);
		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				priceList = (PriceList) spinner2.getAdapter().getItem(position);
				currency.setText(priceList.getCurrency$_identifier());
			}

			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
			
		});
		
		if(app.getPriceLists() == null){
			LoadPriceListsTask priceListsTask = new LoadPriceListsTask(NewRequisitionActivity.this);
			priceListsTask.setOnPostExecuteListener(new OnPostExecuteListener<List<PriceList>>() {
				
				public void onPostExecute(List<PriceList> response,
						List<String> errorMessages) {
					// TODO Auto-generated method stub
					app.setPriceLists(response);
					setPriceListAdapter(spinner2, response);
				}
			});
			priceListsTask.execute();
		}
		else {
			setPriceListAdapter(spinner2, app.getPriceLists());
		}
	}
}	

package com.peoplewalking.warehouse.dialogs;

import java.util.HashMap;
import java.util.Map;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.BpartnerSelectionActivity;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.util.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

public class FilterOrderDialog extends DialogFragment implements View.OnClickListener,  DatePickerDialog.OnDateSetListener{
	private static final String TAG = "FilterOrderDialog";
    
    private TextView selectedPartnerTextview;
    private EditText fromDateEditText;
    private EditText toDateEditText;
    private Button btnSelectPartner;
    private Button btnClearPartner;
    private BusinessPartner selectedPartner;
    private OnFilterOrderListener onFilterOrderListener;
    private View dialogView;
    private Map<String, Object> filterParams = new HashMap<String, Object>();
    private DatePickerFragment datePickerFragment;
    private String currentDateFilter;
    private Dialog filterOrderDialog;
    
    public FilterOrderDialog() {
	filterParams = new HashMap<String, Object>();
	datePickerFragment = new DatePickerFragment();
	datePickerFragment.setOnDateSetListener(this);
    }
    
    @Override
    public void onAttach(Activity activity) {
	super.onAttach(activity);
        try {
            onFilterOrderListener = (OnFilterOrderListener) activity;	    
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFilterDoneListener");
        }
        
        try {
            LayoutInflater inflater = getActivity().getLayoutInflater();
  	  dialogView = inflater.inflate(R.layout.dialog_filter_orders, null);
  	  
  	  selectedPartnerTextview = (TextView) dialogView.findViewById(R.id.tv_selected_bpartner);
  	  fromDateEditText = (EditText) dialogView.findViewById(R.id.et_from);
  	  fromDateEditText.setOnClickListener(this);
  	  toDateEditText = (EditText) dialogView.findViewById(R.id.et_to);
  	  toDateEditText.setOnClickListener(this);
  	  btnSelectPartner = (Button) dialogView.findViewById(R.id.btn_select_partner);
  	  btnSelectPartner.setOnClickListener(this);
  	  btnClearPartner = (Button) dialogView.findViewById(R.id.btn_clear_partner);	
  	  btnClearPartner.setOnClickListener(this);

  	  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity())
  	  .setView(dialogView)
  	  .setPositiveButton("Apply", new DialogInterface.OnClickListener() {		    
  		    public void onClick(DialogInterface dialog, int which) {			
  			filterParams.put(Constants.FILTER_ORDER_PARTNER, selectedPartner);
  			filterParams.put(Constants.FILTER_ORDER_DATE_FROM, fromDateEditText.getText().toString());
  			filterParams.put(Constants.FILTER_ORDER_DATE_TO, toDateEditText.getText().toString());
  			FilterOrderDialog.this.onFilterOrderListener.onFilter(dialog, filterParams);
  			dialog.dismiss();			
  		    }
  	   })
  	   .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {	    
  	       	   public void onClick(DialogInterface dialog, int which) {
  	       	       dialog.dismiss();
  	       	   }
  	    });
  	  
           filterOrderDialog = dialogBuilder.create();
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {     
		
	  return filterOrderDialog;
    }
    
    public void setDefaultValue(String key, Object value){
	this.filterParams.put(key, value);
    }
    
    public void setDefaultValues(Map<String, Object> filterParams){
	
    }

    public interface OnFilterOrderListener {
        public void onFilter(DialogInterface dialog, Map<String, Object> filterParams);
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	if(requestCode == Constants.REQUEST_CODE_BPARTNER && data != null){
	        selectedPartner = (BusinessPartner) data.getExtras().getSerializable(Constants.ID_EXTRA_SELECTED_BPARTNER);
		if(selectedPartner != null){		
		    this.selectedPartnerTextview.setText(selectedPartner.getIdentifier());
		    this.filterParams.put(Constants.FILTER_ORDER_PARTNER, selectedPartner);
		}
	    }
    }

    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.btn_select_partner:
	    Intent partnerSelectionIntent = new Intent(getActivity(), BpartnerSelectionActivity.class);
	    startActivityForResult(partnerSelectionIntent, Constants.REQUEST_CODE_BPARTNER);	    
	    break;
	case R.id.btn_clear_partner:
	    selectedPartner = null;
	    this.selectedPartnerTextview.setText(R.string.prompt_select_partner);
	    this.filterParams.remove(Constants.FILTER_ORDER_PARTNER);
	    break;
	case R.id.et_from:
	    currentDateFilter =  Constants.FILTER_ORDER_DATE_FROM;
	    datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
	    break;
	case R.id.et_to:
	    currentDateFilter = Constants.FILTER_ORDER_DATE_TO;
	    datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
	    break;
	default:
	    break;
	}
    }

    public void onDateSet(DatePicker arg0, int year, int monthOfYear, int dayOfMonth) {
	String stringDate = (String.valueOf(year) + "-" + String.valueOf(monthOfYear) + "-" + String.valueOf(dayOfMonth) );
	if(currentDateFilter.equals(Constants.FILTER_ORDER_DATE_FROM)){
	    fromDateEditText.setText(stringDate);
	    this.filterParams.put(Constants.FILTER_ORDER_DATE_FROM, stringDate);
	} else if(currentDateFilter.equals(Constants.FILTER_ORDER_DATE_TO)){
	    toDateEditText.setText(stringDate);
	    this.filterParams.put(Constants.FILTER_ORDER_DATE_TO, stringDate);
	}
    }
}

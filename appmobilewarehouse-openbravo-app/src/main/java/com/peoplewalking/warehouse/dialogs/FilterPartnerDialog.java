package com.peoplewalking.warehouse.dialogs;

import java.util.HashMap;
import java.util.Map;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.util.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class FilterPartnerDialog extends DialogFragment {
    
    private EditText filterPartnerName;
    private Spinner filterPartnerCategory;
    private Spinner filterPendigOrders; 
    private OnFilterPartnerListener onFilterPartnerListener;
    private Integer partnerCategory;
    View dialogView;
    
    @Override
    public void onAttach(Activity activity) {
	super.onAttach(activity);
        try {
            onFilterPartnerListener = (OnFilterPartnerListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFilterDoneListener");
        }
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
	  LayoutInflater inflater = getActivity().getLayoutInflater();
	  View dialogView = inflater.inflate(R.layout.dialog_filter_partner, null);
	  filterPartnerName = (EditText) dialogView.findViewById(R.id.et_name);
	  
	  filterPartnerCategory = (Spinner) dialogView.findViewById(R.id.sp_category);
	  ArrayAdapter<CharSequence> filterCategoryAdapter =ArrayAdapter.createFromResource(getActivity(),
		  									R.array.options_bpartner_types,
		  									android.R.layout.simple_spinner_item);
	  filterCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	  
	  filterPartnerCategory.setAdapter(filterCategoryAdapter);
	  
	  filterPendigOrders = (Spinner) dialogView.findViewById(R.id.sp_pending_orders);
	  ArrayAdapter<CharSequence> filterPendigOrdersAdapter =  ArrayAdapter.createFromResource(getActivity(),
		  								R.array.options_pending_orders,
		  								android.R.layout.simple_spinner_item);
	  filterPendigOrdersAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	  
	  filterPendigOrders.setAdapter(filterPendigOrdersAdapter);
	  
	  if(this.partnerCategory != null){
		filterPartnerCategory.setSelection(this.partnerCategory, true);
		this.filterPartnerCategory.setEnabled(false);
	  }
	  
	  AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity())
	  .setView(dialogView)
	  .setPositiveButton("Apply", new DialogInterface.OnClickListener() {		    
		    public void onClick(DialogInterface dialog, int which) {
			Map<String, Object> filterParams = new HashMap<String, Object>();
			filterParams.put(Constants.FILTER_PARTNER_NAME, filterPartnerName.getText().toString());
			filterParams.put(Constants.FILTER_PARTNER_CATEGORY, filterPartnerCategory.getSelectedItemPosition());
			filterParams.put(Constants.FILTER_PARTNER_PENDING_ORDERS, filterPendigOrders.getSelectedItemPosition());
			FilterPartnerDialog.this.onFilterPartnerListener.onFilter(dialog, filterParams);
			dialog.dismiss();			
		    }
	   })
	   .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {	    
	       	   public void onClick(DialogInterface dialog, int which) {
	       	       dialog.dismiss();
	       	   }
	    });
	  
        return dialogBuilder.create();
    }
    
    public void setPartnerCategory(Integer partnerCategory){
    	this.partnerCategory = partnerCategory;
    }
    
    public interface OnFilterPartnerListener {
        public void onFilter(DialogInterface dialog, Map<String, Object> filterParams);
    }
}

package com.peoplewalking.warehouse.interfaces;

import java.util.List;

import com.peoplewalking.warehouse.openbravo.model.Product;

public interface OnPostExecuteListener<T> {
    public void onPostExecute(T response, List<String> errorMessages);
}

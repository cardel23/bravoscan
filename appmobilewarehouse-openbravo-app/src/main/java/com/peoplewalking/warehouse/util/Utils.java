package com.peoplewalking.warehouse.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.OBOrderLine;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;

public class Utils {
	public static OBOrderLine findOrderLineByProduct(List<OBOrderLine> orderLines, DeliveryListItem product){
		if(orderLines != null){
			for(OBOrderLine orderline: orderLines){
				if(orderline.getProduct() != null && orderline.getProduct().equals(product.getProduct().getObId())){
					return orderline;
				}
			}
		}
		return null;
	}
	
	public static String floatToString(Float number){
		return (number != null) ? String.valueOf(number) : "N/A";
	}
	
	public static String splitDateTime(String dateTimeString){
		if(dateTimeString != null){
			String[] parts = dateTimeString.split("T");
			return (parts != null && parts.length != 0) ? parts[0] : null;
		}
		return null;
	}
	
	public static String dateToString(Date date){
		if(date != null){
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			return formatter.format(date);	
		}
		return null;
	}
	
	public static String generateScanListName(Context context, boolean listTypeIN){
	    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy - HHmmss");
		String listName;		
		if(listTypeIN){
		    listName = context.getString(R.string.product_list_in, dateFormat.format(new Date()));
		}else{
		    listName = context.getString(R.string.product_list_out, dateFormat.format(new Date()));
		}
		return listName;
	}
	
	public static String generateRequisitionName(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy - HHmmss");
		String listName = "PR" + dateFormat.format(new Date());
		return listName;
		
	}
	
	public static String subString(String text, int lenght){
	    if(text != null){
		if(text.length() > lenght){
		    return text.substring(0, lenght).concat("...");
		} else{
		    return text;
		}
	    }
	    return "";
	}
	
	/**
	 * Crea un identificador aleatorio unico
	 * @return ID hexadecimal para objetos Openbravo
	 * @author Carlos Delgadillo
	 * @fecha 05/08/2015
	 */
	public static String generateObId(){
		UUID rawUid = UUID.randomUUID();
		String stringUid = rawUid.toString();
		String parsedObId = stringUid.replace("-", "");
		return parsedObId.toUpperCase();
	}
	
}

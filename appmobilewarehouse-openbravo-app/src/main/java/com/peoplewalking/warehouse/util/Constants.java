package com.peoplewalking.warehouse.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Constants {
	public static final String ID_EXTRA_SELECTED_ORDER = "SelectedOrder";
	public static final String ID_EXTRA_SELECTED_LIST = "SelectedList";
	public static final String ID_EXTRA_NEW_LIST = "NewList";
	public static final String ID_EXTRA_NEW_REQUISITION = "NewRequisition";
	public static final String ID_EXTRA_CURRENT_GOODRECEIPT = "CurrentGoodReceipt";
	public static final String ID_EXTRA_SELECTED_BPARTNER = "SelectedBpartner";
	public static final int REQUEST_CODE_DELIVERY_LIST = 1;
	public static final int REQUEST_CODE_ORDER = 2;
	public static final int REQUEST_CODE_BPARTNER = 3;
	public static final int REQUEST_CODE_DOCTYPE = 4;
	public static final int REQUEST_CODE_REQUISITION = 4;
	public static final String PRODUCT_IMAGES_DIRNAME = "products";
	public static final String ID_EXTRA_SELECTED_DOCTYPE = "SelectedDocType";
	public static final Integer TOAST_MESSAGE_LENGHT = 80;
	
	public static final String FILTER_START_ROW = "startRow";
	public static final String FILTER_END_ROW = "endRow";

    public static final String FILTER_PARTNER_NAME = "filterPartnerName";
    
    public static final String FILTER_PARTNER_CATEGORY = "filterPartnerCategory";
    public static final Integer PARTNER_CATEGORY_ANY = 0;
    public static final Integer PARTNER_CATEGORY_VENDOR = 1;
    public static final Integer PARTNER_CATEGORY_CLIENT = 2;
    
    public static final String FILTER_PARTNER_PENDING_ORDERS = "filterPartnerPendingOrders";
    public static final Integer PARTNER_PENDING_ORDER_ANY = 0;
    public static final Integer PARTNER_PENDING_ORDER_YES = 1;
    public static final Integer PARTNER_PENDING_ORDER_NO = 2;
    
    public static final String FILTER_ORDER_PARTNER = "filterOrderPartner";
    public static final String FILTER_ORDER_DATE_FROM = "filterOrderDateFrom";
    public static final String FILTER_ORDER_DATE_TO = "filterOrderDateto";
    public static final String FILTER_ORDER_TYPE = "filterOrderType";

	/** Nombres de las entidades**/
	private static final String STORAGE_BIN_ENTITY_NAME = "Locator";
	private static final String REQUISITION_LINE_ENTITY_NAME = "ProcurementRequisitionLine";
	
	private static final HashMap<String, String> ENTITY_NAME_MAP = new HashMap<String, String>();
//	private static List<HashMap<String, String>> ENTITY_NAME_MAP;
	
	
	public static String getClassNames(String entityName){
//		if(ENTITY_NAME_MAP == null){
//			ENTITY_NAME_MAP = new HashMap<String,String>();
			
			/** Agregar los maps con los nombres de entidades y su clase respectiva **/
			ENTITY_NAME_MAP.put(STORAGE_BIN_ENTITY_NAME, "com.peoplewalking.warehouse.openbravo.model.StorageBin");
			ENTITY_NAME_MAP.put(REQUISITION_LINE_ENTITY_NAME, "com.peoplewalking.warehouse.openbravo.model.RequisitionLine");
//			/** Llenar la lista con los maps **/
//			ENTITY_NAME_MAP.add(ENTITY_NAME_MAP);
//		}
		
//		for(HashMap<String, String> h : ENTITY_NAME_MAP ){
			
			if(ENTITY_NAME_MAP.containsKey(entityName))
				return ENTITY_NAME_MAP.get(entityName);
//		}
		return null;
	}
}

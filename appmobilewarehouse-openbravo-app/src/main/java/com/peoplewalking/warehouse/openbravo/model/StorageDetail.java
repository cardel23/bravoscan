/**
 * 
 */
package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Carlos Delgadillo
 *
 */
public class StorageDetail extends OBModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6475370954190757444L;

	@Expose
	@SerializedName("quantityOnHand")
	private Integer quantityOnHand;
	@Expose
	@SerializedName("storageBin")
	private String storageBin;
	@Expose
	@SerializedName("product")
	private String product;
	
	/**
	 * 
	 */
	public StorageDetail() {
		// TODO Auto-generated constructor stub
	}

	public Integer getQuantityOnHand() {
		return quantityOnHand;
	}

	public void setQuantityOnHand(Integer quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}

	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

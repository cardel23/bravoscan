package com.peoplewalking.warehouse.openbravo.api;

import java.util.List;
import java.util.Map;

import com.peoplewalking.warehouse.openbravo.model.*;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;

public interface OpenbravoService {
	public static final int UNAUTHORIZED = 401;
	
	/*
	 * Product Services
	 */
	
	@GET("/Product/{id}")
	public Product getProduct(@Path("id") String productId);
	
	@GET("/Product")
	public Product getProductByUPC(@Query("_where") String query);
	
	@GET("/Product")
	public List<Product> getProductList(@Query("_where") String query);
	
	@GET("/Product")
	public List<Product> getProductList(@QueryMap Map<String, String> params);
	
	/*
	 * Product Image Services
	 */
	@GET("/ADImage/{id}")
	public ProductImage getProductImage(@Path("id") String productId);
	
	
	/*
	 * StoreBin Services
	 */
	
	@GET("/Locator/{id}")
	public StorageBin getStorageBin(@Path("id") String storeBinId);
	
	@GET("/Locator")
	public List<StorageBin> getStorageBinList(@Query("_where") String query);
	
	@GET("/Locator")
	public List<StorageBin> getStorageBinList(@QueryMap Map<String, String> query);
	
	/*
	 * Warehouse Services
	 */
	
	@GET("/Warehouse/{id}")
	public Warehouse getWarehouse(@Path("id") String warehouseId);
	
	@GET("/Warehouse")
	public List<Warehouse> getWarehouseList(@Query("_where") String query);
	
	@GET("/Warehouse")
	public void getWarehouseList(@Query("_where") String query, Callback<List<Warehouse>> cb);
	
	/*
	 * Partner Services
	 */
	
	@GET("/BusinessPartner/{id}")
	public BusinessPartner getBPartner(@Path("id") String partnerId);
	
	@GET("/BusinessPartner")
	public List<BusinessPartner> getBpartnerList(@QueryMap Map<String, String> params);
	
	@GET("/BusinessPartner")
	public List<BusinessPartner> getBpartnerList(@Query("_where") String query);
	
	@GET("/BusinessPartner")
	public void findAllBusinessPartners(Callback<List<BusinessPartner>> cb);
	
	/*
	 * PartnerLocation Services
	 */
	
	@GET("/BusinessPartnerLocation/{id}")
	public BusinessPartnerLocation getBPartnerLocation(@Path("id") String partnerLocationId);
	
	@GET("/BusinessPartnerLocation")
	public List<BusinessPartnerLocation> getBPartnerLocationList(@Query("_where") String query);
	
	
	/*
	 * Order Services
	 */	
	
	@GET("/Order/{id}")
	public OBOrder getOrder(@Path("id") String orderId);
	
	@GET("/Order")
	public List<OBOrder> getOrderList(@QueryMap Map<String, String> parameters);
		
	/*
	 * OrderLine Services
	 */
	
	@GET("/OrderLine/{id}")
	public OBOrderLine getOrderLine(@Path("id") String orderLineId);
		
	@GET("/OrderLine")
	public List<OBOrderLine> getOrderLineList(@Query("_where") String query);
	
	/*
	 * GoodsReceipt Services
	 */
	
	@GET("/MaterialMgmtShipmentInOut/{id}")
	public GoodsReceipt getGoodsReceipt(@Path("id") String goodsReceiptId);
	
	@GET("/MaterialMgmtShipmentInOut")
	public List<GoodsReceipt> getGoodsReceiptList(@Query("_where") String query);
	
	@POST("/MaterialMgmtShipmentInOut")
	public List<GoodsReceipt> saveGoodReceipt(@Body GoodsReceipt goodsReceipt);
	
	
	/*
	 * GoodsReceiptLine Services
	 */
	
	@GET("/MaterialMgmtShipmentInOutLine/{id}")
	public GoodsReceiptLine getGoodsReceiptLine(@Path("id") String goodsReceiptLineId);
	
	@GET("/MaterialMgmtShipmentInOutLine")
	public List<GoodsReceiptLine> getGoodsReceiptLineList(@Query("_where") String query);
	
	@POST("/MaterialMgmtShipmentInOutLine")
	public List<GoodsReceiptLine> saveGoodReceiptLine(@Body GoodsReceiptLine goodsReceiptLine);
	
	/**
	 * Guarda una lista de GoodsReceipts como un arreglo JSON
	 * No se serializan los elementos
	 * @param goodsReceiptsList
	 * @return 
	 */
	@POST("/MaterialMgmtShipmentInOutLine")
	public List<GoodsReceiptLine> saveGoodsReceiptLines(@Body List<GoodsReceiptLine> goodsReceiptLinesList);
	
	/*
	 * ADUser Services	
	 */
	
	@GET("/ADUser/{id}")
	public User getUser(@Path("id") String userId);
	
	@GET("/ADUser")
	public List<User> getUserList(@Query("_where") String query);
	
	@GET("/ADUser")
	public void getUserList(@Query("_where") String query, Callback<List<User>> cb);
	/*
	 * Language Services
	 */
	
	@GET("/ADLanguage/{id}")
	public Language getLanguage(@Path("id") String languageId);
	
	@GET("/ADLanguage")
	public List<Language> getLanguageList(@Query("_where") String query);
	
	/*
	 * Organization Services
	 */
	
	@GET("/Organization/{id}")
	public Organization getOrganization(@Path("id") String organizationId);
	
	@GET("/Organization/{id}")
	public void getOrganization(@Path("id") String organizationId, Callback<Organization> cb);
	
	@GET("/Organization")
	public List<Organization> getOrganizationList(@Query("_where") String query);
	
	/*
	 * DocumentType Services
	 */
	
	@GET("/DocumentType/{id}")
	public DocumentType getDocType(@Path("id") String docTypeId);
	
	@GET("/DocumentType")
	public List<DocumentType> getDocTypeList(@Query("_where") String query);
	
	@GET("/DocumentType")
	public List<DocumentType> getDocTypeList(@QueryMap Map<String, String> params);
	
	@GET("/DocumentType")
	public void getDocTypeList(@Query("_where") String query, Callback<List<DocumentType>> cb);
	
	
	@POST("/MaterialMgmtInventoryCount")
	public List<InventoryCount> saveInventory(@Body InventoryCount inventoryCount);
	
	@POST("/MaterialMgmtInventoryCountLine")
	public List<InventoryCountLine> saveInventoryLines(@Body List<InventoryCountLine> inventoryCountLineList);
	
	@GET("/MaterialMgmtStorageDetail")
	public List<StorageDetail> getStorageDetail(@Query("_where") String query);
	
	@GET("/PricingPriceList")
	public List<PriceList> getPriceLists(@Query("_where") String query);
	
	@GET("/PricingPriceList")
	public List<PriceList> getPriceLists(@QueryMap Map<String, String> parameters);
	
	@POST("/ProcurementRequisition")
	public List<Requisition> saveRequisition(@Body Requisition requisition);
	
	@POST("/ProcurementRequisitionLine")
	public List<RequisitionLine> saveRequisitionLines(@Body List<RequisitionLine> requisitionLines);
	
	@GET("/ProductCategory")
	public List<ProductCategory> getProductCategories(@QueryMap Map<String, String> parameters);
}

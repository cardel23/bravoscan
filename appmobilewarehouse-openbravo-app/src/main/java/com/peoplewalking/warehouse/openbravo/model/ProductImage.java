package com.peoplewalking.warehouse.openbravo.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImage extends OBModel{

    @Expose
    @SerializedName("width")
    private String width;
    @Expose
    @SerializedName("height")
    private String height;
    @Expose
    @SerializedName("imageURL")
    private String imageUrl;
    @Expose
    @SerializedName("mimetype")
    private String mimetype;
    @Expose
    @SerializedName("bindaryData")
    private String bindaryData;
    
    
    public String getObId() {
        return obId;
    }
    public void setObId(String obId) {
        this.obId = obId;
    }
    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getHeight() {
        return height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }    
    public String getMimetype() {
        return mimetype;
    }
    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }
    public String getBindaryData() {
        return bindaryData;
    }
    public void setBindaryData(String bindaryData) {
        this.bindaryData = bindaryData;
    }
    
    /**
     * @param encodedString
     * @return bitmap (from given string)
     */
    public Bitmap getBitmapImage(){
       try{
         byte [] encodeByte=Base64.decode(this.bindaryData,Base64.DEFAULT);
         Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
         return bitmap;
       }catch(Exception e){
         e.getMessage();
         return null;
       }
    }   
    
}

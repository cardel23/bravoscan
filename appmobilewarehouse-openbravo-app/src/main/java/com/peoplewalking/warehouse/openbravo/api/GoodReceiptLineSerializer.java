package com.peoplewalking.warehouse.openbravo.api;

import java.lang.reflect.Type;
import java.util.List;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceiptLine;

public class GoodReceiptLineSerializer implements JsonSerializer<GoodReceiptLineSerializer>{
	public static final String TAG = "GoodReceiptLineSerializer";
	private static GoodReceiptLineSerializer instance;
	private JsonArray array;
	JsonObject objectWrapper = new JsonObject();
	
	public JsonElement serialize(GoodReceiptLineSerializer src, Type typeOfSrc,
			JsonSerializationContext context) {
		
//		try {
		/*	
			Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			JsonParser parser = new JsonParser();
			JsonObject objectWrapper = new JsonObject();
			JsonObject goodReceiptLineJson = parser.parse(gson.toJson(src, GoodsReceiptLine.class)).getAsJsonObject();	
			goodReceiptLineJson.add("shipmentReceipt",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("shipmentReceipt").getAsString() + "}"));
			goodReceiptLineJson.add("businessPartner",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("businessPartner").getAsString() + "}"));
			goodReceiptLineJson.add("product",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("product").getAsString() + "}"));
			
			if(goodReceiptLineJson.get("salesOrderLine") != null){
				goodReceiptLineJson.add("salesOrderLine",
								parser.parse("{\"id\": " + goodReceiptLineJson.get("salesOrderLine").getAsString() + "}"));
//				goodReceiptLineJson.add("orderUOM",
//					parser.parse("{\"id\": '" + goodReceiptLineJson.get("orderUOM").getAsString() + "'}"));//Nota: el campo orderUOM no es serializable. Probando serializar campo
			}
			*/
//			processList(src);
			objectWrapper.add("data", getArray());
			return objectWrapper;
//		} catch (Exception e) {
//			Log.e(TAG, e.getMessage(), e);
//			return null;
//		}
		
	}
	
	public static GoodReceiptLineSerializer instance(){
		if(instance == null)
			instance = new GoodReceiptLineSerializer();
		return instance;
	}
	
	public JsonArray getArray(){
		if(array == null)
			array = new JsonArray();
		return array;
	}
	
	public void processList(List<GoodReceiptLineSerializer> list){
		for(GoodReceiptLineSerializer goodsReceiptLine: list){
			Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			JsonParser parser = new JsonParser();
//			JsonObject objectWrapper = new JsonObject();
			JsonObject goodReceiptLineJson = parser.parse(gson.toJson(goodsReceiptLine, GoodsReceiptLine.class)).getAsJsonObject();	
			goodReceiptLineJson.add("shipmentReceipt",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("shipmentReceipt").getAsString() + "}"));
			goodReceiptLineJson.add("businessPartner",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("businessPartner").getAsString() + "}"));
			goodReceiptLineJson.add("product",
					parser.parse("{\"id\": " + goodReceiptLineJson.get("product").getAsString() + "}"));
			
			if(goodReceiptLineJson.get("salesOrderLine") != null){
				goodReceiptLineJson.add("salesOrderLine",
								parser.parse("{\"id\": " + goodReceiptLineJson.get("salesOrderLine").getAsString() + "}"));
//				goodReceiptLineJson.add("orderUOM",
//					parser.parse("{\"id\": '" + goodReceiptLineJson.get("orderUOM").getAsString() + "'}"));//Nota: el campo orderUOM no es serializable. Probando serializar campo
			}
			getArray().add(goodReceiptLineJson);
		}
	}

}

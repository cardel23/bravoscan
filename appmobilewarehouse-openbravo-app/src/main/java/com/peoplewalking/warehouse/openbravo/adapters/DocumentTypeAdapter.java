package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adaptador para lista de tipos de documento de albarán
 * @author Carlos Delgadillo
 * @fecha 20/05/2015
 *
 */


public class DocumentTypeAdapter extends ArrayAdapter<DocumentType> {
	
	private NovoventApp app;
	private List<DocumentType> docTypes;
	private LayoutInflater layoutInflater;
	private TextView mTextView;
	
	public DocumentTypeAdapter(NovoventApp context, List<DocumentType> objects) {
		super(context, 0, objects);
		this.app = context;
		this.docTypes = objects;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = LayoutInflater.from(app).inflate(android.R.layout.simple_list_item_1, parent, false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setSingleLine(false);
		mTextView.setText(docTypes.get(position).toString());
		mTextView.setTextColor(Color.BLACK);
		return view;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		layoutInflater = (LayoutInflater) app.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setText(docTypes.get(position).toString());
		mTextView.setTextColor(Color.BLACK);
		return view;
	}
	
	@Override
	public DocumentType getItem(int position) {
		// TODO Auto-generated method stub
		return docTypes.get(position);
	}
	
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
	}
	

}

package com.peoplewalking.warehouse.openbravo.model;

import java.util.ArrayList;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Ignore;

public class GoodsReceiptLine extends OBModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2923049229922123011L;
	
	@Expose
	@SerializedName(value = "movementDate")
	private String movementDate;
	@Expose
	@SerializedName(value = "lineNo")
	private Integer lineNo;
	@Expose
	@SerializedName(value = "storageBin")
	private String storageBinId;
	@Expose
	@SerializedName(value = "storageBin$_identifier")
	private String storageBinIdentifier;
	@Expose
	@SerializedName(value = "shipmentReceipt")
	private String goodReceiptId;
	@Expose
	@SerializedName(value = "shipmentReceipt$_identifier")
	private String goodReceiptIdentifier;
	@Expose
	@SerializedName(value = "businessPartner")
	private String businessPartnerId;	
	@Expose
	@SerializedName(value = "businessPartner$_identifier")
	private String businessPartnerIdentifier;	
	@SerializedName(value = "orderUOM")
	private String orderUOM;
	@Expose
	@SerializedName(value = "salesOrderLine")
	private String salesOrderLine;
	@SerializedName(value = "orderQuantity")
	private Float orderedQuantity;
	@Expose
	@SerializedName(value = "product")
	private String productId;
	@Expose
	@SerializedName(value = "product$_identifier")
	private String productIdentifier;
	
	@Ignore
	private Product product;
	
	@Expose
	@SerializedName(value = "movementQuantity")
	private Float movementQuantity;

	@Expose
	@SerializedName(value = "uOM")
	private String uOMId;
	@Expose
	@SerializedName(value = "uOM$_identifier")
	private String uOMIdentifier;
		
	private ArrayList<String>  errors;
	
	public GoodsReceiptLine(){
		this.entityName = "MaterialMgmtShipmentInOutLine";
	}
	
	public String getMovementDate() {
		return movementDate;
	}
	public void setMovementDate(String movementDate) {
		this.movementDate = movementDate;
	}
	public Integer getLineNo() {
		return lineNo;
	}
	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}
	public String getStorageBinId() {
		return storageBinId;
	}
	public void setStorageBinId(String storageBinId) {
		this.storageBinId = storageBinId;
	}
	public String getStorageBinIdentifier() {
		return storageBinIdentifier;
	}
	public void setStorageBinIdentifier(String storageBinIdentifier) {
		this.storageBinIdentifier = storageBinIdentifier;
	}
	public String getGoodReceiptId() {
		return goodReceiptId;
	}
	public void setGoodReceiptId(String goodReceiptId) {
		this.goodReceiptId = goodReceiptId;
	}
	public String getGoodReceiptIdentifier() {
		return goodReceiptIdentifier;
	}
	public void setGoodReceiptIdentifier(String goodReceiptIdentifier) {
		this.goodReceiptIdentifier = goodReceiptIdentifier;
	}
	public String getBusinessPartnerId() {
		return businessPartnerId;
	}
	public void setBusinessPartnerId(String businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}
	public String getBusinessPartnerIdentifier() {
		return businessPartnerIdentifier;
	}
	public void setBusinessPartnerIdentifier(String businessPartnerIdentifier) {
		this.businessPartnerIdentifier = businessPartnerIdentifier;
	}
	public String isSalesOrderLine() {
		return salesOrderLine;
	}
	public void setSalesOrderLine(String salesOrderLine) {
		this.salesOrderLine = salesOrderLine;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductIdentifier() {
		return productIdentifier;
	}
	public void setProductIdentifier(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Float getMovementQuantity() {
		return movementQuantity;
	}
	public void setMovementQuantity(Float movementQuantity) {
		this.movementQuantity = movementQuantity;
	}	
	public Float getOrderedQuantity() {
		return orderedQuantity;
	}
	public void setOrderedQuantity(Float orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}	
	
	public String getuOMId() {
		return uOMId;
	}
	public void setuOMId(String uOMId) {
		this.uOMId = uOMId;
	}
	public String getuOMIdentifier() {
		return uOMIdentifier;
	}
	public void setuOMIdentifier(String uOMIdentifier) {
		this.uOMIdentifier = uOMIdentifier;
	}
	public String getSalesOrderLine() {
		return salesOrderLine;
	}
	public ArrayList<String> getErrors() {
		return errors;
	}
	public void setErrors(ArrayList<String> errors) {
		this.errors = errors;
	}	
	public String getOrderUOM() {
	    return orderUOM;
	}
	public void setOrderUOM(String orderUOM) {
	    this.orderUOM = orderUOM;
	}
	public boolean isValid() {
		boolean valid = true;
		
		if(this.lineNo == null){
			valid = false;
			this.errors.add("El campo LineNo es requerido");
		}
		
		if(this.productId == null){
			valid = false;
			this.errors.add("El campo Product es requerido");
			}
		if(this.movementDate == null){
			valid = false;
			this.errors.add("El campo movementDate es requerido");
		}
		
		if(this.movementQuantity == null){
			valid =false;
			this.errors.add("El campo movementQuantity es requerido");
		}
		return valid;
	}
	
	
}

package com.peoplewalking.warehouse.openbravo.api;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.peoplewalking.warehouse.openbravo.model.Requisition;

public class RequisitionSerializer implements JsonSerializer<Requisition> {

	public RequisitionSerializer() {
		// TODO Auto-generated constructor stub
	}

	public JsonElement serialize(Requisition src, Type typeOfSrc,
			JsonSerializationContext context) {
		Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		JsonParser parser = new JsonParser();
		JsonObject objectWrapper = new JsonObject();
		JsonObject goodReceiptJson = parser.parse(gson.toJson(src, Requisition.class)).getAsJsonObject();
		objectWrapper.add("data", goodReceiptJson);
		return objectWrapper;
	}

}

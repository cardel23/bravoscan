package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OBOrder extends OBModel implements Serializable {
	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = -374578588586117306L;
	
	public static final int PURCHASE_TYPE = 0;
	public static final int SALES_TYPE = 1;
	
	@Expose
	@SerializedName("documentNo")
	private String documentNo;
	@Expose
	@SerializedName("processed")
	private boolean processed;
	@Expose
	@SerializedName("summedLineAmount")
	private double summedLineAmount;
	@Expose
	@SerializedName("businessPartner")
	private String businessPartnerId;
	@Expose
	@SerializedName("businessPartner$_identifier")
	private String businessPartnerIdentifier;
	@Expose
	@SerializedName("documentType")
	private String docTypeId;
	@Expose
	@SerializedName("documentType$_identifier")
	private String docTypeIdentifier;
	@Expose
	@SerializedName("currency")
	private String currencyId;
	@Expose
	@SerializedName("currency$_identifier")
	private String currencyIdentifier;
	
	public OBOrder(){
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}	

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	
	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public double getSummedLineAmount() {
		return summedLineAmount;
	}

	public void setSummedLineAmount(double summedLineAmount) {
		this.summedLineAmount = summedLineAmount;
	}

	public String getBusinessPartnerId() {
		return businessPartnerId;
	}

	public void setBusinessPartnerId(String businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}

	public String getBusinessPartnerIdentifier() {
		return businessPartnerIdentifier;
	}

	public void setBusinessPartnerIdentifier(String businessPartnerIdentifier) {
		this.businessPartnerIdentifier = businessPartnerIdentifier;
	}

	public String getDocTypeId() {
		return docTypeId;
	}

	public void setDocTypeId(String docTypeId) {
		this.docTypeId = docTypeId;
	}

	public String getDocTypeIdentifier() {
		return docTypeIdentifier;
	}

	public void setDocTypeIdentifier(String docTypeIdentifier) {
		this.docTypeIdentifier = docTypeIdentifier;
	}
	
	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getCurrencyIdentifier() {
		return currencyIdentifier;
	}

	public void setCurrencyIdentifier(String currencyIdentifier) {
		this.currencyIdentifier = currencyIdentifier;
	}

	@Override
	public String toString() {
		return "Order [documentNo=" + documentNo + "]";
	}

}

package com.peoplewalking.warehouse.openbravo.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Organization extends OBModel {

	@Expose
	@SerializedName("name")
	private String name;
	@Expose
	@SerializedName("client$_identifier")
	private String clientIdentifier;
	
	public Organization(){
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientIdentifier() {
		return clientIdentifier;
	}

	public void setClientIdentifier(String clientIdentifier) {
		this.clientIdentifier = clientIdentifier;
	}

	@Override
	public String toString() {
		return "Organization [name=" + name + "]";
	}
	
	

}

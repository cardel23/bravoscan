package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.openbravo.model.GoodsReceiptLine;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;
import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.R.color;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GoodsReceiptLineAdapter extends ArrayAdapter<GoodsReceiptLine>{
    	private static final String TAG = "GoodsReceiptLineAdapter";
	private Context context;
	private List<GoodsReceiptLine> goodsReceiptLineList;
	private LayoutInflater layoutInflater;
	private ImageView mViewProductImage;
	private TextView mViewProductName;
	private TextView mViewOrderedQuantity;
	private TextView mViewReceivedQuantity, line1, line2;
	private RelativeLayout mViewContainer;
	private NovoventApp app;
	private boolean matchedToOrder;
	private SparseBooleanArray mSelectedItemsIds;
	
	public GoodsReceiptLineAdapter(Context context, List<GoodsReceiptLine> goodReceiptLines, Boolean matchedToOrder){
		super(context, R.layout.item_receipt_line);
		this.context = context;
		this.goodsReceiptLineList = goodReceiptLines;
		this.matchedToOrder = matchedToOrder;
		app = (NovoventApp) context.getApplicationContext();
		mSelectedItemsIds = new SparseBooleanArray();
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		GoodsReceiptLine currentGoodReceiptLine= this.goodsReceiptLineList.get(position);
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(R.layout.item_receipt_line, parent, false);
		mViewProductName = (TextView) view.findViewById(R.id.product_name);
		mViewOrderedQuantity = (TextView) view.findViewById(R.id.ordered_quantity);
		mViewReceivedQuantity = (TextView) view.findViewById(R.id.received_quantity);
		mViewContainer = (RelativeLayout) view.findViewById(R.id.container_receipt_line);
		mViewProductImage = (ImageView) view.findViewById(R.id.product_image);
		line1 = (TextView) view.findViewById(R.id.textView2);
		line2 = (TextView) view.findViewById(R.id.textView4);
 		if(currentGoodReceiptLine != null){
 			line1.setText(context.getString(R.string.ordered_quantity));
 			line2.setText(context.getString(R.string.received_quantity));
			mViewProductName.setText(currentGoodReceiptLine.getProduct().toString());
			mViewOrderedQuantity.setText(Utils.floatToString(currentGoodReceiptLine.getOrderedQuantity()));
			mViewReceivedQuantity.setText(Utils.floatToString(currentGoodReceiptLine.getMovementQuantity()));
			mViewProductImage.setImageBitmap(app.loadImageFromInternalStorage(currentGoodReceiptLine.getProduct().getRelativeLocalImagePath()));
		}
 		int bgColor = Color.TRANSPARENT;
 		if(matchedToOrder){
 		    if(currentGoodReceiptLine.getOrderedQuantity() == null || currentGoodReceiptLine.getOrderedQuantity() > currentGoodReceiptLine.getMovementQuantity()){
 			bgColor = color.non_matched_receipt_line;
 		    }else if( currentGoodReceiptLine.getOrderedQuantity() < currentGoodReceiptLine.getMovementQuantity()){
			bgColor = color.invalid_greater_quantity;
 		    }
 		}
 		try {
 		   view.setBackgroundResource(mSelectedItemsIds.get(position) ? android.R.color.holo_blue_light
 				: bgColor);
		} catch (Exception e) {
		   Log.e(TAG, e.getMessage(), e);
		}
		return view;
	}	
	
	public void remove(int position){
	    if(goodsReceiptLineList != null && !goodsReceiptLineList.isEmpty()){
		 goodsReceiptLineList.remove(position);
		 notifyDataSetChanged();
	    }
	}
	
	@Override
	public int getCount() {
        	return this.goodsReceiptLineList.size();
	}
	
	@Override
	public GoodsReceiptLine getItem(int position) {
	    if(goodsReceiptLineList != null && !goodsReceiptLineList.isEmpty()){
		return goodsReceiptLineList.get(position);
	    }
	    return null;
	}
	
	public void removeSelection() {
	        mSelectedItemsIds = new SparseBooleanArray();
	        notifyDataSetChanged();
	}
	
	public void toggleSelection(int position){
		selectView(position, !mSelectedItemsIds.get(position));
	}
	
	public void selectView(int position, boolean value) {
	        if (value)
	            mSelectedItemsIds.put(position, value);
	        else
	            mSelectedItemsIds.delete(position);
	 
	        notifyDataSetChanged();
	}
	
	public int getSelectedCount() {
	     return mSelectedItemsIds.size();
	}
	 
	public SparseBooleanArray getSelectedIds() {
	     return mSelectedItemsIds;
	}
	
}

package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;

import com.orm.SugarRecord;

public class DeliveryListItem extends SugarRecord<DeliveryListItem>  implements Serializable{

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = -5885031890705471007L;
	
	private DeliveryList listId;
	private float receivedAmount;
	private Integer onHandAmount;
	private Product product;
	private String bin;
	private String binId;
	
	public DeliveryListItem() {
	}
	
	public DeliveryListItem(Product product) {
		this.product = product;
	}

	public DeliveryList getListId() {
		return listId;
	}

	public void setListId(DeliveryList listId) {
		this.listId = listId;
	}

	public float getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(float receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return String.valueOf(product);
	}

	public Integer getOnHandAmount() {
		return onHandAmount;
	}

	public void setOnHandAmount(Integer onHandAmount) {
		this.onHandAmount = onHandAmount;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}
	
	public String getBinId() {
		return binId;
	}

	public void setBinId(String binId) {
		if(binId != null)
			this.binId = binId;
		else
			this.binId = "";
	}

}

package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryCountLine extends OBModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5257797708090186917L;
	
	@Expose
	@SerializedName("physInventory")
	private String inventory;
	@Expose
	@SerializedName("physInventory$_identifier")
	private String inventoryIdentifier;
	@Expose
	@SerializedName("storageBin")
	private String storageBin;
	@Expose
	@SerializedName("storageBin$_identifier")
	private String storageBinIdentifier;
	@Expose
	@SerializedName("product")
	private String product;
	@Expose
	@SerializedName("product$_identifier")
	private String productIdentifier;
	@Expose
	@SerializedName("lineNo")
	private Integer lineNo;
	@Expose
	@SerializedName("quantityCount")
	private Integer quantityCount;
	@Expose
	@SerializedName("bookQuantity")
	private Integer bookQuantity;
	@Expose
	@SerializedName("uOM")
	private String uOM;
	@Expose
	@SerializedName("uOM$_identifier")
	private String uOMIdentifier;
	
	
	

	public InventoryCountLine() {
		// TODO Auto-generated constructor stub
		this.entityName = "MaterialMgmtInventoryCountLine";
	}




	public Integer getBookQuantity() {
		return bookQuantity;
	}




	public void setBookQuantity(Integer bookQuantity) {
		this.bookQuantity = bookQuantity;
	}




	public String getInventory() {
		return inventory;
	}




	public void setInventory(String inventory) {
		this.inventory = inventory;
	}




	public String getInventoryIdentifier() {
		return inventoryIdentifier;
	}




	public void setInventoryIdentifier(String inventoryIdentifier) {
		this.inventoryIdentifier = inventoryIdentifier;
	}




	public String getStorageBin() {
		return storageBin;
	}




	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}




	public String getStorageBinIdentifier() {
		return storageBinIdentifier;
	}




	public void setStorageBinIdentifier(String storageBinIdentifier) {
		this.storageBinIdentifier = storageBinIdentifier;
	}




	public String getProduct() {
		return product;
	}




	public void setProduct(String product) {
		this.product = product;
	}




	public String getProductIdentifier() {
		return productIdentifier;
	}




	public void setProductIdentifier(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}




	public Integer getLineNo() {
		return lineNo;
	}




	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}




	public Integer getQuantityCount() {
		return quantityCount;
	}




	public void setQuantityCount(Integer quantityCount) {
		this.quantityCount = quantityCount;
	}




	public String getuOM() {
		return uOM;
	}




	public void setuOM(String uOM) {
		this.uOM = uOM;
	}




	public String getuOMIdentifier() {
		return uOMIdentifier;
	}




	public void setuOMIdentifier(String uOMIdentifier) {
		this.uOMIdentifier = uOMIdentifier;
	}

}

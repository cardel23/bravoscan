package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;




public class BusinessPartnerLocation extends OBModel {

	@Expose
	@SerializedName("bussinesPartner")
	private String businessPartnerId;
	@Expose
	@SerializedName("businessPartner$_identifier")
	private String businessPartnerIdentifier;
	@Expose
	@SerializedName("locationAddress")
	private String locationAddress;
	
	public String getObId() {
		return obId;
	}
	public void setObId(String id) {
		this.obId = id;
	}	
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getBusinessPartnerId() {
		return businessPartnerId;
	}
	public void setBusinessPartnerId(String businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}
	public String getBusinessPartnerIdentifier() {
		return businessPartnerIdentifier;
	}
	public void setBusinessPartnerIdentifier(String businessPartnerIdentifier) {
		this.businessPartnerIdentifier = businessPartnerIdentifier;
	}
	public String getLocationAddress() {
		return locationAddress;
	}
	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}
}

package com.peoplewalking.warehouse.openbravo.api;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.InventoryCount;

public class InventoryCountSerializer implements JsonSerializer<InventoryCount> {

	public InventoryCountSerializer() {
		// TODO Auto-generated constructor stub
	}

	public JsonElement serialize(InventoryCount src, Type typeOfSrc,
			JsonSerializationContext context) {
		Gson gson =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		JsonParser parser = new JsonParser();
		JsonObject objectWrapper = new JsonObject();
		JsonObject goodReceiptJson = parser.parse(gson.toJson(src, InventoryCount.class)).getAsJsonObject();	
//		goodReceiptJson.add("businessPartner",
//				parser.parse("{\"id\": " + goodReceiptJson.get("businessPartner").getAsString() + "}"));
//		goodReceiptJson.add("partnerAddress",
//				parser.parse("{\"id\": " + goodReceiptJson.get("partnerAddress").getAsString() + "}"));
//		goodReceiptJson.add("warehouse",
//						parser.parse("{\"id\": " + goodReceiptJson.get("warehouse").getAsString() + "}"));
//		goodReceiptJson.add("documentType",
//				parser.parse("{\"id\": "+ goodReceiptJson.get("documentType").getAsString() + "}"));

		objectWrapper.add("data", goodReceiptJson);
		return objectWrapper;
	}

}

package com.peoplewalking.warehouse.openbravo.api;

import java.util.List;
import java.util.concurrent.TimeUnit;

import android.util.Base64;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceiptLine;
import com.peoplewalking.warehouse.openbravo.model.InventoryCount;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.openbravo.model.StorageDetail;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.openbravo.model.Warehouse;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class OBServiceClient {
	private static NovoventApp app;
	public static String OB_SERVER;
	private static String OB_PORT;
	public static String OB_URL = "http://"+OB_SERVER+"/openbravo";
//    	public static final String OB_SERVER = "http://52.16.177.26/openbravo";
//    	public static final String OB_SERVER = "http://192.168.10.104:8080/openbravo";
    public static final String REST_BASE_URL = OB_SERVER + "/org.openbravo.service.json.jsonrest";
    public static final String SHOWIMAGE_URL = OB_SERVER + "/utility/ShowImage";   
	private static OpenbravoService obService = null;

	private OBServiceClient() {
	};

	public static OpenbravoService initializeService(String username,
			String password) {
		app = NovoventApp.getInstance();
		//Authenticator authenticator = new OBAuthenticator(username, password);
		OkHttpClient httpClient = new OkHttpClient();
		httpClient.setReadTimeout(40, TimeUnit.SECONDS);
		httpClient.setConnectTimeout(40, TimeUnit.SECONDS);
		OkClient client = new OkClient(httpClient);
		Gson gson = new GsonBuilder().setFieldNamingPolicy(
						FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
						.excludeFieldsWithoutExposeAnnotation()
				.registerTypeAdapter(List.class, new OBListDeserializer<User>())
				.registerTypeAdapter(List.class, new OBListDeserializer<Warehouse>())
				.registerTypeAdapter(List.class, new OBListDeserializer<Product>())		
				.registerTypeAdapter(List.class, new OBListDeserializer<GoodsReceipt>())		
				.registerTypeAdapter(List.class, new OBListDeserializer<GoodsReceiptLine>())
				.registerTypeAdapter(List.class, new OBListDeserializer<DocumentType>())
				.registerTypeAdapter(List.class, new OBListDeserializer<StorageBin>())
				.registerTypeAdapter(List.class, new OBListDeserializer<StorageDetail>())
				.registerTypeAdapter(List.class, new OBListDeserializer<PriceList>())
				.registerTypeAdapter(GoodsReceipt.class, new GoodReceiptSerializer())
				.registerTypeAdapter(Requisition.class, new RequisitionSerializer())
				.registerTypeAdapter(InventoryCount.class, new InventoryCountSerializer())
//				.registerTypeAdapter(List.class, GoodReceiptLineSerializer.instance()) //No se necesita serializar los elementos
				.create();
		
		RestAdapter.Builder builder = new RestAdapter.Builder()
				.setEndpoint(getRestString())
				.setClient(client)
				.setConverter(new GsonConverter(gson))
				.setLogLevel(RestAdapter.LogLevel.FULL);
		
		if (username != null && password != null) {            
		    	// concatenate username and password with colon for authentication
                        final String credentials = username + ":" + password;
            
                        builder.setRequestInterceptor(new RequestInterceptor() {
                            
                            public void intercept(RequestInterceptor.RequestFacade request) {
                                // create Base64 encoded string
                                String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                                request.addHeader("Authorization", string);
                                request.addHeader("Accept", "application/json");
                            }
                        });
                  }

		RestAdapter adapter = builder.build();
		obService = adapter.create(OpenbravoService.class);
		return obService;
	}

	public static OpenbravoService getOBService() {
		return obService;
	}
	
	public final static String getOB_SERVER(){
		return app.getServerFromContext();
	}
	
	public final static String getOB_URL(){
		return "http://"+getOB_SERVER()+":"+getOB_PORT()+"/openbravo";
	}
	
	public final static String getRestString(){
		return getOB_URL() + "/org.openbravo.service.json.jsonrest";
	}
	
	public final static String getOB_PORT(){
		OB_PORT = String.valueOf(app.getApplicationPort());
		return OB_PORT;
	}
	

}

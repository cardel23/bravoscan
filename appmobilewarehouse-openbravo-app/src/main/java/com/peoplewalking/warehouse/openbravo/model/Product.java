package com.peoplewalking.warehouse.openbravo.model;

import java.lang.reflect.Field;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product extends OBModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7847029475816858650L;

	@Expose
	@SerializedName("searchKey")
	private String searchKey;
	@Expose
	@SerializedName("name")
	private String title;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("uOM")
	private String uomId;
	@Expose
	@SerializedName("uOM$_identifier")
	private String uomIdentifier;
	@Expose
	@SerializedName("image")
	private String imageId;
	@Expose
	@SerializedName("uPCEAN")
	private String upcean;
	@Expose
	@SerializedName("storageBin")
	private String storageBin;
	@Expose
	@SerializedName("storageBin$_identifier")
	private String storageBinIdentifier;
	
	private String relativeLocalImagePath;
	
	//No serializados para SQLite
	private long listId;
	private double receivedAmount;
	
	public Product() {
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUomId() {
		return uomId;
	}

	public void setUomId(String uomId) {
		this.uomId = uomId;
	}

	public String getUomIdentifier() {
		return uomIdentifier;
	}

	public void setUomIdentifier(String uomIdentifier) {
		this.uomIdentifier = uomIdentifier;
	}

	public long getListId() {
		return listId;
	}

	public void setListId(long listId) {
		this.listId = listId;
	}

	public double getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(double receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public String getuPCEAN() {
		return upcean;
	}

	public void setuPCEAN(String uPCEAN) {
		this.upcean = uPCEAN;
	}
	
	public String getImageId() {
	    return imageId;
	}

	public void setImageId(String imageId) {
	    this.imageId = imageId;
	}	

	public String getRelativeLocalImagePath() {
	    return relativeLocalImagePath;
	}

	public void setRelativeLocalImagePath(String localImagePathName) {
	    this.relativeLocalImagePath = localImagePathName;
	}
	
	public String getStorageBin() {
		return storageBin;
	}

	public void setStorageBin(String storageBin) {
		this.storageBin = storageBin;
	}

	public String getStorageBinIdentifier() {
		return storageBinIdentifier;
	}

	public void setStorageBinIdentifier(String storageBinIdentifier) {
		this.storageBinIdentifier = storageBinIdentifier;
	}

	public void updateFields(Product p){
		this.description = p.getDescription();
		this.entityName = p.getEntityName();
		this.imageId = p.getImageId();
		this.listId = p.getListId();
		this.relativeLocalImagePath = p.getRelativeLocalImagePath();
		this.obId = p.getObId();
		this.receivedAmount = p.getReceivedAmount();
		this.searchKey = p.getSearchKey();
		this.title = p.getTitle();
		this.uomId = p.getUomId();
		this.uomIdentifier = p.getUomIdentifier();
		this.upcean = p.getuPCEAN();
		this.storageBin = p.getStorageBin();
		this.storageBinIdentifier = p.getStorageBinIdentifier();
	}

	public static boolean checkFields(Object o) throws IllegalAccessException, IllegalArgumentException{
		Field[] fields = o.getClass().getDeclaredFields();
		for(Field f : fields){
			f.setAccessible(true);
			if(f.get(o) != null){
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		if(getTitle() != null){
			return getTitle();
		}
		else if(getDescription() != null){
			return getDescription();
		}
		else
			return getuPCEAN();
	}

}

package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.R.string;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adaptador que se encarga de crear un list item de 2 lineas para las listas de productos escaneados.
 * La primera línea muestra el código de barras o nombre del producto y la segunda la cantidad recibida
 * @author Carlos Delgadillo
 * fecha 26/04/2015
 *
 */
public class ProductListAdapter extends ArrayAdapter<DeliveryListItem> {
	
	private SparseBooleanArray mSelectedItemsIds;
	private List<DeliveryListItem> products;
	private TextView title, subtitle, bin;
	private ImageView image;
	private NovoventApp app;
	
	public ProductListAdapter(Context context, List<DeliveryListItem> products) {
		super(context, 0, products);
		mSelectedItemsIds = new SparseBooleanArray();
		this.products = products;
		app = (NovoventApp) context.getApplicationContext();
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		Context context = this.getContext().getApplicationContext();
		DeliveryListItem item = getItem(position);    
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_delivery_list_detail, parent, false);
		}       
		title = (TextView) convertView.findViewById(R.id.textItem1);
		subtitle = (TextView) convertView.findViewById(R.id.textItem2);
		bin = (TextView) convertView.findViewById(R.id.textItem3);
		image = (ImageView) convertView.findViewById(R.id.product_image);
		title.setText(item.toString());
//		subtitle.setText("Cantidad: "+String.valueOf(product.getReceivedAmount()));
		subtitle.setText(context.getResources().getString(R.string.scan_amount, (int)item.getReceivedAmount()));
		bin.setVisibility(View.VISIBLE);
		bin.setText(context.getResources().getString(R.string.assigned_bin, item.getBinId()));
		title.setTextSize(20);
		subtitle.setTextSize(16);
		bin.setTextSize(16);
		try {
			image.setImageBitmap(app.loadImageFromInternalStorage(item.getProduct().getRelativeLocalImagePath()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		convertView
		.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
				: Color.TRANSPARENT);
		return convertView;
	}
	
	public void toggleSelection(int position){
		selectView(position, !mSelectedItemsIds.get(position));
	}
	
	public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
	
	public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
 
        notifyDataSetChanged();
    }
 
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }
 
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
	

}

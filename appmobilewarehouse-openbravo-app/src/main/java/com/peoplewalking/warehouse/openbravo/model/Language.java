package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Language extends OBModel {

	@Expose
	@SerializedName(value = "name")
	private String name;
	
	public Language(){
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Language [name=" + name + "]";
	}

}

package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StorageBinAdapter extends ArrayAdapter<StorageBin> {

	private NovoventApp app;
	private TextView title;
	private List<StorageBin> storageBins;
	
	public StorageBinAdapter(Context context, List<StorageBin> bins) {
		super(context, 0, bins);
		app = (NovoventApp) context.getApplicationContext();
		// TODO Auto-generated constructor stub
		if(!bins.get(0).getSearchKey().equals("") && bins.get(0).getSearchKey() != app.getString(R.string.select_item) ){
			StorageBin blankOption = new StorageBin();
			blankOption.setSearchKey(app.getString(R.string.select_item));
			bins.add(0, blankOption);
		}
		storageBins = bins;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(app).inflate(android.R.layout.simple_spinner_item, parent, false);
		setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		title = (TextView) convertView.findViewById(android.R.id.text1);
		title.setText(storageBins.get(position).toString());
		title.setTextColor(Color.BLACK);
		return convertView;
	}
	
	public int getPosition(String item){
		int position = 0;
		try {
			for (StorageBin s : storageBins){
				if(s.getObId() == null)
					continue;
				else
				if(s.getIdentifier().equals(item)){
					position = getPosition(s);
					break;
				}
			}
			return position;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return position;
		}		
	}
}

package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.RequisitionLine;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adaptador que maneja las lineas de necesidades de material
 * @author Carlos Delgadillo
 * fecha 03/08/2015
 *
 */
public class RequisitionLineAdapter extends ArrayAdapter<RequisitionLine> {

	private SparseBooleanArray mSelectedItemsIds;
	List<RequisitionLine> requisitionsList;
	Context context;
	private TextView title, subtitle, type;
	
	public RequisitionLineAdapter(Context context, List<RequisitionLine> requisitionList) {
		super(context, 0, requisitionList);
		mSelectedItemsIds = new SparseBooleanArray();
		this.requisitionsList = requisitionList;
		this.context = context;
	}
	
	public RequisitionLineAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = this.getContext().getApplicationContext();
		RequisitionLine requisitionLine = getItem(position);    
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_delivery_list, parent, false);
		}       
		title = (TextView) convertView.findViewById(R.id.textItem1);
		subtitle = (TextView) convertView.findViewById(R.id.textItem2);
		type = (TextView) convertView.findViewById(R.id.textItem3);
		type.setVisibility(View.GONE);
		title.setText(requisitionLine.toString());
		title.setTextColor(Color.BLACK);
		title.setTextSize(20);
//		subtitle.setTextSize(16);
		subtitle.setText(context.getString(R.string.n_products, requisitionLine.getQuantity()));
		convertView
		.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
		: Color.TRANSPARENT);
		return convertView;
	}
	
	public void toggleSelection(int position){
		selectView(position, !mSelectedItemsIds.get(position));
	}
	
	public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
	
	public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
 
        notifyDataSetChanged();
    }
 
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }
 
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
	

}

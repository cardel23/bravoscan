package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequisitionLine extends OBModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4513270853136994833L;
	
	@Expose
	@SerializedName(value = "lineNo")
	private Integer lineNo;
	@Expose
	@SerializedName(value = "requisition")
	private String requisition;
	@Expose
	@SerializedName(value = "product")
	private String product;
	@Expose
	@SerializedName(value = "businessPartner")
	private String businessPartner;
	@Expose
	@SerializedName(value = "priceList")
	private String priceList;
	@Expose
	@SerializedName(value = "needByDate")
	private String needByDate;
	@Expose
	@SerializedName(value = "unitPrice")
	private Float unitPrice;
	@Expose
	@SerializedName(value = "listPrice")
	private float listPrice;
	@Expose
	@SerializedName(value = "quantity")
	private int quantity;
	@Expose
	@SerializedName(value = "discount")
	private Float discount;
	@Expose
	@SerializedName(value = "description")
	private String description;
	@Expose
	@SerializedName(value = "name")
	private String name;

	private Long requisitionId;
	
	public RequisitionLine() {
		// TODO Auto-generated constructor stub
	}
	
	
	public RequisitionLine(String entityName){
		this.entityName = entityName;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getBusinessPartner() {
		return businessPartner;
	}


	public void setBusinessPartner(String businessPartner) {
		this.businessPartner = businessPartner;
	}

	
	public String getPriceList() {
		return priceList;
	}


	public void setPriceList(String priceList) {
		this.priceList = priceList;
	}


	public String getNeedByDate() {
		return needByDate;
	}


	public void setNeedByDate(String needByDate) {
		this.needByDate = needByDate;
	}


	public Float getUnitPrice() {
		return unitPrice;
	}


	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}


	public float getListPrice() {
		return listPrice;
	}


	public void setListPrice(float listPrice) {
		this.listPrice = listPrice;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public Float getDiscount() {
		return discount;
	}


	public void setDiscount(Float discount) {
		this.discount = discount;
	}


	public Long getRequisitionId() {
		return requisitionId;
	}


	public void setRequisitionId(Long requisitionId) {
		this.requisitionId = requisitionId;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getLineNo() {
		return lineNo;
	}


	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}


	public String getRequisition() {
		return requisition;
	}


	public void setRequisition(String requisition) {
		this.requisition = requisition;
	}
	


	@Override
	public String toString() {
		
		return name;
	}
	
}

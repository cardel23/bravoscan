package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.RequisitionLine;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adaptador que maneja las necesidades de material
 * @author Carlos Delgadillo
 * fecha 29/07/2015
 *
 */
public class RequisitionAdapter extends ArrayAdapter<Requisition> {

	private SparseBooleanArray mSelectedItemsIds;
	List<Requisition> requisitionsList;
	Context context;
	private TextView title, subtitle, type;
	
	public RequisitionAdapter(Context context, List<Requisition> requisitionList) {
		super(context, 0, requisitionList);
		mSelectedItemsIds = new SparseBooleanArray();
		this.requisitionsList = requisitionList;
		this.context = context;
	}
	
	public RequisitionAdapter(Context context, int resource) {
		super(context, resource);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			Context context = this.getContext().getApplicationContext();
			Requisition requisition = getItem(position);    
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_delivery_list, parent, false);
			}       
			title = (TextView) convertView.findViewById(R.id.textItem1);
			subtitle = (TextView) convertView.findViewById(R.id.textItem2);
			type = (TextView) convertView.findViewById(R.id.textItem3);
			type.setVisibility(View.GONE);
			title.setText(requisition.toString());
			title.setTextColor(Color.BLACK);
			title.setTextSize(20);
//			subtitle.setTextSize(16);
			long numProducts = RequisitionLine.count(RequisitionLine.class, "requisition_id = ?", new String[]{ String.valueOf(requisition.getId()) });
			subtitle.setText(context.getString(R.string.n_products, numProducts));
			convertView
			.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
			: Color.TRANSPARENT);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return convertView;
	}
	
	public void toggleSelection(int position){
		selectView(position, !mSelectedItemsIds.get(position));
	}
	
	public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
	
	public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
 
        notifyDataSetChanged();
    }
 
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }
 
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
	

}

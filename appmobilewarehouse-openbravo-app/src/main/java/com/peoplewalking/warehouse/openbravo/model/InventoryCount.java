package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryCount extends OBModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1086908589984820474L;
	
	@Expose
	@SerializedName("name")
	private String name;
	@Expose
	@SerializedName("warehouse")
	private String warehouse;
	@Expose
	@SerializedName("warehouse$_identifier")
	private String warehouseIdentifier;
	@Expose
	@SerializedName("organization")
	private String organization;
	@Expose
	@SerializedName("organization$_identifier")
	private String organizationIdentifier;
	@Expose
	@SerializedName("movementDate")
	private String movementDate;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("inventoryType")
	private String inventoryType;
	

	public InventoryCount() {
		// TODO Auto-generated constructor stub
		this.entityName = "MaterialMgmtInventoryCount";
		this.inventoryType = "N";
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String getWarehouse() {
		return warehouse;
	}


	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}


	public String getWarehouseIdentifier() {
		return warehouseIdentifier;
	}


	public void setWarehouseIdentifier(String warehouseIdentifier) {
		this.warehouseIdentifier = warehouseIdentifier;
	}


	public String getOrganization() {
		return organization;
	}


	public void setOrganization(String organization) {
		this.organization = organization;
	}


	public String getOrganizationIdentifier() {
		return organizationIdentifier;
	}


	public void setOrganizationIdentifier(String organizationIdentifier) {
		this.organizationIdentifier = organizationIdentifier;
	}


	public String getMovementDate() {
		return movementDate;
	}


	public void setMovementDate(String movementDate) {
		this.movementDate = movementDate;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getInventoryType() {
		return inventoryType;
	}


	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	
	

}

package com.peoplewalking.warehouse;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.google.gson.reflect.TypeToken;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartnerLocation;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceiptLine;
import com.peoplewalking.warehouse.openbravo.model.Language;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.openbravo.model.OBOrderLine;
import com.peoplewalking.warehouse.openbravo.model.Organization;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.openbravo.model.Warehouse;

public class OBServiceClientTest {
	private static final String USERNAME = "bravoscan";
	private static final String PASSWORD = "bravoscan2015";
	private static OpenbravoService service = OBServiceClient.initializeService(USERNAME, PASSWORD);
	@Test
	public void testGetUser(){		
		User user = service.getUser("CADCDC3549FB4201B5F24E4C03AD2349");
		Assert.assertEquals("User username should be equal", null, user.getUsername());
		Assert.assertEquals("User name should be equal", "Juan L�pez", user.getName());
		Assert.assertEquals("User password should be equal", null,user.getEntityName());
		Assert.assertNotNull("Userlist should not be empty", user);
	}
	
	@Test
	public void testGetUserList(){
		List<User> userList = service.getUserList("");
		Assert.assertNotNull("Userlist should not be empty", userList);
		Assert.assertNotSame("Userlist should not be empty", userList.size(), 0);
	}
	
	@Test
	public void testGetWarehouse(){
		Warehouse warehouse = service.getWarehouse("B2D40D8A5D644DD89E329DC297309055");
		Assert.assertNotNull("Werehouse should not be empty", warehouse);
		Assert.assertEquals("Werehouse id  should be equal ", "B2D40D8A5D644DD89E329DC297309055", warehouse.getObId());
		Assert.assertEquals("Werehouse name  should be equal ", "Espa�a Regi�n Norte", warehouse.getName());
		
	}
	
	@Test
	public void testGetWarehouseList(){
		List<Warehouse> WarehouseList = service.getWarehouseList("");
		Assert.assertNotNull("Warehouselist should not be empty", WarehouseList);
		Assert.assertNotSame("Warehouselist  should not be empty",WarehouseList.size(), 0 ) ;
		
	}
	
	@Test
	public void testPartner(){
		BusinessPartner bPartner = service.getBPartner("FF2EC1D0E28E4F85BE85532ADD556772");
		Assert.assertNotNull("Partner should not be empty",bPartner);
		Assert.assertEquals("Partner id should be equal","FF2EC1D0E28E4F85BE85532ADD556772",bPartner.getObId());
		Assert.assertEquals("Partner name should  be equal","Be Soft Drinker, Inc.",bPartner.getName());
		Assert.assertEquals("Partner should not be empty",bPartner.getEntityName());
		
	}
	
	@Test
	public void testPartnerLocation(){
		BusinessPartnerLocation bPartnerLocation = service.getBPartnerLocation("");
		Assert.assertNotNull("Partner should not be empty", bPartnerLocation);
		Assert.assertEquals("Partner should not be empty", bPartnerLocation.getLocationAddress());
		Assert.assertNotNull("Partner should not be empty", bPartnerLocation.getObId());
	}
	
	@Test
	public void testOrder(){
		OBOrder oBOrder = service.getOrder("FBF47A639DB64E23946CABEAB5C77B08");
		Assert.assertNotNull("Order shoul not be empty", oBOrder);
		Assert.assertEquals("Order shoul be equal", oBOrder.getObId());
		Assert.assertNotNull("Order DocumentNo shoul not be empty", oBOrder.getDocumentNo());
		Assert.assertNotNull("Order EntityName shoul not be empty", oBOrder.getEntityName());
		Assert.assertNotNull("Oreder DocumenType nhoul not be empty", oBOrder.getDocTypeId());
				
	}
	
//	@Test
//	public void testOrderlist(){
//		List<OBOrder> orderlist= service.getOrderList("");
//		Assert.assertNotNull("OrderList should not be empty", orderlist);
//		Assert.assertNotSame("OrderList should not be empty", orderlist.size(), 0);
//	}
	
	@Test
	public void testOrderLine(){
		OBOrderLine oBOrderLine = service.getOrderLine("FFEA02CA91224890BD4346073D70EAC0");
		Assert.assertNotNull("OrderLine shoul not be empty", oBOrderLine);
		Assert.assertEquals("OrderLine  id shoul be equal","FFEA02CA91224890BD4346073D70EAC0", oBOrderLine.getObId());
		Assert.assertNotNull("OrderLine EntityName shoul be no empty", oBOrderLine.getEntityName());
		}
	
	@Test
	public void TestOrderLineList(){
		List<OBOrderLine> orderLineList = service.getOrderLineList("");
		Assert.assertNotNull("OrderLineList should be not empty", orderLineList);
		Assert.assertNotSame("OrderLineList shoul be not empty", orderLineList.size(), 0);
				
	}
	
	@Test
	public void TestGoodsReceipt(){
		GoodsReceipt goodsReceipt = service.getGoodsReceipt(" FFFAFE1781DF42F48AD3A70991E77FC8");
		Assert.assertEquals("GoodsReceipt Id shoul  be equal", goodsReceipt.getObId());
		Assert.assertEquals("GoodsReceipt EntityName shoul be equal ","MaterialMgmtShipmentInOut" ,goodsReceipt.getEntityName());
	}
	
	@Test
	public void TestGoodsReceiptList() {
		
		List<GoodsReceipt> goodsReceiptList = service.getGoodsReceiptList("");
		Assert.assertNotNull("GoodsReceiptList  should be not empty", goodsReceiptList);
		Assert.assertNotSame("GoodsReceiptList  be no empty", goodsReceiptList.size(), 0);
		}
	
	@Test
	public void TestGoodsReceiptLine() {
		GoodsReceiptLine goodsReceiptLine = service.getGoodsReceiptLine( "FFFFEB479E914B76980B28DD931A77B3");
		Assert.assertNotNull("GoodsReceiptLine should be not empty", goodsReceiptLine);
		Assert.assertEquals("GoodsReceiptLine  id should be equal","FFFFEB479E914B76980B28DD931A77B3" ,goodsReceiptLine.getObId());
		Assert.assertEquals("GoodsReceiptLine EntityName be equal ", "MaterialMgmtShipmentInOutLine",goodsReceiptLine.getEntityName());
		Assert.assertEquals("GoodsReceiptLine  id should be equal","Cherry Cola" ,goodsReceiptLine.getProductId());
		Assert.assertNotNull("GoodsReceiptLine Line No should be not empty", goodsReceiptLine.getLineNo());
	}
	
	@Test
	 public void TestGoodsReceiptLineList(){
		List<GoodsReceiptLine> goodsReceiptLineList = service.getGoodsReceiptLineList("");
		Assert.assertNotNull("GoodsReceiptLineList Shoul be not empty", goodsReceiptLineList);
		Assert.assertNotSame("GoodsReceiptLineList Shoul be not empty", goodsReceiptLineList.size(), 0);
		}
	
	@Test
	public void TestLanguage() {
		Language lenguage = service.getLanguage("800002");
		Assert.assertNotNull("Lenguage shoul be not empty", lenguage);
		Assert.assertEquals("Lenguage id shoul be equal","800002", lenguage.getObId());
		Assert.assertEquals("Lenguage name should be equal","gl_ES", lenguage.getName());
		}
	
	@Test
	public void TestLenguageList()  {
		List<Language> lenguageList = service.getLanguageList("");
		Assert.assertNotNull("LenguageList  should be not empty", lenguageList);
		Assert.assertNotSame("LenguageList should be not empty", lenguageList.size(), 0);
	}
	
	@Test
	public void TestOrganization(){
		Organization organization = service.getOrganization("E443A31992CB4635AFCAEABE7183CE85");
		Assert.assertNotNull("Organization should be not empty", organization );
		Assert.assertEquals("Organization Id should be Equal", "E443A31992CB4635AFCAEABE7183CE85" ,organization.getObId());
		Assert.assertEquals("Organization Name should be Equal", "F&B Espa�a - Regi�n Norte" ,organization.getName());
		Assert.assertEquals("Organization  Name should be Equal ", organization.getEntityName());
		}
	@Test
	public void TestOrganizationlist(){
		 List<Organization> organizationList = service.getOrganizationList("");
		 Assert.assertNotNull("OrganizationList  should be not empty", organizationList);
			Assert.assertNotSame("organizationList should be not empty", organizationList.size(), 0);
	}
	
	@Test
	public void TestDocumentType(){
		DocumentType documentType = service.getDocType("FF315752F91C44429D78002C629D58A0");
		Assert.assertNotNull("DocumentType shold be not empty", documentType);
		Assert.assertEquals("DocumentType id should be equal ", "FF315752F91C44429D78002C629D58A0", documentType.getObId());
		Assert.assertEquals("DocumentType Name shold be equal", "MM Receipt", documentType.getName());
		Assert.assertEquals("DocumentType EntityName shold be equal", "DocumentType", documentType.getEntityName());
		Assert.assertEquals("DocumentType DocumentCategory shold be equal","MMR", documentType.getDocumentCategory());
	}
	
	@Test
	public void TestDocumentTypeList(){
		List<DocumentType> documentType = service.getDocTypeList("");
		Assert.assertNotNull("DocumentTypeList  should be not empty", documentType);
		Assert.assertNotSame("DocumentTypeList should be not empty", documentType.size(), 0);
		}
}

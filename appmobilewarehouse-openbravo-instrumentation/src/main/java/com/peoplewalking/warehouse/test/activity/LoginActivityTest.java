package com.peoplewalking.warehouse.test.activity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowActivity;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.LoginActivity;
import com.peoplewalking.warehouse.activity.MainActivity;
import com.peoplewalking.warehouse.test.testHelper.CustomRobolectricTestRunner;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


@RunWith(CustomRobolectricTestRunner.class)
public class LoginActivityTest{
    private static final String USERNAME = "bravoscan";
    private static final String PASSWORD = "bravoscan2015";
    private LoginActivity loginActivity;

    public LoginActivityTest() {
    }
    
    @Before
    public void setup() {
	try {
	    loginActivity = Robolectric.buildActivity(LoginActivity.class).create().get();
	} catch (Exception e) {
	   e.printStackTrace();
	}
    }

    @Test
    public void testLoginActivity() {
	try {
	    TextView mUsernameView = (EditText) loginActivity.findViewById(R.id.username);
		TextView mPasswordView = (EditText) loginActivity.findViewById(R.id.password);
		Button mSignInButton = (Button) loginActivity.findViewById(R.id.sign_in_button);
		mUsernameView.setText(USERNAME);
		mPasswordView.setText(PASSWORD);
		mSignInButton.performClick();
		
		String expectedIntentClassName = new Intent(loginActivity, MainActivity.class).getComponent().getClassName();
		String nextStartedIntent = Shadows.shadowOf(loginActivity).getNextStartedActivity().getComponent().getClassName();
		
		
	       assertThat("Main activity should start after login successfuly",nextStartedIntent, equalTo("")); 
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
    }
}


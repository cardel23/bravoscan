package com.peoplewalking.warehouse.test.testHelper;

import org.junit.runners.model.InitializationError;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.Fs;

public class CustomRobolectricTestRunner extends RobolectricTestRunner{
    public CustomRobolectricTestRunner(Class<?> testClass) throws InitializationError {
	    super(testClass);
	  }

	  @Override
	  protected AndroidManifest getAppManifest(Config config) {
	      try {
		    String myAppPath = CustomRobolectricTestRunner.class.getProtectionDomain()
                          .getCodeSource()
                          .getLocation()
                          .getPath();
                    String manifestPath = myAppPath + "../../../appmobilewarehouse-openbravo-app/src/main/AndroidManifest.xml";
                    String resPath = myAppPath + "../../../appmobilewarehouse-openbravo-app/src/main/res";
                    String assetPath = myAppPath + "../../../appmobilewarehouse-openbravo-app/src/main/assets";
                    AndroidManifest manifest = createAppManifest(Fs.fileFromPath(manifestPath), Fs.fileFromPath(resPath), Fs.fileFromPath(assetPath));
                    return manifest;
	    } catch (Exception e) {
		e.printStackTrace();
		return null;
	    }
	  }
}

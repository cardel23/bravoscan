package com.peoplewalking.warehouse.activity;


import java.util.List;


import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

/**
 * 
 * @author Carlos Delgadillo
 * fecha 14/04/2015
 *
 */
public class NewScanListActivity extends Activity implements OnClickListener {
	
	private Button createBtn;
	private EditText listNameTxt;
	private TextView listName, mViewSelectedBpartner;
	private NovoventApp app;
	private Spinner spinner;
	DeliveryList deliveryList;
	private ImageView imgSelectedBpartner;
	ProgressBar v;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (NovoventApp)getApplicationContext();
//		app.setLocaleConfig();
		setContentView(R.layout.activity_new_list);
		setTitle(R.string.title_activity_new_list);
		initNewListActivity();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}
	
	private void initNewListActivity(){
		createBtn = (Button)findViewById(R.id.button1);
		listNameTxt = (EditText)findViewById(R.id.editText1);
		listName = (TextView)findViewById(R.id.textItem3);
		listName.setText(R.string.create_new_list);
		createBtn.setOnClickListener(this);
		createBtn.setText(R.string.accept);
		listNameTxt.setImeActionLabel(getString(R.string.accept), R.id.newListDone);
		deliveryList = new DeliveryList();
		listNameTxt.setText(Utils.generateScanListName(this, false));
		listNameTxt.selectAll();
		listNameTxt.requestFocus();

		mViewSelectedBpartner = (TextView) findViewById(R.id.textView15);
		imgSelectedBpartner = (ImageView) findViewById(R.id.imageView2);
		
		spinner = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> listTypeAdapter =  ArrayAdapter.createFromResource(this,
					R.array.options_list_type,
					android.R.layout.simple_spinner_item);
		listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	  
		spinner.setAdapter(listTypeAdapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if(position == DeliveryList.INVENTORY){
					deliveryList.setBusinessPartner(null);
					mViewSelectedBpartner.setText(getString(R.string.prompt_select_partner));
					imgSelectedBpartner.setImageResource(R.drawable.icon_required);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		listNameTxt.setOnEditorActionListener(new OnEditorActionListener() {
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_DONE)
				{
				    createList();
				    return true;
				}
				return false;
			}
		});

	}
	
	@Override
	public boolean onNavigateUp() {
	    this.finish();
	    return true;
	}
	
	public void onClick(View v) {
		if(v.getId() == R.id.button1){
		    createList();
		}
	}
	
	private void createList(){
		deliveryList.setListName(listNameTxt.getText().toString());
		try {
			if(deliveryList.getListName() == null || deliveryList.getListName().equals("")){
			    throw new Exception(getString(R.string.no_list_name));
			}
			List<DeliveryList> deliveryLists = DeliveryList.find(DeliveryList.class, "list_name = ?", deliveryList.getListName());
			if(deliveryLists != null && !deliveryLists.isEmpty()){
			    throw new Exception(getString(R.string.list_already_exists,deliveryList.getListName()));
			}
			if(spinner.getSelectedItemPosition() == DeliveryList.INBOUND)
				deliveryList.setInbound(true);
			else
				deliveryList.setInbound(false);
			deliveryList.setListType(spinner.getSelectedItemPosition());
			deliveryList.setUser(app.getUser());
			deliveryList.save();
			
			app.setDeliveryList(deliveryList);
			if(getCallingActivity() == null){
				Intent intent = new Intent(app, ScanListDetailActivity.class);
				finish();
				startActivity(intent);
			}
			else{
				Intent intent = new Intent();
				Bundle bundle = new Bundle();
				bundle.putSerializable(Constants.ID_EXTRA_NEW_LIST, deliveryList);
				intent.putExtras(bundle);
				setResult(Constants.REQUEST_CODE_DELIVERY_LIST, intent);
				finish();
			}
		} catch (Exception e) {
			Toast toast = Toast.makeText(getApplicationContext(),
	        e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
			e.printStackTrace();
		}
	}

	public void onViewClick(View button){
		if(spinner.getSelectedItemPosition() == DeliveryList.INVENTORY){
			Toast.makeText(this,"No se puede seleccionar un tercero para lista de inventario",Toast.LENGTH_SHORT).show();
			return;
		}
		if(spinner.getSelectedItemPosition() == DeliveryList.INBOUND)
			deliveryList.setInbound(true);
		else
			deliveryList.setInbound(false);
		app.setDeliveryList(deliveryList);
		Intent targetIntent;
		targetIntent = new Intent(this ,BpartnerSelectionActivity.class);
		startActivityForResult(targetIntent, Constants.REQUEST_CODE_BPARTNER);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			setSelectedBpartner((BusinessPartner) data.getSerializableExtra(Constants.ID_EXTRA_SELECTED_BPARTNER));
		}
		catch (NullPointerException e){
			Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
//			Log.d("TAG",e.getMessage());
		}

	}

	private void setSelectedBpartner(BusinessPartner businessPartner) {
		if (businessPartner != null) {
			if (!businessPartner.getObId().equalsIgnoreCase(mViewSelectedBpartner.getText().toString())) {
				deliveryList.setBusinessPartner(businessPartner);
				mViewSelectedBpartner.setText(businessPartner.getIdentifier());
				imgSelectedBpartner.setImageResource(R.drawable.icon_ok);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_list, menu);
//		v =  (ProgressBar) MenuItemCompat.getActionView(miActionProgressItem);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


}

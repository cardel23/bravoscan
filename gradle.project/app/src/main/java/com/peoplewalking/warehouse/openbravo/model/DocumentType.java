package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentType extends OBModel implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -776039227487131893L;
	
	public static String IN = "MMR";
	public static String OUT = "MMS";

	@Expose
	@SerializedName("name")
	private String name;
	@Expose
	@SerializedName("documentCategory")
	private String documentCategory;
	@Expose
	@SerializedName("organization$_identifier")
	private String organizationIdentifier;
	@Expose
	@SerializedName("printText")
	private String printText;
	
	public DocumentType() {
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getOrganizationIdentifier() {
		return organizationIdentifier;
	}

	public void setOrganizationIdentifier(String organizationIdentifier) {
		this.organizationIdentifier = organizationIdentifier;
	}

	public String getPrintText() {
		return printText;
	}

	public void setPrintText(String printText) {
		this.printText = printText;
	}

	@Override
	public String toString() {
		String test;
		if(printText == null)
			test = name;
		else
			test = printText;
		return identifier;
	}
	
	

}

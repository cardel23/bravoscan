package com.peoplewalking.warehouse.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import com.orm.SugarRecord;
import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.dialogs.DatePickerFragment;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.InventoryCount;
import com.peoplewalking.warehouse.openbravo.model.InventoryCountLine;
import com.peoplewalking.warehouse.openbravo.model.OBResponseWrapper;
import com.peoplewalking.warehouse.openbravo.model.Organization;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.tasks.DocTypeTask;
import com.peoplewalking.warehouse.tasks.StorageDetailTask;
import com.peoplewalking.warehouse.tasks.TaskWithCallback;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import retrofit.Call;
import retrofit.Response;

public class NewInventoryActivity extends Activity implements OnClickListener, DatePickerDialog.OnDateSetListener {

	public static final String TAG = "NewInventoryActivity";
	private EditText nameTxt, descriptionTxt, dateTxt;
	private Button button;
	private InventoryCount inventoryCount;
	private List<InventoryCountLine> inventoryCountLineList;
	private NovoventApp app;
	private DeliveryList selectedDeliveryList;
	private SaveInventoryTask saveInventoryTask;
	private DatePickerFragment datePickerFragment;
	private SimpleDateFormat dateFormatter;
	private ArrayAdapter<Organization> organizationAdapter;
	private Spinner organizationSpinner;
	private String currentOrganization;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_inventory);
		
		nameTxt = (EditText) findViewById(R.id.editText1);
		descriptionTxt = (EditText) findViewById(R.id.editText2);
		dateTxt = (EditText) findViewById(R.id.editText3);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(this);
		app = (NovoventApp) getApplicationContext();
		selectedDeliveryList = app.getDeliveryList();
		datePickerFragment = new DatePickerFragment();
		datePickerFragment.setOnDateSetListener(this);
		dateTxt.setInputType(InputType.TYPE_NULL);
		dateTxt.setOnClickListener(this);
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		organizationAdapter = new ArrayAdapter<Organization>(this, android.R.layout.simple_list_item_1);
		organizationAdapter.addAll(app.getOrganizations());
//		docAdapter = new ArrayAdapter<DocumentType>(this, android.R.layout.simple_list_item_1);
//		docList = new ArrayList<DocumentType>();
//		docAdapter.addAll(docList);
		organizationSpinner = (Spinner) findViewById(R.id.spinner5);
		organizationSpinner.setAdapter(organizationAdapter);
		organizationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				ArrayAdapter<Organization> tempAdapter = (ArrayAdapter<Organization>) organizationSpinner.getAdapter();
				Organization organization = tempAdapter.getItem(position);
				currentOrganization = (organization.getObId());
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_inventory, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void createInventory(){
		if(dateTxt.getText().toString() == "" || dateTxt.getText().toString() == null)
			Toast.makeText(app, getString(R.string.error_field_required), Toast.LENGTH_SHORT);
		else{
			inventoryCount = new InventoryCount();
			inventoryCount.setName(nameTxt.getText().toString());
			inventoryCount.setMovementDate(dateTxt.getText().toString());
			inventoryCount.setDescription(descriptionTxt.getText().toString());
			inventoryCount.setWarehouse(app.getUser().getDefaultWarehouseId());
			inventoryCount.setOrganization(currentOrganization);
			inventoryCountLineList = new ArrayList<InventoryCountLine>();
			saveInventoryTask = new SaveInventoryTask(NewInventoryActivity.this, getString(R.string.saving));
			saveInventoryTask.execute();
		}
	}
	
	public class SaveInventoryTask extends TaskWithCallback<Void, Integer, List<String>>{
		
		public SaveInventoryTask(Context context, String loadingMessage) {
			super(context, loadingMessage);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		@Override
		protected List<String> doInBackground(Void... params) {	
			return executeSynchronous();
		}
		
		@Override
		public List<String> executeSynchronous(Void... params) {

			ArrayList<String> errorMessages = new ArrayList<String>();
			String errorMessage;
			try{

				Response response = app.getObService().saveInventory(inventoryCount).execute();
				Log.i("RESPONSE",response.raw().body().toString());
				List<InventoryCount> resultList = (List<InventoryCount>) response.body();
				if(resultList == null || resultList.isEmpty()){
					errorMessage = getString(R.string.save_goodreceipt_failed);
					errorMessages.add(errorMessage);
					Log.e(TAG, errorMessage);
				}

				else{
					inventoryCount = resultList.get(0);
				}

				List<DeliveryListItem> items = selectedDeliveryList.getProducts();
				int lineNo = 1;
				int j = 0;

				for(int i=0;i<items.size();i++){
					DeliveryListItem item = items.get(i);
					Product p = item.getProduct();
					String query = "storageBin='"+item.getBin()+"' AND product='"+p.getObId()+"'";
					StorageDetailTask storageDetailTask = new StorageDetailTask(app, query);
					InventoryCountLine line = new InventoryCountLine();
					line.setLineNo(lineNo);
					line.setProduct(p.getObId());
					line.setProductIdentifier(p.getIdentifier());
					line.setStorageBin(item.getBin());
					line.setInventory(inventoryCount.getObId());
					line.setQuantityCount((int) item.getReceivedAmount());
					line.setuOM(p.getUomId());
					line.setuOMIdentifier(p.getUomIdentifier());
					if(item.getOnHandAmount()==null)
						line.setBookQuantity(storageDetailTask.executeSynchronous());
					else
						line.setBookQuantity(item.getOnHandAmount());
					inventoryCountLineList.add(line);
					if(inventoryCountLineList.size()==50 || i==items.size()-1){
						Response<List<InventoryCountLine>> response2 = app.getObService().saveInventoryLines(inventoryCountLineList).execute();
						List<InventoryCountLine> resultLines = response2.body();
						if(resultLines == null || resultLines.isEmpty()){
							errorMessage = getString(R.string.unexpected_error);
							errorMessages.add(errorMessage);
							Log.e(TAG, errorMessage);
							throw new Exception (errorMessage);
							// TODO: show non saved lines
						} else {
							// TODO: delete product from list if good receipt line is saved in OB

						}
						inventoryCountLineList = new ArrayList<InventoryCountLine>();
					}
					lineNo ++;
				}
			} catch (Exception e) {
				errorMessage = getString(R.string.unexpected_error);
				errorMessages.add(errorMessage);
				Log.e(TAG, e.getMessage(), e);
			}
			return errorMessages;
		}
		
		@Override
		protected void onPostExecute(List<String> errorList) {			
			super.onPostExecute(errorList);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewInventoryActivity.this);

			if(!errorList.isEmpty()){
				String[] errors = errorList.toArray(new String[errorList.size()]);
				alertDialogBuilder.setTitle(getString(R.string.operation_failed))
						  .setItems(errors, null)
						  .setCancelable(true)
						  .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
			} else{
				try {
//					selectedDeliveryList.delete();
					app.getDeliveryList().delete();
				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
					app.getDeliveryList().delete();
					Toast.makeText(NewInventoryActivity.this, "Error deleting scan list", Toast.LENGTH_LONG).show();
				}
			    	
				
			    app.setDeliveryList(null);
        		alertDialogBuilder.setCancelable(false)
        					.setTitle(getString(R.string.operation_success))
        					.setMessage(getString(R.string.save_inventory_success, inventoryCount.getName()))
        					.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
        						public void onClick(DialogInterface dialog, int which) {
        							navigateUpTo(app.getHomeIntent());
        						}
        					});	
			}
//			loadingViewHelper.showProgress(false);
			alertDialogBuilder.create().show();
		}
		
	}

	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.button1){
			createInventory();
//			saveInventoryTask.execute();
		}
		if(v.getId() == R.id.editText3){
			datePickerFragment.show(getFragmentManager(), "DatePickerFragment");
		}
	}

	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar newDate = Calendar.getInstance();
        newDate.set(year, monthOfYear, dayOfMonth);
        dateTxt.setText(dateFormatter.format(newDate.getTime()));
	}
	
}

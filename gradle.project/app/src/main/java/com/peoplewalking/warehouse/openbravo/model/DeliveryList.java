package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;
import java.util.List;

import com.orm.SugarRecord;

/**
 * Contiene las listas de productos recibidos (SQLite)
 * @author Carlos Delgadillo
 * fecha 24/04/2015
 *
 */
public class DeliveryList extends SugarRecord<DeliveryList> implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 8670698441093899202L;
	private String listId;
	private String listName;
	private BusinessPartner businessPartner;
	private boolean inbound;
	private List<DeliveryListItem> products;
	private User user;
	private Integer listType;
	public static final int INBOUND = 0,
							OUTBOUND = 1,
							INVENTORY = 2;
	
	
	public String getListId() {
		return listId;
	}



	public void setListId(long listId) {
		this.listId = String.valueOf(listId);
	}



	public String getListName() {
		return listName;
	}



	public void setListName(String listName) {
		this.listName = listName;
	}



	public List<DeliveryListItem> getProducts() {
		return products;
	}



	public void setProducts(List<DeliveryListItem> products) {
		this.products = products;
	}

	public DeliveryListItem getListItemByUpcean(String upcean, String storageBin){
	    if(this.products != null){
		for(DeliveryListItem item : this.products){
		    if(item.getProduct() != null && upcean.equals(item.getProduct().getuPCEAN())
		    		&& storageBin.equals(item.getProduct().getStorageBin())){
			return item;
		    }
		}
	    }
	    return null;
	}

	public boolean isInbound() {
		return inbound;
	}

	public void setInbound(boolean inbound) {
		this.inbound = inbound;
	}

	public BusinessPartner getBusinessPartner() {
		return businessPartner;
	}

	public void setBusinessPartner(BusinessPartner businessPartner) {
		this.businessPartner = businessPartner;
	}
	
	public User getUser() {
	    return user;
	}

	public void setUser(User user) {
	    this.user = user;
	}

	public Integer getListType() {
		return listType;
	}



	public void setListType(Integer listType) {
		this.listType = listType;
	}



	public DeliveryList() {
	}


	@Override
	public String toString() {
		return "Nº " + getId() + " - " + listName;
	}

}

package com.peoplewalking.warehouse.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import retrofit.RetrofitError;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.adapters.GoodsReceiptLineAdapter;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceiptLine;
import com.peoplewalking.warehouse.openbravo.model.OBOrderLine;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.tasks.LoadProductTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.NumberPickerBuilder;
import com.peoplewalking.warehouse.util.Utils;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Toast;

import retrofit.Response;

public class GenReceiptDetailActivity extends Activity {
	public static final String TAG = "ReceiptDetailActivity";
	private List<OBOrderLine> orderLines;
	private DeliveryList selectedDeliveryList;
	private GoodsReceipt currentGoodReceipt;
	private List<GoodsReceiptLine> goodReceiptLines;
	private NovoventApp app;
	private ListView mListViewReceiptLines;
	private GoodsReceiptLineAdapter goodsReceiptLineAdapter;	
	private LinearLayout mViewMatchingErrorsIndicator;
	private LinearLayout mViewGreaterQuantityIndicator;
	private LinearLayout mViewMainContainer;
	private LoadingViewHelper loadingViewHelper;
	private Button mBtnGenerate;
	private Boolean matchedToOrder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		app = (NovoventApp) getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_receipt_detail);
		this.setTitle(R.string.title_activity_register_receipt);
		mViewMainContainer = (LinearLayout) findViewById(R.id.main_container);
		mListViewReceiptLines = (ListView) findViewById(R.id.lv_receipt_lines);
		mViewMatchingErrorsIndicator = (LinearLayout) findViewById(R.id.matching_warning_indicator);
		mViewGreaterQuantityIndicator = (LinearLayout) findViewById(R.id.greater_quantity_error_indicator);
		mBtnGenerate = (Button) findViewById(R.id.btnGenerate);
		
		currentGoodReceipt = (GoodsReceipt) getIntent().getSerializableExtra(Constants.ID_EXTRA_CURRENT_GOODRECEIPT);
		if (currentGoodReceipt == null){
			currentGoodReceipt = app.getGoodReceipt();
		}
		matchedToOrder = currentGoodReceipt.getSalesOrderId() != null;
		selectedDeliveryList = app.getDeliveryList();
		
		mListViewReceiptLines.setLongClickable(true);
		mListViewReceiptLines.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL); 
		mListViewReceiptLines.setMultiChoiceModeListener(lineMultichoiseListener);
		mListViewReceiptLines.setOnItemLongClickListener(new OnItemLongClickListener() {
	        	public boolean onItemLongClick (AdapterView<?> parent, View view, int position, long id) {
	        	     view.setSelected(true);
	        	     return true;
	        	   }
	        });
//		registerForContextMenu(mListViewReceiptLines);
		try {
		    	loadingViewHelper = new LoadingViewHelper(this, mViewMainContainer);
		    	loadingViewHelper.showProgress(true);
			PopulateGoodReceiptLinesTask populateGoodReceiptLinesTask = new PopulateGoodReceiptLinesTask();
			populateGoodReceiptLinesTask.execute();
		} catch (Exception e) {
			Log.e(TAG, e.toString());
			Toast.makeText(GenReceiptDetailActivity.this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
		}		
		
	}
	
	public void onButtonClick(View button){
		switch (button.getId()) {
		case R.id.btnBack:
			finish();
			break;
		case R.id.btnGenerate:
		    	loadingViewHelper.showProgress(true);
			SaveGoodReceiptTask  saveGReceiptTask = new SaveGoodReceiptTask();
			saveGReceiptTask.execute();
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	
	private void onItemEdit(final int position){
		LayoutInflater layoutInflater = LayoutInflater.from(GenReceiptDetailActivity.this);
		View dialogEdit = layoutInflater.inflate(R.layout.dialog_edit_received_quantity, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GenReceiptDetailActivity.this);
		alertDialogBuilder.setView(dialogEdit);
		NumberPickerBuilder pickerBuilder = new NumberPickerBuilder(dialogEdit.findViewById(R.id.numberPicker2));
		pickerBuilder.setmDefault(goodReceiptLines.get(position).getMovementQuantity().intValue());
		final NumberPicker editAmount = pickerBuilder.create();
		final EditText viewNewQuantity = (EditText) dialogEdit.findViewById(R.id.et_new_quantity);
		viewNewQuantity.setText(String.valueOf(goodReceiptLines.get(position).getMovementQuantity()));
		// setup a dialog window
		alertDialogBuilder
				.setMessage(R.string.edit_goodreceiptline)
				.setCancelable(true)
				.setPositiveButton(getString(R.string.edit), new DialogInterface.OnClickListener() {
					
							public void onClick(DialogInterface dialog, int id) {
//								Float newQuantity = Float.parseFloat(viewNewQuantity.getText().toString());
//								Integer newQuantity = Integer.getInteger(string)
								GoodsReceiptLine line = goodReceiptLines.get(position);
								line.setMovementQuantity((float)editAmount.getValue());
								line.save();
								goodsReceiptLineAdapter.notifyDataSetChanged();
								validateQuantitiesMatchingErrors();
							}
						})
				.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								dialog.cancel();
							}
						});
		AlertDialog alertD = alertDialogBuilder.create();
		alertD.show();
	}


	public class PopulateGoodReceiptLinesTask extends AsyncTask<Void, Void, List<GoodsReceiptLine>>{
		private ArrayList<String> errorMessages = new ArrayList<String>();
		String errorMessage;
		@Override
		protected List<GoodsReceiptLine> doInBackground(Void... params) {
			try {
				// Some validations
			    	if(currentGoodReceipt.getSalesOrderId() != null){
			    	    orderLines = app.getObService().getOrderLineList("salesOrder='" + currentGoodReceipt.getSalesOrderId() + "'").execute().body();
			    	    if(orderLines == null || orderLines.isEmpty()){
					errorMessage = getString(R.string.no_orderlines_found, currentGoodReceipt.getSalesOrderIdentifier());
					throw new Exception(errorMessage);
			    	    }
			    	}
			    	
				//populate receipt lines
				goodReceiptLines = new ArrayList<GoodsReceiptLine>();
				int lineNo = 10;
				for(DeliveryListItem item: selectedDeliveryList.getProducts()){
					
					LoadProductTask loadProductTask = new LoadProductTask(app);
					Product obProduct = loadProductTask.executeSynchronous(item.getProduct().getuPCEAN());
					if(obProduct == null){
					   errorMessages.add(getString(R.string.product_not_found, item.getProduct().getuPCEAN()));
					   continue; //Iterate to next product if current is not found in OB
					}
					item.setProduct(obProduct); // update product with new info	
									
					GoodsReceiptLine line = new GoodsReceiptLine();	
					line.setProductIdentifier(item.getProduct().toString());
					line.setProductId(item.getProduct().getObId());
					line.setProduct(item.getProduct());
					line.setBusinessPartnerId(currentGoodReceipt.getPartnerId());
					line.setLineNo(lineNo);
					line.setMovementDate(Utils.dateToString(new Date()));
					line.setMovementQuantity(item.getReceivedAmount());
					line.setuOMId(item.getProduct().getUomId());
					line.setuOMIdentifier(item.getProduct().getUomIdentifier());
					line.setStorageBinId(item.getBin());
					line.setOrganizationId(currentGoodReceipt.getOrganizationId());
//					line.setStorageBinIdentifier(item.getProduct().getStorageBinIdentifier());
					
					if(currentGoodReceipt.getSalesOrderId() != null){
					    OBOrderLine matchingOrderLine = Utils.findOrderLineByProduct(orderLines, item);	
					    if(matchingOrderLine != null){
        					line.setSalesOrderLine(matchingOrderLine.getObId());
        					line.setOrderedQuantity(matchingOrderLine.getOrderedQuantity());
					    } 
					}
					goodReceiptLines.add(line);
					lineNo += 10;
				}
				

//			} catch (RetrofitError e) {
//				errorMessage = getString(R.string.retrofit_error_getting_data);
//				errorMessages.add(errorMessage);
//				Log.e(TAG, e.toString(), e);
			} catch (Exception e) {
				errorMessages.add(e.getMessage());
				Log.e(TAG, e.toString(), e);
			}
			return goodReceiptLines;
		}
		
		@Override
		protected void onPostExecute(List<GoodsReceiptLine> result) {			
			super.onPostExecute(result);
			if(!errorMessages.isEmpty()){
			    for(String errorMsg: errorMessages){
				Toast.makeText(GenReceiptDetailActivity.this, errorMsg, Toast.LENGTH_LONG).show();
			    }
			}
			goodsReceiptLineAdapter = new GoodsReceiptLineAdapter(GenReceiptDetailActivity.this, result, matchedToOrder);
			mListViewReceiptLines.setAdapter(goodsReceiptLineAdapter);
			if(matchedToOrder){
			    validateQuantitiesMatchingErrors();
			}
			loadingViewHelper.showProgress(false);
		}
		
	}

	public class SaveGoodReceiptTask extends AsyncTask<Void, Integer, List<String>>{		
		@Override
		protected List<String> doInBackground(Void... params) {	
			ArrayList<String> errorMessages = new ArrayList<String>();
			String errorMessage;
			try {
                Response response = app.getObService()
                        .saveGoodReceipt(currentGoodReceipt).execute();
				 List<GoodsReceipt> resultList = (List<GoodsReceipt>) response.body();
				 if(resultList == null || resultList.isEmpty()){
					 errorMessage = getString(R.string.save_goodreceipt_failed);
					 errorMessages.add(errorMessage);
					 Log.e(TAG, errorMessage);
				 } else{
					 currentGoodReceipt = resultList.get(0);			 
					 for(GoodsReceiptLine line : goodReceiptLines){
						 line.setGoodReceiptId(currentGoodReceipt.getObId());
					 }
//					 List<GoodsReceiptLine> resultLines = app.getObService().saveGoodReceiptLine(goodReceiptLines.get(0));
					 List<GoodsReceiptLine> resultLines = app.getObService()
							 .saveGoodsReceiptLines(goodReceiptLines).execute().body();
					 if(resultLines == null || resultLines.isEmpty()){
						 errorMessage = getString(R.string.unexpected_error);
						 errorMessages.add(errorMessage);
						 Log.e(TAG, errorMessage);
						 // TODO: show non saved lines
					 } else {
					     // TODO: delete product from list if good receipt line is saved in OB
					 }
					 
					 
				 }
//			} catch (RetrofitError e) {
//				errorMessage = getString(R.string.retrofit_error_getting_data);
//				errorMessages.add(errorMessage);
//				Log.e(TAG, e.getMessage(), e);
			} catch (Exception e) {
				errorMessage = getString(R.string.unexpected_error);
				errorMessages.add(errorMessage);
				Log.e(TAG, e.getMessage(), e);
			}
			return errorMessages;
		}
		
		@Override
		protected void onPostExecute(List<String> errorList) {			
			super.onPostExecute(errorList);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(GenReceiptDetailActivity.this);

			if(!errorList.isEmpty()){
				String[] errors = errorList.toArray(new String[errorList.size()]);
				alertDialogBuilder.setTitle(getString(R.string.operation_failed))
						  .setItems(errors, null)
						  .setCancelable(true)
						  .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
			} else{
				try {
					app.getDeliveryList().delete();
				} catch (Exception e) {
					Log.e(TAG, e.getMessage(), e);
					Toast.makeText(GenReceiptDetailActivity.this, "Error deleting scan list", Toast.LENGTH_LONG).show();
				}
			    	
				
			    app.setDeliveryList(null);
        		alertDialogBuilder.setCancelable(false)
        					.setTitle(getString(R.string.operation_success));
        					if(currentGoodReceipt.getDocumentNo()!=null && !currentGoodReceipt.getDocumentNo().equals(""))
        						alertDialogBuilder.setMessage(getString(R.string.save_goodreceipt_success, currentGoodReceipt.getDocumentNo()));
        					else
        						alertDialogBuilder.setMessage(getString(R.string.operation_success));
        					alertDialogBuilder.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
        						public void onClick(DialogInterface dialog, int which) {
        							navigateUpTo(app.getHomeIntent());
        						}
        					});
									
			}
			loadingViewHelper.showProgress(false);
			alertDialogBuilder.create().show();
		}
		
	}

	private void validateQuantitiesMatchingErrors(){
	    if(currentGoodReceipt.getSalesOrderId() != null){
	    	boolean diferrentQuantities = false;
	    	boolean greaterQuantities = false;
	    	for(GoodsReceiptLine line : goodReceiptLines){
        	    if(line.getOrderedQuantity() == null || line.getOrderedQuantity() > line.getMovementQuantity()){
        	        diferrentQuantities = true;
        	    }else if( line.getOrderedQuantity() < line.getMovementQuantity()){
        		greaterQuantities = true;
        	    }
	    	}
    		if(diferrentQuantities){
    		    mViewMatchingErrorsIndicator.setVisibility(View.VISIBLE);
    		}else{
    		    mViewMatchingErrorsIndicator.setVisibility(View.GONE);
    		}
    		if(greaterQuantities){
    		    mViewGreaterQuantityIndicator.setVisibility(View.VISIBLE);
    		    mBtnGenerate.setClickable(false);
    		    mBtnGenerate.setAlpha(0.5F);
    		}else{
    		    mViewGreaterQuantityIndicator.setVisibility(View.GONE);
    		    mBtnGenerate.setClickable(true);
    		    mBtnGenerate.setAlpha(1F);
    		}
	    }
	}
	private int nr = 0;
	private int selectedProductIndex;
	private MenuItem mMenuItemEdit;
	private MultiChoiceModeListener lineMultichoiseListener  = new MultiChoiceModeListener() {
	    
	    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	    }
	    
	    public void onDestroyActionMode(ActionMode mode) {
		 goodsReceiptLineAdapter.removeSelection();
	    }
	    
	    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		nr = 0;
	        MenuInflater inflater = getMenuInflater();
	        inflater.inflate(R.menu.action_list_menu, menu);
	        menu.getItem(1).setTitle(getString(R.string.delete));
	        mMenuItemEdit = (MenuItem) menu.findItem(R.id.action_edit);
	        mMenuItemEdit.setVisible(false);
	        return true;
	    }
	    
	    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		 switch (item.getItemId()) {
             	   case R.id.action_delete:
             	       try {
             		 SparseBooleanArray selected = goodsReceiptLineAdapter.getSelectedIds();
           		   for(int i = selected.size()-1;i>=0;i--){
           			   if(selected.valueAt(i)){
           				   goodsReceiptLineAdapter.remove(selected.keyAt(i));
           				   goodsReceiptLineAdapter.notifyDataSetChanged();
           				   validateQuantitiesMatchingErrors();
           			   }
           		   }
           		   mode.finish();
           		   return true;
		    } catch (Exception e) {
			Log.e(TAG, getString(R.string.unexpected_error), e);
			Toast.makeText(GenReceiptDetailActivity.this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
		    }
             	   case R.id.action_edit:
             	       mode.finish();
             	       onItemEdit(selectedProductIndex);               	       
             	       return true;
                 default:
              	   return false;
		 }
	    }
	    
	    public void onItemCheckedStateChanged(ActionMode mode, int position,
		    long id, boolean checked) {
                if (checked) {
                    nr++;
                    goodsReceiptLineAdapter.toggleSelection(position);                   
                } else {
                    nr--;
                    goodsReceiptLineAdapter.toggleSelection(position);                
                }
                mode.setTitle(getString(R.string.selected, nr));
                selectedProductIndex = position;
                if(nr == 1){
                    mMenuItemEdit.setVisible(false);
                } else{
                    mMenuItemEdit.setVisible(false);
                }
	    }
	};
}

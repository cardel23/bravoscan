package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceList extends OBModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6677295822078969053L;
	
	@Expose
	@SerializedName(value = "name")
	private String name;
	@Expose
	@SerializedName(value = "currency")
	private String currency;
	@Expose
	@SerializedName(value = "currency$_identifier")
	private String currency$_identifier;

	public PriceList() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrency$_identifier() {
		return currency$_identifier;
	}

	public void setCurrency$_identifier(String currency$_identifier) {
		this.currency$_identifier = currency$_identifier;
	}

	@Override
	public String toString() {
		return name;
	}
	

}

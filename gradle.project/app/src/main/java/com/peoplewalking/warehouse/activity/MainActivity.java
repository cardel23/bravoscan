/**
 * 
 */
package com.peoplewalking.warehouse.activity;

import java.util.Locale;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.controllers.MainActivityController;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

/**
 * @author Darlon Espinoza
 *	This is the main menu activity
 */
public class MainActivity extends Activity {
	Locale locale;
	Configuration config;
	NovoventApp app;

	private MainActivityController MAIN_ACTIVITY_CONTROLLER;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		app = (NovoventApp)getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		if(app.getHomeIntent() == null)
			app.setIntent(getIntent());
		setContentView(R.layout.activity_main);
		setTitle(R.string.title_activity_main);
		MAIN_ACTIVITY_CONTROLLER = new MainActivityController(app);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(MAIN_ACTIVITY_CONTROLLER, "MAIN_ACTIVITY_CONTROLLER");
		fragmentTransaction.commit();
	}
	
	
	public void onClickOptionMenu(View button){

		MAIN_ACTIVITY_CONTROLLER.selectActivity(button);
	}

}

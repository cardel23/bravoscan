package com.peoplewalking.warehouse.exceptions;

public class InvalidArgumentException extends Exception {

    /**
     * SerialVersionUID
     */
    private static final long serialVersionUID = 1L;
    
    public InvalidArgumentException(String message) {
	super(message);
    }

}

package com.peoplewalking.warehouse.openbravo.api;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.peoplewalking.warehouse.openbravo.model.OBModel;
import com.peoplewalking.warehouse.util.Constants;

public class OBListDeserializer<T extends OBModel> implements JsonDeserializer<List<T>> {
	public static final String TAG = "OBListDeserializer";
	public Class<T> classType;
	public List<T> deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
			Gson gson = new GsonBuilder().
					excludeFieldsWithoutExposeAnnotation()
//					.setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK")
					.create();
		try {
			JsonObject jsonResponse = new JsonParser().parse(json.toString()).getAsJsonObject();
			if (jsonResponse.has("response")) {
				JsonObject responseWrapper = jsonResponse.getAsJsonObject("response");
				if(responseWrapper.has("data")){
					JsonArray data = responseWrapper.getAsJsonArray("data").getAsJsonArray();
					List<T> elementsArray;
					try {
						elementsArray = gson.fromJson(data.toString(), type);
					} catch (Exception e) {
						elementsArray = new ArrayList<T>();
						try {
							for (JsonElement jsonElement : data) {
								String entityName = jsonElement.getAsJsonObject().get( "_entityName" ).getAsString();
								String className = Constants.getClassNames(entityName);
								try {
									T element = context.deserialize( jsonElement, Class.forName( className ) );
									elementsArray.add(element);
								} catch (JsonSyntaxException e2) {
									e2.printStackTrace();
								}
								
							}
						} catch (Exception e1) {
							e1.printStackTrace();
							throw new JsonParseException(e.getMessage());
						}
					}
					return elementsArray;		
				}
			} 
		} catch (JsonParseException e) {
			Log.e(TAG, e.getMessage(), e);
			throw e;
		}				
		return null;
	}

}

package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User extends OBModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6173517186857498311L;

	@Expose
	@SerializedName("name")
	private String name;
	
	@Expose
	@SerializedName("username")
	private String username;
	
	@Expose
	@SerializedName("password")
	private String password;
	
	@Expose
	@SerializedName("defaultOrganization")
	private String defaultOrganizationId;
	
	@Expose
	@SerializedName("defaultOrganization$_identifier")
	private String defaultOrganizationIdentifier;
	
	@Expose
	@SerializedName("organization")
	private String organizationId;
	
	@Expose
	@SerializedName("organization$_identifier")
	private String organizationIdentifier;
	
	@Expose
	@SerializedName("defaultWarehouse")
	private String defaultWarehouseId;
	
	@Expose
	@SerializedName("defaultWarehouse$_identifier")
	private String defaultWarehouseIdentifier;
	
	@Expose
	@SerializedName("defaultLanguage")
	private String defaultLanguageId;
	
	@Expose
	@SerializedName("defaultLanguage$_identifier")
	private String defaultLanguageIdentifier;
	
	public String getObId() {
		return obId;
	}
	public void setObId(String obId) {
		this.obId = obId;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public String getDefaultOrganizationId() {
		if(defaultOrganizationId == null)
			return organizationId;
		return defaultOrganizationId;
	}
	public void setDefaultOrganizationId(String defaultOrganizationId) {
		this.defaultOrganizationId = defaultOrganizationId;
	}
	public String getDefaultOrganizationIdentifier() {
		if(defaultOrganizationIdentifier == null)
			return organizationIdentifier;
		return defaultOrganizationIdentifier;
	}
	public void setDefaultOrganizationIdentifier(
			String defaultOrganizationIdentifier) {
		this.defaultOrganizationIdentifier = defaultOrganizationIdentifier;
	}
	public String getDefaultWarehouseId() {
		return defaultWarehouseId;
	}
	public void setDefaultWarehouseId(String defaultWarehouseId) {
		this.defaultWarehouseId = defaultWarehouseId;
	}
	public String getDefaultWarehouseIdentifier() {
		return defaultWarehouseIdentifier;
	}
	public void setDefaultWarehouseIdentifier(String defaultWarehouseIdentifier) {
		this.defaultWarehouseIdentifier = defaultWarehouseIdentifier;
	}
	public String getDefaultLanguageId() {
		return defaultLanguageId;
	}
	public void setDefaultLanguageId(String defaultLanguageId) {
		this.defaultLanguageId = defaultLanguageId;
	}
	public String getDefaultLanguageIdentifier() {
		return defaultLanguageIdentifier;
	}
	public void setDefaultLanguageIdentifier(String defaultLanguageIdentifier) {
		this.defaultLanguageIdentifier = defaultLanguageIdentifier;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (obId == null) {
			if (other.obId != null)
				return false;
		} else if (!obId.equals(other.obId))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}
	
	
}

package com.peoplewalking.warehouse.tasks;

import java.util.List;

import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.ProductCategory;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Context;

/**
 * Tarea para obtener categorías de producto
 * @author Carlos Delgadillo
 * fecha: 04/08/2015
 */
public class LoadCategoriesTask extends	TaskWithCallback<Void, Void, List<ProductCategory>> {

	public LoadCategoriesTask(Context context) {
		super(context);
		app = (NovoventApp) context.getApplicationContext();
	}
	
	public LoadCategoriesTask(Context context, String loadingMessage) {
		super(context, loadingMessage);
		app = (NovoventApp) context.getApplicationContext();
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected List<ProductCategory> doInBackground(Void... params) {
		// TODO Auto-generated method stub
		return executeSynchronous(params);
	}
	
	@Override
	public List<ProductCategory> executeSynchronous(Void... params) {
		try {
			queryParams.put("_selectedProperties", "id,name");
			queryParams.put("_sortBy","name");
			List<ProductCategory> categories = OBServiceClient.getOBService().getProductCategories(queryParams).execute().body();
			return categories;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

}

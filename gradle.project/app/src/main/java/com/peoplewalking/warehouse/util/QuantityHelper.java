package com.peoplewalking.warehouse.util;

/**
 * Created by carlos on 03-21-16.
 */
public class QuantityHelper {

    private int key;
    private String value;

    public QuantityHelper(int key, String value){
        this.setKey(key);
        this.setValue(value);
    }


    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}

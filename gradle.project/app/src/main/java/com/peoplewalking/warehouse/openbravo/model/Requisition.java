package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.peoplewalking.warehouse.util.Utils;

public class Requisition extends OBModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8765119239627739923L;
	
	@Expose
	private String name;
	@Expose
	@SerializedName(value = "userContact")
	private String requester;
	@Expose
	@SerializedName(value = "organization")
	private String organization;
	@Expose
	@SerializedName(value = "businessPartner")
	private String businessPartner;
	@Expose
	@SerializedName(value = "currency")
	private String currency;
	@Expose
	@SerializedName(value = "description")
	private String description;
	@Expose
	@SerializedName(value = "documentNo")
	private String documentNo;
	

	private User user;
	private List<RequisitionLine> requisitionLines;
	private BusinessPartner partnerRef;

	public Requisition() {
		// TODO Auto-generated constructor stub
	}
	
	public Requisition(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getRequester() {
		return requester;
	}


	public void setRequester(String requester) {
		this.requester = requester;
	}


	public String getOrganization() {
		return organization;
	}


	public void setOrganization(String organization) {
		this.organization = organization;
	}


	public String getBusinessPartner() {
		return businessPartner;
	}


	public void setBusinessPartner(String businessPartner) {
		this.businessPartner = businessPartner;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}


	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


	public List<RequisitionLine> getRequisitionLines() {
		return requisitionLines;
	}


	public void setRequisitionLines(List<RequisitionLine> requisitionLines) {
		this.requisitionLines = requisitionLines;
	}


	public BusinessPartner getPartnerRef() {
		return partnerRef;
	}


	public void setPartnerRef(BusinessPartner partnerRef) {
		this.partnerRef = partnerRef;
	}


	public String getDocumentNo() {
		return documentNo;
	}


	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}


	@Override
	public String toString() {
		StringBuilder builtName = new StringBuilder(name);
		if(description != null)
			builtName.append(" - " + description);
		return builtName.toString();
	}

}

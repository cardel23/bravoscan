package com.peoplewalking.warehouse.tasks;

import java.util.List;

import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.model.Product;

import android.content.Context;

import retrofit.Response;

public class ProductListTask extends TaskWithCallback<Void, Void, List<Product>> {

	private String storageBin, productCategory,productIdentifier;
	private int quantity;

	public ProductListTask(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ProductListTask(Context context, String storageBin, String productCategory, String productIdentifier){
		super(context);
		this.storageBin=storageBin;
		this.productCategory=productCategory;
		this.productIdentifier=productIdentifier;
//		this.quantityOnHand=quantityOnHand;
	}

	@Override
	protected List<Product> doInBackground(Void... params) {
		// TODO Auto-generated method stub
		List<Product> products;
        StringBuilder builder = new StringBuilder();
		builder.append("as e where e in(");
        builder.append(query.toString());
        builder.append(")");
		try {
			Response<List<Product>> response = app.getObService().getProductList(query.toString()).execute();
			products = response.body();
			return products;
		}
		catch (Exception e){
			return null;
		}

	}

    @Override
    public void setOnPostExecuteListener(OnPostExecuteListener<List<Product>> onPostExecuteListener) {
        super.setOnPostExecuteListener(onPostExecuteListener);
    }
}

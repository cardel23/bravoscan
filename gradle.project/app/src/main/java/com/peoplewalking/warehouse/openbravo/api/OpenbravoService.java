package com.peoplewalking.warehouse.openbravo.api;

import java.util.List;
import java.util.Map;

import com.peoplewalking.warehouse.openbravo.model.*;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;

public interface OpenbravoService {
	public static final int UNAUTHORIZED = 401;
	public static final String Context = "/openbravo/org.openbravo.service.json.jsonrest";
	
	/*
	 * Product Services
	 */
	
	@GET(Context+"/Product/{id}")
	public Call<Product> getProduct(@Path("id") String productId);
	
	@GET(Context+"/Product")
	public Call<Product> getProductByUPC(@Query("_where") String query);
	
	@GET(Context+"/Product")
	public Call<List<Product>> getProductList(@Query("_where") String query);
	
	@GET(Context+"/Product")
	public Call<List<Product>> getProductList(@QueryMap Map<String, String> params);
	
	/*
	 * Product Image Services
	 */
	@GET(Context+"/ADImage/{id}")
	public Call<ProductImage> getProductImage(@Path("id") String productId);
	
	
	/*
	 * StoreBin Services
	 */
	
	@GET(Context+"/Locator/{id}")
	public Call<StorageBin> getStorageBin(@Path("id") String storeBinId);
	
	@GET(Context+"/Locator")
	public Call<List<StorageBin>> getStorageBinList(@Query("_where") String query);
	
	@GET(Context+"/Locator")
	public Call<List<StorageBin>> getStorageBinList(@QueryMap Map<String, String> query);
	
	/*
	 * Warehouse Services
	 */
	
	@GET(Context+"/Warehouse/{id}")
	public Call<Warehouse> getWarehouse(@Path("id") String warehouseId);
	
	@GET(Context+"/Warehouse")
	public Call<List<Warehouse>> getWarehouseList(@Query("_where") String query);
	
	@GET(Context+"/Warehouse")
	public void getWarehouseList(@Query("_where") String query, Callback<List<Warehouse>> cb);
	
	/*
	 * Partner Services
	 */
	
	@GET(Context+"/BusinessPartner/{id}")
	public Call<BusinessPartner> getBPartner(@Path("id") String partnerId);
	
	@GET(Context+"/BusinessPartner")
	public Call<List<BusinessPartner>> getBpartnerList(@QueryMap Map<String, String> params);
	
	@GET(Context+"/BusinessPartner")
	public Call<List<BusinessPartner>> getBpartnerList(@Query("_where") String query);
	
	@GET(Context+"/BusinessPartner")
	public void findAllBusinessPartners(Callback<List<BusinessPartner>> cb);
	
	/*
	 * PartnerLocation Services
	 */
	
	@GET(Context+"/BusinessPartnerLocation/{id}")
	public Call<BusinessPartnerLocation> getBPartnerLocation(@Path("id") String partnerLocationId);
	
	@GET(Context+"/BusinessPartnerLocation")
	public Call<List<BusinessPartnerLocation>> getBPartnerLocationList(@Query("_where") String query);
	
	
	/*
	 * Order Services
	 */	
	
	@GET(Context+"/Order/{id}")
	public Call<OBOrder> getOrder(@Path("id") String orderId);
	
	@GET(Context+"/Order")
	public Call<List<OBOrder>> getOrderList(@QueryMap Map<String, String> parameters);
		
	/*
	 * OrderLine Services
	 */
	
	@GET(Context+"/OrderLine/{id}")
	public Call<OBOrderLine> getOrderLine(@Path("id") String orderLineId);
		
	@GET(Context+"/OrderLine")
	public Call<List<OBOrderLine>> getOrderLineList(@Query("_where") String query);
	
	/*
	 * GoodsReceipt Services
	 */
	
	@GET(Context+"/MaterialMgmtShipmentInOut/{id}")
	public Call<GoodsReceipt> getGoodsReceipt(@Path("id") String goodsReceiptId);
	
	@GET(Context+"/MaterialMgmtShipmentInOut")
	public Call<List<GoodsReceipt>> getGoodsReceiptList(@Query("_where") String query);
	
	@POST(Context+"/MaterialMgmtShipmentInOut")
	public Call<List<GoodsReceipt>> saveGoodReceipt(@Body GoodsReceipt goodsReceipt);
	
	
	/*
	 * GoodsReceiptLine Services
	 */
	
	@GET(Context+"/MaterialMgmtShipmentInOutLine/{id}")
	public Call<GoodsReceiptLine> getGoodsReceiptLine(@Path("id") String goodsReceiptLineId);
	
	@GET(Context+"/MaterialMgmtShipmentInOutLine")
	public Call<List<GoodsReceiptLine>> getGoodsReceiptLineList(@Query("_where") String query);
	
	@POST(Context+"/MaterialMgmtShipmentInOutLine")
	public Call<List<GoodsReceiptLine>> saveGoodReceiptLine(@Body GoodsReceiptLine goodsReceiptLine);
	
	/**
	 * Guarda una lista de GoodsReceipts como un arreglo JSON
	 * No se serializan los elementos
	 * @param goodsReceiptsList
	 * @return 
	 */
	@POST(Context+"/MaterialMgmtShipmentInOutLine")
	public Call<List<GoodsReceiptLine>> saveGoodsReceiptLines(@Body List<GoodsReceiptLine> goodsReceiptLinesList);
	
	/*
	 * ADUser Services	
	 */
	
	@GET(Context+"/ADUser/{id}")
	public Call<User> getUser(@Path("id") String userId);
	
	@GET(Context+"/ADUser")
	public Call<List<User>> getUserList(@Query("_where") String query);
	
	@GET(Context+"/ADUser")
	public void getUserList(@Query("_where") String query, Callback<List<User>> cb);
	/*
	 * Language Services
	 */
	
	@GET(Context+"/ADLanguage/{id}")
	public Call<Language> getLanguage(@Path("id") String languageId);
	
	@GET(Context+"/ADLanguage")
	public Call<List<Language>> getLanguageList(@Query("_where") String query);
	
	/*
	 * Organization Services
	 */
	
	@GET(Context+"/Organization/{id}")
	public Call<Organization> getOrganization(@Path("id") String organizationId);
	
	@GET(Context+"/Organization/{id}")
	public void getOrganization(@Path("id") String organizationId, Callback<Organization> cb);
	
	@GET(Context+"/Organization")
	public Call<List<Organization>> getOrganizationList(@Query("_where") String query);
	
	/*
	 * DocumentType Services
	 */
	
	@GET(Context+"/DocumentType/{id}")
	public Call<DocumentType> getDocType(@Path("id") String docTypeId);
	
	@GET(Context+"/DocumentType")
	public Call<List<DocumentType>> getDocTypeList(@Query("_where") String query);
	
	@GET(Context+"/DocumentType")
	public Call<List<DocumentType>> getDocTypeList(@QueryMap Map<String, String> params);
	
	@GET(Context+"/DocumentType")
	public void getDocTypeList(@Query("_where") String query, Callback<List<DocumentType>> cb);
	
	
	@POST(Context+"/MaterialMgmtInventoryCount")
	public Call<List<InventoryCount>> saveInventory(@Body InventoryCount inventoryCount);
	
	@POST(Context+"/MaterialMgmtInventoryCountLine")
	public Call<List<InventoryCountLine>> saveInventoryLines(@Body List<InventoryCountLine> inventoryCountLineList);
	
	@GET(Context+"/MaterialMgmtStorageDetail")
	public Call<List<StorageDetail>> getStorageDetail(@Query("_where") String query);
	
	@GET(Context+"/PricingPriceList")
	public Call<List<PriceList>> getPriceLists(@Query("_where") String query);
	
	@GET(Context+"/PricingPriceList")
	public Call<List<PriceList>> getPriceLists(@QueryMap Map<String, String> parameters);
	
	@POST(Context+"/ProcurementRequisition")
	public Call<List<Requisition>> saveRequisition(@Body Requisition requisition);
	
	@POST(Context+"/ProcurementRequisitionLine")
	public Call<List<RequisitionLine>> saveRequisitionLines(@Body List<RequisitionLine> requisitionLines);
	
	@GET(Context+"/ProductCategory")
	public Call<List<ProductCategory>> getProductCategories(@QueryMap Map<String, String> parameters);
}

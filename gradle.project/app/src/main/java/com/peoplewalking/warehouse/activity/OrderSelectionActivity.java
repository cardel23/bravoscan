package com.peoplewalking.warehouse.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.dialogs.FilterOrderDialog;
import com.peoplewalking.warehouse.dialogs.FilterOrderDialog.OnFilterOrderListener;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.OrdersAdapter;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.tasks.FindOrderTask;
import com.peoplewalking.warehouse.tasks.LoadOrdersTask;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;

public class OrderSelectionActivity extends Activity implements OnItemClickListener, OnPostExecuteListener<List<OBOrder>>, OnFilterOrderListener{
	private static final String TAG = "OrderSelectionActivity"; 
	private ListView mListviewPendingOrders;
	private NovoventApp app = null;
	private OBOrder selectedOrder = null;
	private BusinessPartner selectedBpartner = null;
	private OrdersAdapter ordersAdapter;
	private LoadOrdersTask loadOrdersTask;
	private Integer startRow;
	private Integer endRow;
	private Integer rowIncrement= 50;
	private Button btnLoadMore;
	private Button btnFilter;
	private SearchView searchView;
	private FilterOrderDialog dialogFilterOrder;
	private Map<String, Object> filterParams;
    private boolean clearList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		app = (NovoventApp) getApplicationContext();
		app.setLocaleConfig();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_selection2);
		setTitle(R.string.select_order);
		mListviewPendingOrders = (ListView) findViewById(R.id.lvOrders);
		mListviewPendingOrders.setOnItemClickListener(this);	
//		mViewSelectedPartner = (TextView) findViewById(R.id.tv_selected_bpartner);
        this.btnLoadMore = new Button(this);
//        this.btnLoadMore.setBackgroundColor(getResources().getColor(R.color.app_base_color));
        this.btnLoadMore.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        this.btnLoadMore.setText("Load more");
        this.btnLoadMore.setVisibility(View.GONE);
        this.btnLoadMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startRow = endRow + 1;
                endRow += rowIncrement;
                filterParams.put(Constants.FILTER_START_ROW, startRow);
                filterParams.put(Constants.FILTER_END_ROW, endRow);
                loadPendingOrders(filterParams);
            }
        });
        mListviewPendingOrders.addFooterView(btnLoadMore);

        this.ordersAdapter = new OrdersAdapter(this, new ArrayList<OBOrder>());
        this.mListviewPendingOrders.setAdapter(this.ordersAdapter);
        selectedBpartner = (BusinessPartner) getIntent().getExtras().getSerializable(Constants.ID_EXTRA_SELECTED_BPARTNER);

        dialogFilterOrder = new FilterOrderDialog();
        btnFilter = (Button) findViewById(R.id.btn_filter);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialogFilterOrder.show(getFragmentManager(), "FilterOrderDialog");
            }
        });
        this.filterParams = new HashMap<String, Object>();
        if(app.getDeliveryList().getListType() == DeliveryList.INBOUND){
            this.filterParams.put(Constants.FILTER_ORDER_TYPE, OBOrder.PURCHASE_TYPE);
        }
        else if(app.getDeliveryList().getListType() == DeliveryList.OUTBOUND){
            this.filterParams.put(Constants.FILTER_ORDER_TYPE, OBOrder.SALES_TYPE);
        }

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		// Inflate menu to add items to action bar if it is present.
		inflater.inflate(R.menu.order_selection2, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager =
				(SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView =
				(SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                CharSequence charSequence = searchView.getQuery();
                findOrder(charSequence.toString());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence charSequence = searchView.getQuery();
                findOrder(charSequence.toString());
            }
        });

		searchView.setOnSearchClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                searchView.clearFocus();
			}
		});
		searchView.setSearchableInfo(
				searchManager.getSearchableInfo(getComponentName()));

		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	     dialogFilterOrder.onActivityResult(requestCode, resultCode, data);
	}

	private void findOrder(String documentNo){
		try{
			FindOrderTask findOrderTask = new FindOrderTask(this, filterParams);
			findOrderTask.setOnPostExecuteListener(new OnPostExecuteListener<List<OBOrder>>() {
                @Override
                public void onPostExecute(List<OBOrder> response, List<String> errorMessages) {
                    ordersAdapter.clear();
                    ordersAdapter.addAll(response);
                    ordersAdapter.notifyDataSetChanged();
                    clearList = true;
                    btnLoadMore.setVisibility(View.GONE);
                }
            });
			findOrderTask.execute(documentNo);
		}
		catch (Exception e){
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
			Log.e(TAG, e.getMessage(), e);
		}
	}
	
	private void loadPendingOrders(Map<String, Object> filterParams){
	    try {
		   loadOrdersTask = new LoadOrdersTask(this, filterParams);
		   loadOrdersTask.setOnPostExecuteListener(this);
		   loadOrdersTask.execute();
		} catch (Exception e) {
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
			Log.e(TAG, e.getMessage(), e);
		}
	}

	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
	    try {
			selectedOrder = (OBOrder) adapter.getItemAtPosition(position);
			Intent intent = new Intent();
			Bundle mBundle = new Bundle();
			mBundle.putSerializable(Constants.ID_EXTRA_SELECTED_ORDER, selectedOrder);
			intent.putExtras(mBundle);
			setResult(Constants.REQUEST_CODE_ORDER, intent);
			finish();
	    } catch (Exception e) {
			Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();

			Log.e(TAG, e.getMessage(), e);
	    }
	}

	public void onPostExecute(List<OBOrder> pendingOrderList,
		List<String> errorMessages) {
	    try {
            if(clearList){
                this.ordersAdapter.clear();
                clearList = false;
            }

		this.ordersAdapter.addAll(pendingOrderList);
		this.ordersAdapter.notifyDataSetChanged();
		if(this.ordersAdapter.isEmpty()){
		    Toast.makeText(this, R.string.no_orders_found_with_filters , Toast.LENGTH_LONG).show();
		}

		if(pendingOrderList.size() < rowIncrement){
		    this.btnLoadMore.setVisibility(View.GONE);		    
		} else{
		    this.btnLoadMore.setVisibility(View.VISIBLE);
		}		
		if(errorMessages != null){
		    for(String errorMsg : errorMessages){
			Toast.makeText(OrderSelectionActivity.this, errorMsg, Toast.LENGTH_LONG).show();
		    }
		} 
	    } catch (Exception e) {
		Toast.makeText(this, getString(R.string.unexpected_error), Toast.LENGTH_LONG);
		Log.e(TAG, e.getMessage(), e);
	    }
	}

	public void onFilter(DialogInterface dialog,
		Map<String, Object> filterParams) {
	    this.filterParams.putAll(filterParams);
	    startRow = 0;
	    endRow = startRow + rowIncrement;
	    this.filterParams.put(Constants.FILTER_START_ROW, startRow);
	    this.filterParams.put(Constants.FILTER_END_ROW, endRow);
	    loadPendingOrders(this.filterParams);
	}

}

package com.peoplewalking.warehouse.openbravo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoodsReceipt extends OBModel implements Serializable{	
	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 8441503636371686852L;


	@Expose
	@SerializedName("movementDate")
	private String movementDate;
	@Expose
	@SerializedName("documentNo")
	private String documentNo;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("processed")
	private boolean processed;
	@Expose
	@SerializedName("businessPartner")
	private String partnerId;
	@Expose
	@SerializedName("businessPartner$_identifier")
	private String partnerIdentifier;
	@Expose
	@SerializedName("partnerAddress")
	private String partnerLocationId;
	@Expose
	@SerializedName("partnerAddress$_identifier")
	private String partnerLocationIdentifier;
	@Expose
	@SerializedName("warehouse")
	private String warehouseId;
	@Expose
	@SerializedName("warehouse$_identifier")
	private String warehouseIdenfier;
	@Expose
	@SerializedName("documentType")
	private String docTypeId;
	@Expose
	@SerializedName(value = "documentType$_identifier")
	private String docTypeIdentifier;
	@Expose
	@SerializedName(value = "accountingDate")
	private String accountingDate;
	@Expose
	@SerializedName("salesOrder")
	private String salesOrderId;
	@Expose
	@SerializedName("salesOrder$_identifier")
	private String salesOrderIdentifier;
	@Expose
	@SerializedName("organization")
	private String organizationId;
	@Expose
	@SerializedName("salesTransaction")
	private boolean salesTransaction;
	
	
	
	private ArrayList<String>  errors;
	 
	public String getAccountingDate() {
		return accountingDate;
	}

	public void setAccountingDate(String accountingDate) {
		this.accountingDate = accountingDate;
	}

	public ArrayList<String> getErrors() {
		this.isValid();
		return errors;
	}

	public void setErrors(ArrayList<String> errors) {
		this.errors = errors;
	}
	public GoodsReceipt(){
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getMovementDate() {
		return movementDate;
	}

	public void setMovementDate(String movementDate) {
		this.movementDate = movementDate;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerIdentifier() {
		return partnerIdentifier;
	}

	public void setPartnerIdentifier(String partnerIdentifier) {
		this.partnerIdentifier = partnerIdentifier;
	}

	public String getPartnerLocationId() {
		return partnerLocationId;
	}

	public void setPartnerLocationId(String partnerLocationId) {
		this.partnerLocationId = partnerLocationId;
	}

	public String getPartnerLocationIdentifier() {
		return partnerLocationIdentifier;
	}

	public void setPartnerLocationIdentifier(String partnerLocationIdentifier) {
		this.partnerLocationIdentifier = partnerLocationIdentifier;
	}

	public String getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}

	public String getWarehouseIdenfier() {
		return warehouseIdenfier;
	}

	public void setWarehouseIdenfier(String warehouseIdenfier) {
		this.warehouseIdenfier = warehouseIdenfier;
	}

	public String getDocTypeId() {
		return docTypeId;
	}

	public void setDocTypeId(String docTypeId) {
		this.docTypeId = docTypeId;
	}

	public String getDocTypeIdentifier() {
		return docTypeIdentifier;
	}

	public void setDocTypeIdentifier(String docTypeIdentifier) {
		this.docTypeIdentifier = docTypeIdentifier;
	}
	
	public String getSalesOrderId() {
	    return salesOrderId;
	}

	public void setSalesOrderId(String salesOrderId) {
	    this.salesOrderId = salesOrderId;
	}

	public String getSalesOrderIdentifier() {
	    return salesOrderIdentifier;
	}

	public void setSalesOrderIdentifier(String salesOrderIdentifier) {
	    this.salesOrderIdentifier = salesOrderIdentifier;
	}

	public boolean isValid() {
		this.errors = new ArrayList<String>();
		boolean valid = true;
		
		if(this.documentNo == null){
			valid = false;
			this.errors.add("El campo DocumentNo  es requerido");
		}
		if(this.docTypeId == null){
			valid = false;
			this.errors.add("El campo DocType es requerido");
			}
		if(this.warehouseId == null){
			valid = false;
			this.errors.add("El campo Warehouse es requerido");
			}
		if(this.partnerId == null){
			valid = false;
			this.errors.add("El campo Businesspartne es requerido");
			}
		if(this.partnerLocationId == null){
			valid = false;
			this.errors.add("el campo BusinessPartnerLocation es requerido");
		}
		if(this.movementDate == null){
			valid = false;
			this.errors.add("El campo MovementDate es requerido");
		}
		if(this.accountingDate == null){
			valid = false;
			this.errors.add("El campo AccountingDate es requerido");
		}
		return valid;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public boolean isSalesTransaction() {
		return salesTransaction;
	}

	public void setSalesTransaction(boolean salesTransaction) {
		this.salesTransaction = salesTransaction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

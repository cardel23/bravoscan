package com.peoplewalking.warehouse.viewhelpers;

import com.peoplewalking.warehouse.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoadingViewHelper extends ViewHelper{
    private static final String TAG = "LoadingViewHelper";
	LinearLayout progressBarContainer;
	View container;
	ViewGroup parentContainer;
	
	public LoadingViewHelper(Context context, View viewToHide) {
	    super(context);
	    try {
		this.context = context;
		this.container = viewToHide;
		this.progressBarContainer = (LinearLayout) LayoutInflater.from(this.context).inflate(R.layout.template_loading_progressbar, null);
		this.parentContainer = (ViewGroup) this.container.getParent();		
		if(parentContainer != null){
		    this.parentContainer.addView(this.progressBarContainer, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		}
	    } catch (Exception e) {
		Toast.makeText(context, "Error while loading layout", Toast.LENGTH_LONG).show();
		Log.e(TAG, e.getMessage(), e);
		e.printStackTrace();
	    }
	}

	public void showProgress(final boolean show) {
	    if(show){
		this.replaceView(container, progressBarContainer);
	    } else{
		this.replaceView(progressBarContainer, container);
	    }
	}

}

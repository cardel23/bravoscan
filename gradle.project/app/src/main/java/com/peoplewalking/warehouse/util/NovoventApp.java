package com.peoplewalking.warehouse.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import com.orm.SugarApp;
import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.InitialActivity;
import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.GoodsReceipt;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.openbravo.model.Organization;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.openbravo.model.ProductCategory;
import com.peoplewalking.warehouse.openbravo.model.Requisition;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.openbravo.model.Warehouse;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.widget.Toast;

import retrofit.Response;

/**
 * Contexto de la aplicación Bravoscan
 * @author Carlos Delgadillo
 * fecha 13/04/2015
 *
 */
public class NovoventApp extends SugarApp {
	private OpenbravoService obService = null;
	private User user;
	private String selectedCodeList;
	private String selectedBarCode;
	private DeliveryList deliveryList;
	private OBOrder obOrder;
	private GoodsReceipt goodReceipt;
	private Locale locale;
	private SharedPreferences sharedPreferences;
	private static NovoventApp instance;
	private Intent homeIntent;
	private File imageDir;
	private Organization organization;
	private List<Warehouse> warehouseList;
	private List<DocumentType> docTypeList;
	private List<DocumentType> outboundDocList;
	private List<DocumentType> inboundDocList;
	private List<StorageBin> storageBins;
	private List<BusinessPartner> vendors;
	private List<PriceList> priceLists;
	private List<ProductCategory> categoryList;
	private String server;
	private int currentFragment;
	private Requisition currentRequisition;
	private BusinessPartner currentBusinessPartner;
	public static final String SERVER = "server";
	private static final String TAG = "BRAVOSCAN";
	private static final String KEY_APP_CRASHED = "UnhandledException";
	private List<Organization> organizations;
	private Map<String, Object> filterParams;

	@Override
	public void onCreate() {
	    super.onCreate();
        StrictMode.ThreadPolicy policy = new
        		StrictMode.ThreadPolicy.Builder()
        		.permitAll().build();
        		StrictMode.setThreadPolicy(policy);
	    ContextWrapper cw = new ContextWrapper(getApplicationContext());
	    imageDir = cw.getDir("images", Context.MODE_PRIVATE);
	    final UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler( new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable exception) {
                // Save the fact we crashed out.
                getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
                    .putBoolean( KEY_APP_CRASHED, true ).apply();
                // Chain default exception handler.
                if ( defaultHandler != null ) {
                    defaultHandler.uncaughtException( thread, exception );
                }
            }
        } );

        boolean bRestartAfterCrash = getSharedPreferences( TAG , Context.MODE_PRIVATE )
                .getBoolean( KEY_APP_CRASHED, false );
        if ( bRestartAfterCrash ) {
            // Clear crash flag.
            getSharedPreferences( TAG , Context.MODE_PRIVATE ).edit()
                .putBoolean( KEY_APP_CRASHED, false ).apply();
            // Re-launch from root activity with cleared stack.
            Intent intent = new Intent( this, InitialActivity.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
            Toast.makeText(this, "Se ha producido un error inesperado", Toast.LENGTH_SHORT).show();
            startActivity( intent );
            Toast.makeText(this, "Se ha producido un error inesperado", Toast.LENGTH_SHORT).show();
        }
        
	}
	
	
	public String getSelectedCodeList() {
		return selectedCodeList;
	}

	public void setSelectedCodeList(String selectedCodeList) {
		this.selectedCodeList = selectedCodeList;
	}
	
	public String getSelectedBarCode() {
		return selectedBarCode;
	}
	
	public void setSelectedBarCode(String selectedBarCode) {
		this.selectedBarCode = selectedBarCode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public DeliveryList getDeliveryList() {
		return deliveryList;
	}

	public void setDeliveryList(DeliveryList deliveryList) {
		this.deliveryList = deliveryList;
	}

	public OBOrder getObOrder() {
		return obOrder;
	}

	public void setObOrder(OBOrder obOrder) {
		this.obOrder = obOrder;
	}

	public GoodsReceipt getGoodReceipt() {
		return goodReceipt;
	}

	public void setGoodReceipt(GoodsReceipt goodReceipt) {
		this.goodReceipt = goodReceipt;
	}

	public OpenbravoService getObService() {
		return obService;
	}

	public void setObService(OpenbravoService obService) {
		this.obService = obService;
	}	
	
	public boolean isConnected(){
	    ConnectivityManager cm =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    return netInfo != null && netInfo.isConnectedOrConnecting();
	}
	
	public static NovoventApp getInstance(){
		if(instance == null){
			instance = (NovoventApp)new NovoventApp().getSugarContext();
		}
		return instance;
	}
	
	public static NovoventApp getNewInstance(){
		return (NovoventApp) new NovoventApp().getSugarContext();
	}
	
	/**
	 * Crea un locale por defecto si no existe en las propiedades
	 * @return el locale de la app
	 * @author Carlos Delgadillo
	 * @fecha 05/05/2015
	 */
	public Locale getLocale() {
		if(locale == null){
			String lang = getPrefs().getString("locale",null);
			if(lang == null){
				editSharedPreference("locale", "es");
				locale = new Locale("es");
			}
			else
				locale = new Locale(lang);
		}
		Locale.setDefault(locale);
		return Locale.getDefault();
	}
	
	public void setLocale(Locale locale) {
		this.locale = locale;
		editSharedPreference("locale", locale.getLanguage());
		setLocaleConfig();
	}
	
	
	public String getWarehouse(){
		String warehouse = getPrefs().getString("defaultWarehouse_"+getUser().getUsername(), null);
		if(warehouse == null){
			warehouse = getUser().getDefaultWarehouseId();
			setWarehouse(warehouse);
		}
		return warehouse;
	}
	
	private void setWarehouse(String warehouse){
		editSharedPreference("defaultWarehouse_"+getUser().getUsername(), warehouse);
	}
	
	public boolean isServerSet(){
		if(getServer() == null)
			return false;
		return true;
	}
	
	public String getServer(){
		server = getPrefs().getString(SERVER, null);
		return server;
	}
	
	/**
	 * Busca el id para el tipo de documento por defecto para albaranes de entrada. Si no existe, asigna por defecto el tipo de documento
	 * correspondiente a la organización a la que pertenece el usuario
	 * @return string con el id del documento
	 * @author Carlos Delgadillo
	 * @fecha 29/05/2015
	 */
	public String getDefaultInboundPreference(){
		String inbound = getPrefs().getString("defaultInbound_"+getUser().getUsername(), null);
		try {
			if(inbound == null){
				inbound = getInboundDocList().get(0).getObId();
				setDefaultInboundPreference(inbound);
			}
			return inbound;
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage();
		}
		
	}
	
	/**
	 * Igual que el anterior, busca el id para el tipo de documento por defecto para albaranes de salida. Si no existe, asigna por defecto el tipo de documento
	 * correspondiente a la organización a la que pertenece el usuario
	 * @return string con el id del documento
	 * @author Carlos Delgadillo
	 * @fecha 29/05/2015
	 */
	public String getDefaultOutboundPreference(){
		try {
			String outbound = getPrefs().getString("defaultOutbound_"+getUser().getUsername(), null);
			if(outbound == null){
				outbound = getOutboundDocList().get(0).getObId();
				setDefaultOutboundPreference(outbound);
			}
			return outbound;
		} catch (Exception e) {
			// TODO: handle exception
			return e.getMessage();
		}
		
	}
	
	public void setDefaultInboundPreference(String inbound){
		try {
//			inbound = getInboundDocList().get(0).getObId();
			editSharedPreference("defaultInbound_"+getUser().getUsername(), inbound);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void setDefaultOutboundPreference(String outbound){
		try {
//			outbound = getInboundDocList().get(0).getObId();
			editSharedPreference("defaultOutbound_"+getUser().getUsername(), outbound);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * 
	 * @param preference
	 * @return
	 * @author Carlos Delgadillo
	 * @fecha 31/05/2015
	 */
	public DocumentType getDefaultInboundFromList(String preference){
		for(DocumentType t : getInboundDocList()){
			if(t.getObId().equals(preference))
				return t;
		}
		return null;
	}
	
	/**
	 * 
	 * @param preference
	 * @return
	 * @author Carlos Delgadillo
	 * @fecha 31/05/2015
	 */
	public DocumentType getDefaultOutboundFromList(String preference){
		for(DocumentType t : getOutboundDocList()){
			if(t.getObId().equals(preference))
				return t;
		}
		return null;
	}
	
	public String getSavedUser(){
		String user = getPrefs().getString("savedUser", null);
		return user;
	}
	
	public String getSavedPass(){
		String pass = getPrefs().getString("savedPass", null);
		return pass;
	}
	
	public void setServer(String server){
	    editSharedPreference(SERVER, server);
	}
	
	public void setApplicationPort(Integer port){
		editSharedPreference("defaultPort", port);
	}
	
	public Integer getApplicationPort(){
		Integer port = getPrefs().getInt("defaultPort", 0);
		if(port == 0){
			editSharedPreference("defaultPort", 80);
			port = 80;
		}
		return port;
	}
	
	public String getServerFromContext(){
		if(server == null)
			return getServer();
		else
			return server;
	}
	/**
	 * Crea una instancia de SharedPreferences para la aplicación
	 * @return las preferencias del usuario
	 * @author Carlos Delgadillo
	 * @fecha 05/05/2015
	 */
	public SharedPreferences getPrefs(){
		if(sharedPreferences == null)
			sharedPreferences = getSharedPreferences("BRAVOSCAN", Context.MODE_PRIVATE);
		return sharedPreferences;
	}
	
	public boolean isLogged(){
		if(getUser() != null)
			return true;
		return false;
	}
	
	/**
	 * Edita un parámetro de configuración (SharedPreference)
	 * @param preferenceName nombre de la preferencia
	 * @param val valor asignado
	 * @author Carlos Delgadillo
	 * @fecha 06/05/2015
	 */
	public void editSharedPreference(String preferenceName, String val){
		SharedPreferences.Editor editor = getPrefs().edit();
		editor.putString(preferenceName, val);
		editor.commit();
	}
	
	/**
	 * Edita un parámetro de configuración (SharedPreference)
	 * @param preferenceName
	 * @param val valor asignado
	 * @author Carlos Delgadillo
	 * @fecha 06/05/2015
	 */
	public void editSharedPreference(String preferenceName, int val){
		SharedPreferences.Editor editor = getPrefs().edit();
		editor.putInt(preferenceName, val);
		editor.commit();
	}
	
	/**
	 * Setea la configuración del locale para las actividades
	 * @author Carlos Delgadillo
	 * @fecha 06/05/2015
	 */
	public void setLocaleConfig(){
		Configuration config = this.getResources().getConfiguration();
		config.locale = getLocale();
		getResources().updateConfiguration(config, getResources().getDisplayMetrics());
	}
	
	/**
	 * Define un intent de inicio para la navegación
	 * @param intent
	 * @author Carlos Delgadillo
	 * fecha 15/05/2015
	 */
	public void setIntent(Intent intent){
		this.homeIntent = intent;
	}
	
	/**
	 * Retorna el intent home para navegar a la actividad principal
	 * @return el intent definido
	 * @author Carlos Delgadillo
	 * fecha 15/05/2015
	 */
	public Intent getHomeIntent(){
		return homeIntent;
	}
	
	public String saveImageToInternalSorage(Bitmap bitmapImage, String dir, String imageName){
			File imageDir=new File(this.imageDir, dir); 
			if(!imageDir.exists()){
				imageDir.mkdirs();
			}
		
			File imagePath=new File(imageDir, imageName); 
			
	        FileOutputStream fos = null;
	        try {    
	            fos = new FileOutputStream(imagePath);
	            // Use the compress method on the BitMap object to write image to the OutputStream
	            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
	            fos.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        //return relative image path
	        return dir.concat("/").concat(imageName);
	 }
	
	public Bitmap loadImageFromInternalStorage(String relativeImagePath){
	    try {
	    	if(relativeImagePath != null && !"".equals(relativeImagePath)){
		        File f=new File(this.imageDir, relativeImagePath);
		        return BitmapFactory.decodeStream(new FileInputStream(f));	
	    	}
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (Exception e2) {
			e2.printStackTrace();
		}
        return BitmapFactory.decodeResource(getResources(), R.drawable.app_ic_launcher);
	}

	public File getImageDir() {
	    return imageDir;
	}

	public void setImageDir(File imageDir) {
	    this.imageDir = imageDir;
	}

	public Organization getOrganization() {
//		if(organization == null)
//			setOrganization();
		return organization;
	}
	
	public void setOrganization(Organization organization){
		this.organization = organization;
	}
	
//	public Organization setOrganization(){
//		try {
//			OBServiceClient.getOBService().getOrganization(getUser().getDefaultOrganizationId(), new Callback<Organization>() {
//
//				public void success(Organization t, Response response) {
//					// TODO Auto-generated method stub
//					organization = t;
////					return t;
//
//				}
//
//				public void failure(RetrofitError error) {
//					// TODO Auto-generated method stub
////					return null;
//				}
//			});
//			return organization;
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//			return null;
//		}
//
//	}
	
	public List<Warehouse> getWarehouseList(){
//		if(warehouseList == null)
//			setWarehouseList();
		return warehouseList;
	}
	
	public void setWarehouseList(List<Warehouse> warehouseList){
		this.warehouseList = warehouseList;
	}
	
//	public void setWarehouseList(){
//		try {
//			obService.getWarehouseList("organization='"+getOrganization().getObId()+"'", new Callback<List<Warehouse>>() {
//
//				public void success(List<Warehouse> t, Response response) {
//					// TODO Auto-generated method stub
//					warehouseList = t;
//				}
//
//				public void failure(RetrofitError error) {
//					// TODO Auto-generated method stub
//
//				}
//			});
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//	}
	
	public void setDocTypeList(List<DocumentType> docTypes){
		docTypeList = docTypes;
	}
	
//	/**
//	 * Carga los tipos de documento para la aplicación
//	 * @author Carlos Delgadillo
//	 * @fecha 29/05/2015
//	 */
//	public void setDocTypeList(){
//		OBServiceClient.getOBService().getDocTypeList("",new Callback<List<DocumentType>>() {
//
//			public void success(List<DocumentType> t, Response response) {
//				docTypeList = t;
//			}
//
//			public void failure(RetrofitError error) {
//				error.printStackTrace();
//			}
//		});
//	}
	
//	public List<DocumentType> getDocTypeList(){
//		if(docTypeList == null)
//			setDocTypeList();
//		return docTypeList;
//	}
	
//	/**
//	 * Lista de documentos específicos por categoría de entrada o salida
//	 * @param documentCategory la categoría especificada
//	 * @return lista de DocumentType
//	 * @author Carlos Delgadillo
//	 * @fecha 29/05/2015
//	 */
//	public List<DocumentType> getInOutDocTypes(String documentCategory){
//		List<DocumentType> inOutDocTypes = new ArrayList<DocumentType>();
//		for(DocumentType docType : getDocTypeList()){
//			if(docType.getDocumentCategory() == documentCategory)
//				inOutDocTypes.add(docType);
//		}
//		return inOutDocTypes;
//	}
	
	
	public List<DocumentType> getOutboundDocList() {
		return outboundDocList;
	}


	public void setOutboundDocList(List<DocumentType> outboundDocList) {
		this.outboundDocList = outboundDocList;
	}


	public List<DocumentType> getInboundDocList() {
		return inboundDocList;
	}


	public void setInboundDocList(List<DocumentType> inboundDocList) {
		this.inboundDocList = inboundDocList;
	}
	
	/**
	 * @author Carlos Delgadillo
	 * @fecha 13/06/2015
	 * @return storageBins
	 */
	public List<StorageBin> getStorageBins() {
		return storageBins;
	}
	
	/**
	 * @author Carlos Delgadillo
	 * @fecha 13/06/2015
	 * @param storageBins
	 */
	public void setStorageBins(List<StorageBin> storageBins) {
		this.storageBins = storageBins;
	}


//	/**
//	 * Obtiene un tipo de documento específico
//	 * @param documentCategory la categoría del documento
//	 * @param organization la organización a la que pertenece el usuario
//	 * @return el documento encontrado o null si no hay ninguno con esos criterios
//	 * @author Carlos Delgadillo
//	 * @fecha 29/05/2015
//	 */
//	private DocumentType getDocumentType(String documentCategory, String organization){
//		for(DocumentType d : getDocTypeList()){
//			if(d.getDocumentCategory() == documentCategory && d.getOrganizationIdentifier() == getOrganization().getObId()){
//				return d;
//			}
//		}
//		return null;
//	}
	
	public void updatePreferences() throws IOException {
		setWarehouse(getUser().getDefaultWarehouseId());
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("_where", "documentCategory='MMR'");
		params.put("_selectedProperties", "name,printText,documentCategory,organization,id");

		setInboundDocList(getObService().getDocTypeList(params).execute().body());
		params.put("_where", "documentCategory='MMS'");
		setOutboundDocList(getObService().getDocTypeList(params).execute().body());
		params.put("_where", "warehouse='"+getWarehouse()+"'");
		params.put("_selectedProperties", "id,warehouse,rowX,stackY,levelZ,searchKey");
		params.put("_orderBy", "searchKey");
		setStorageBins(getObService().getStorageBinList(params).execute().body());
	}
	
	public List<BusinessPartner> getVendors() {
		return vendors;
	}


	public void setVendors(List<BusinessPartner> vendors) {
		this.vendors = vendors;
	}


	public List<PriceList> getPriceLists() {
		return priceLists;
	}


	public void setPriceLists(List<PriceList> priceLists) {
		this.priceLists = priceLists;
	}


	public Requisition getCurrentRequisition() {
		return currentRequisition;
	}


	public void setCurrentRequisition(Requisition currentRequisition) {
		this.currentRequisition = currentRequisition;
	}


	public List<ProductCategory> getCategoryList() {
		return categoryList;
	}


	public void setCategoryList(List<ProductCategory> categoryList) {
		this.categoryList = categoryList;
	}


	public BusinessPartner getCurrentBusinessPartner() {
		return currentBusinessPartner;
	}


	public void setCurrentBusinessPartner(BusinessPartner currentBusinessPartner) {
		this.currentBusinessPartner = currentBusinessPartner;
	}


	public int getCurrentFragment() {
		try {
			return currentFragment;
		} catch (Exception e) {
			return 0;
		}
		
	}


	public void setCurrentFragment(int currentFragment) {
		this.currentFragment = currentFragment;
	}


	public boolean logout(){
		setUser(null);
		setWarehouseList(null);
		setOrganization(null);
		setPriceLists(null);
		setVendors(null);
		return true;
	}

	public List<Organization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<Organization> organizations) {
		this.organizations = organizations;
	}

	public Map<String, Object> getFilterParams() {
		return filterParams;
	}

	public void setFilterParams(Map<String, Object> filterParams) {
		this.filterParams = filterParams;
	}
}

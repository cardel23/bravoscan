package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;

import android.content.Context;
import android.util.Log;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class BusinessPartnerAdapter extends ArrayAdapter<BusinessPartner>{
    	private static final String TAG = "BusinessPartnerAdapter";
	private Context context;
	private List<BusinessPartner> bPartnerList;
	private LayoutInflater layoutInflater;
	private TextView mTextView;
	
	public BusinessPartnerAdapter(Context context, List<BusinessPartner> bPartnerList){
		super(context, android.R.layout.simple_spinner_dropdown_item);
		this.context = context;
		this.bPartnerList = bPartnerList;
		if(this.bPartnerList == null){
			this.bPartnerList = new ArrayList<BusinessPartner>();
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setText(bPartnerList.get(position).getIdentifier());
		mTextView.setTextColor(Color.BLACK);
		return view;
	}
	
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setText(bPartnerList.get(position).getIdentifier());
		mTextView.setTextColor(Color.BLACK);
		return view;
	}
	
	@Override
	public int getCount() {
		return bPartnerList == null ? 0 : bPartnerList.size();
	}
	
	@Override
	public BusinessPartner getItem(int position) {
		return (bPartnerList == null || bPartnerList.isEmpty()) ? null : bPartnerList.get(position);
	}
	
	@Override
	public void addAll(Collection<? extends BusinessPartner> collection) {
		if(this.bPartnerList != null){
			this.bPartnerList.addAll(collection);
			this.notifyDataSetChanged();
		}
		
	}
	
	@Override
	public void clear() {
		if(this.bPartnerList != null){
			this.bPartnerList.clear();
			this.notifyDataSetChanged();
		}
		
	}
	
	public int getItemPosition(BusinessPartner bPartner){
	    try {
		   if(bPartnerList != null && bPartner!= null){	
        		BusinessPartner currentBP;
        		for(int position = 0, lenght = bPartnerList.size(); position < lenght; position++){
        		    currentBP = bPartnerList.get(0);
        		    if(currentBP.getObId() != null && currentBP.getObId().equals(bPartner)){
        			return position;
        		    }
        		}
		    }
	    } catch (Exception e) {
		Toast.makeText(context, context.getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
		Log.e(TAG, e.getMessage(), e);
	    }
	    return -1;
	}	
}

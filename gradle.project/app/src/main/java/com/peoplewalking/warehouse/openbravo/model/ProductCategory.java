package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductCategory extends OBModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6741538629757238609L;

	@Expose
	@SerializedName("name")
	private String name;
	
	public ProductCategory() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
}

package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import retrofit.RetrofitError;
import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.util.Constants;

public class LoadOrdersTask  extends TaskWithCallback<Void, Void, List<OBOrder>>{
    private static final String TAG = "LoadOrdersTask";
    private Map<String, Object> filterParams;

    public LoadOrdersTask(Context context, Map<String, Object> filterParams) {
		super(context);
		this.filterParams = filterParams;
    }

    @Override
    protected List<OBOrder> doInBackground(Void... args ) {
	List<OBOrder> orderList = null;
	this.errorMessages = new ArrayList<String>();
	try {	    	
	    	StringBuilder ordersQuery = new StringBuilder();
	    		ordersQuery.append("e in (");
//    	    	ordersQuery.append("as o where o in (") esta es la línea del problema
            	query.append("SELECT distinct o FROM Order o ");
		query.append("left join o.orderLineList ol ")
          		.append("left join ol.procurementPOInvoiceMatchList pom ")
            		.append("left join o.documentType dt ")
            		.append("WHERE 1=1 "); 
    	  	
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_TYPE)){
    	  	    if(filterParams.get(Constants.FILTER_ORDER_TYPE).equals(OBOrder.PURCHASE_TYPE)) {
					query.append("AND o.salesTransaction = 'N' ");
		    } else{
					query.append("AND o.salesTransaction = 'Y' ");
    	  	    }
    	  	}    	    		
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_PARTNER)){
    	  	    BusinessPartner selectedPartner = (BusinessPartner) filterParams.get(Constants.FILTER_ORDER_PARTNER);
    	  	    if(selectedPartner != null){
					query.append("AND o.businessPartner='" + selectedPartner.getObId() + "' ");
    	  	    }    	  	    
    	    	}    	  	
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_DATE_FROM)){
    	  	    String dateFrom = (String)filterParams.get(Constants.FILTER_ORDER_DATE_FROM);
    	  	    if(dateFrom != null && !"".equals(dateFrom)){
					query.append("AND o.creationDate > '" + dateFrom + "' ");
    	  	    }
    	  	}
    	  	if(filterParams.containsKey(Constants.FILTER_ORDER_DATE_TO)){
    	  	    String dateTo = (String)filterParams.get(Constants.FILTER_ORDER_DATE_TO);
    	  	    if(dateTo != null && !"".equals(dateTo)){
					query.append("AND o.creationDate < '" + dateTo + "' ");
    	  	    }
    	  	}

		query.append("AND o.documentStatus = 'CO' ")
               		.append("AND dt.return = 'N' ")
               		.append("GROUP BY o, ol,ol.orderedQuantity ")
               		.append("HAVING ol.orderedQuantity > sum(coalesce(pom.quantity,0)))");
    	    	ordersQuery.append(query.toString());
		ordersQuery.append(")");
    	    	Map<String, String> parameters = new HashMap<String, String>();
    	    	parameters.put("_where", ordersQuery.toString());
    	    	
    	        if(filterParams.containsKey(Constants.FILTER_START_ROW)){
    	    		parameters.put("_startRow", String.valueOf((Integer)filterParams.get(Constants.FILTER_START_ROW)));
    	        }
    	        if(filterParams.containsKey(Constants.FILTER_END_ROW)){
    	    		parameters.put("_endRow", String.valueOf((Integer)filterParams.get(Constants.FILTER_END_ROW)));
    	        }	
    	        parameters.put("_selectedProperties", "id,documentNo,processed,summedLineAmount,businessPartner,documentType,currency");
    	    	orderList = app.getObService().getOrderList(parameters).execute().body();
    		if(orderList == null || orderList.isEmpty()){
				ordersQuery = new StringBuilder();
				ordersQuery.append("as o where o in (")
						.append(query.toString())
						.append(")");
				parameters.put("_where", ordersQuery.toString());
				orderList = app.getObService().getOrderList(parameters).execute().body();
				if(orderList == null || orderList.isEmpty()) {
					this.errorMessages.add(app.getString(R.string.no_orders_found_with_filters));
					Log.d(TAG, app.getString(R.string.no_orders_found_with_filters));
				}
			}

	} catch (Exception e) {
	        this.errorMessages.add(app.getString(R.string.unexpected_error));
		Log.e(TAG, e.toString());
	}
	return orderList;
    }

}

package com.peoplewalking.warehouse.util;

import java.io.IOException;
import java.util.Properties;

import android.content.Context;

public class PropertyManager {
	private Context appContext;
	
	public PropertyManager(Context context){
		this.appContext = context;
	}

	public Context getAppContext() {
		return appContext;
	}

	public void setAppContext(Context appContext) {
		this.appContext = appContext;
	}
	
	public Properties getConfigurationProperties() throws IOException{
		Properties configProperties = new Properties();
		configProperties.load(this.appContext.getAssets().open("config.properties"));
		return configProperties;
	}
	
	
}

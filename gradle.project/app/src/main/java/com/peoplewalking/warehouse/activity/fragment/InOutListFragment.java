package com.peoplewalking.warehouse.activity.fragment;

import java.util.ArrayList;
import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.ListTabsActivity;
import com.peoplewalking.warehouse.activity.NewScanListActivity;
import com.peoplewalking.warehouse.activity.ScanListDetailActivity;
import com.peoplewalking.warehouse.openbravo.adapters.DeliveryListAdapter;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;

public class InOutListFragment extends PlaceholderFragment implements OnClickListener{

    private Button mBtnNewList, menuBtn;
    private TextView  contentTxt;
    private ListView mViewProductItems;
    private DeliveryListAdapter deliveryListAdapter;
    private List<DeliveryList> deliveryLists;
    protected ActionMode mActionMode;
    private MenuItem mMenuItemEdit;
    
    
	public InOutListFragment() {
		// TODO Auto-generated constructor stub
	}

	public InOutListFragment(int layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		this.rootView = super.onCreateView(inflater, container, savedInstanceState);
		initListDisplayActivity();
		return rootView;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
	}
	

    private void initListDisplayActivity(){
    	if(app == null)
        	app = (NovoventApp) getActivity().getApplicationContext();
        mBtnNewList = (Button) rootView.findViewById(R.id.codeList_button);
        menuBtn = (Button) rootView.findViewById(R.id.menu_button);
        contentTxt = (TextView) rootView.findViewById(R.id.textView1);
        mViewProductItems = (ListView) rootView.findViewById(android.R.id.list);
        mBtnNewList.setText(R.string.title_activity_new_list);
        menuBtn.setText(R.string.menu_button);
        try {
			deliveryLists = DeliveryList.find(DeliveryList.class, "user = ?", new String[]{String.valueOf(app.getUser().getId())}, null, "sugarid DESC", null);
			if (deliveryLists.size() == 0){
				toggleList(OFF);
				contentTxt.setText(getString(R.string.no_items_found));
			}
			else{
				toggleList(ON);
				deliveryListAdapter = new DeliveryListAdapter(app, deliveryLists);
				mViewProductItems.setLongClickable(true);
				mViewProductItems.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL); 
				
				mViewProductItems.setMultiChoiceModeListener(new MultiChoiceModeListener() {
					private int nr = 0;
					public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
						return false;
					}
					
					public void onDestroyActionMode(ActionMode mode) {
						deliveryListAdapter.removeSelection();
						
					}
					
					public boolean onCreateActionMode(ActionMode mode, Menu menu) {
						nr = 0;
						MenuInflater inflater = getActivity().getMenuInflater();
						inflater.inflate(R.menu.action_list_menu, menu);
						menu.findItem(R.id.action_delete).setTitle(R.string.delete);
						mMenuItemEdit = (MenuItem) menu.findItem(R.id.action_edit);
						mMenuItemEdit.setVisible(false);
						return true;
					}
					
					public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
						switch (item.getItemId()) {
						case R.id.action_delete:
							SparseBooleanArray selected = deliveryListAdapter.getSelectedIds();
	               		   	for(int i = selected.size()-1;i>=0;i--){
	               			   if(selected.valueAt(i)){
	               				   	DeliveryList selectedList = deliveryListAdapter.getItem(selected.keyAt(i));
	               				   	for(DeliveryListItem deliveryListItem : DeliveryListItem.find(
	               						   DeliveryListItem.class,"list_id = ?",String.valueOf(
	               								   selectedList.getId()))){
	               					   	deliveryListItem.delete();
	               				   	}
	               				   	selectedList.delete();
	               				   	deliveryListAdapter.remove(selectedList);
	               			   	}
	               		   	}
	               		   	if(deliveryListAdapter.getCount() == 0)
	               		   		toggleList(OFF);
		               		mode.finish();
			               	return true;
						case R.id.action_edit:
						    mode.finish();
				               	    onItemEdit(selectedListIndex);               	       
				               	    return true;
						default:
						    return false;
						}
					}
					
					public void onItemCheckedStateChanged(ActionMode mode, int position,
							long id, boolean checked) {
					   if (checked) {
        		                       nr++;
        		                       deliveryListAdapter.toggleSelection(position);                   
        		                   } else {
        		                       nr--;
        		                       deliveryListAdapter.toggleSelection(position);                
        		                   }
        		                   mode.setTitle(getString(R.string.selected, nr));
        		                   selectedListIndex = position;
        		                   if(nr == 1){
        		                       mMenuItemEdit.setVisible(false);
        		                   } else{
        		                       mMenuItemEdit.setVisible(false);
        		                   }
        				}
				});
 		        setListAdapter(deliveryListAdapter);
		        mViewProductItems.smoothScrollToPosition(deliveryListAdapter.getCount() - 1);
			}
		} catch (Exception e) {
			Toast.makeText(getActivity(), "Error loading scan lists", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
        
        mBtnNewList.setOnClickListener(this);
        menuBtn.setOnClickListener(this);
    }
    
    @Override
    protected void toggleList(boolean on) {
    	if(!on){
    		mViewProductItems.setVisibility(View.GONE);
			contentTxt.setVisibility(View.VISIBLE);
    	} else if(on){
    		mViewProductItems.setVisibility(View.VISIBLE);
			contentTxt.setVisibility(View.GONE);
    	}
    }
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	if(data != null){
	    	DeliveryList deliveryList = (DeliveryList) data.getSerializableExtra(Constants.ID_EXTRA_NEW_LIST);
	    	if(deliveryLists == null)
	    		deliveryLists = new ArrayList<DeliveryList>();
	    	deliveryLists.add(deliveryList);
	    	if(deliveryListAdapter != null)
	    		deliveryListAdapter.notifyDataSetChanged();
//	    	app.setDeliveryList(deliveryList);
	    	openCodeList();
    	}
    }
    
    private void openCodeList(){
    	Intent i = new Intent(this.ctx, ScanListDetailActivity.class);
    	startActivity(i);
    }
    
    private void onItemEdit(final int position){
	   	LayoutInflater layoutInflater = LayoutInflater.from(this.ctx);
	   	View dialogEdit = layoutInflater.inflate(R.layout.dialog_edit_list, null);
	   	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.ctx);
	   	alertDialogBuilder.setView(dialogEdit);
	   	final EditText newName = (EditText) dialogEdit.findViewById(R.id.et_new_name);
	   	final CheckBox check = (CheckBox) dialogEdit.findViewById(R.id.checkBox1);
	   	final DeliveryList selectedList = deliveryLists.get(position);
	   	final String listName = selectedList.getListName();
	   	// setup a dialog window
	   	alertDialogBuilder.setMessage(getString(R.string.edit))
   			.setCancelable(true)
   			.setPositiveButton(getString(R.string.edit), new DialogInterface.OnClickListener() {
   				public void onClick(DialogInterface dialog, int id) {
   					String textName = newName.getText().toString();
   					final DeliveryList selectedList = deliveryLists.get(position);
   				   	final String listName = selectedList.getListName();
   				    List<DeliveryList> existingDeliveryLists = DeliveryList.find(DeliveryList.class, "list_name = ?", textName);
       				    if(existingDeliveryLists != null && !existingDeliveryLists.isEmpty() && !textName.equals(selectedList.getListName())){
       				    	Toast.makeText(ctx, getString(R.string.list_already_exists, textName), Toast.LENGTH_LONG).show();
       				    }else{
       				    	selectedList.setListName(textName);
       				    	selectedList.setInbound(check.isChecked());
       				    	selectedList.save();
	           				deliveryListAdapter.notifyDataSetChanged();
       				    }
   				}
   			})
   			.setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
   				public void onClick(DialogInterface dialog,	int id) {
   					dialog.cancel();
   				}
   			});
   		alertDialogBuilder.create().show();
       }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
    
//    @Override
//	public void onBackPressed() {
////		this.finish();
//		navigateUpTo(getParentActivityIntent());
//	}
//    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent intent = new Intent(ctx,ListTabsActivity.class);
        	startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    
    public void onClick(View v) {      
        if(v.getId() == R.id.codeList_button){
        	Intent intent = new Intent(ctx, NewScanListActivity.class);
        	startActivityForResult(intent, Constants.REQUEST_CODE_DELIVERY_LIST);
        }
        
        if(v.getId() == R.id.menu_button){
        	getActivity().finish();
        }
    }
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		DeliveryList deliveryList = deliveryListAdapter.getItem(position);
		app = (NovoventApp)getActivity().getApplicationContext();
		app.setDeliveryList(deliveryList);
        openCodeList(); 
	}
	

}

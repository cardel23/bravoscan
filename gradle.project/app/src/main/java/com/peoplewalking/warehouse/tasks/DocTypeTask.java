package com.peoplewalking.warehouse.tasks;

import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.openbravo.model.DocumentType;

import java.util.List;

import retrofit.Response;

/**
 * Created by pwk04 on 10-22-15.
 */
public class DocTypeTask extends TaskWithCallback<String, Void, List<DocumentType>> {

    public DocTypeTask(Context context) {
        super(context);
    }

    @Override
    protected List<DocumentType> executeSynchronous(String... params) {

        List<DocumentType> documentTypes;
        try{
            query.append(params[0]+" ")
                    .append("AND organization IN " + params[1] + "");
            queryParams.put("_where", query.toString());
            queryParams.put("_selectedProperties","id,name,documentCategory");
            service = app.getObService();
            Response response = service.getDocTypeList(queryParams).execute();
            Log.i("RESPONSE",response.raw().request().urlString());
            documentTypes = (List<DocumentType>) response.body();
            return documentTypes;
        }
        catch (Exception e) {
            return null;
        }

    }

    @Override
    protected List<DocumentType> doInBackground(String... params) {
        return executeSynchronous(params);
    }

}

package com.peoplewalking.warehouse.activity.fragment;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 * @author Carlos Delgadillo
 * fecha 17/04/2015
 */
public class PlaceholderFragment extends ListFragment implements OnClickListener {

	protected int LAYOUT_TEMPLATE;
	protected Context ctx;
	protected View rootView;
	protected int selectedListIndex;
	protected NovoventApp app;
	protected final static boolean ON = true;
	protected final static boolean OFF = false;
    
		
	public PlaceholderFragment() {
		this.LAYOUT_TEMPLATE = R.layout.requisition_list_fragment;
	}
	
	public PlaceholderFragment(int layout){
		this.LAYOUT_TEMPLATE = layout;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(this.LAYOUT_TEMPLATE,
				container, false);
		this.ctx = getActivity().getApplicationContext();
		
		return rootView;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	protected void toggleList(boolean on){
    	
    }
	
}

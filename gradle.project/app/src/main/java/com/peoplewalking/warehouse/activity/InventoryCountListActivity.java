package com.peoplewalking.warehouse.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.adapters.CategoryAdapter;
import com.peoplewalking.warehouse.openbravo.adapters.QuantityAdapter;
import com.peoplewalking.warehouse.openbravo.adapters.StorageBinAdapter;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.ProductCategory;
import com.peoplewalking.warehouse.openbravo.model.StorageBin;
import com.peoplewalking.warehouse.tasks.LoadCategoriesTask;
import com.peoplewalking.warehouse.tasks.TaskWithCallback;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.QuantityHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Response;

public class InventoryCountListActivity extends Activity {

    private Spinner locatorSpinner, categorySpinner, quantitySpinner;
    private Button button;
    private StorageBin bin;
    private ProductCategory currentCategory;
    private int currentCondition;
    private NovoventApp app;
    private QuantityHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_count_list);
        app = (NovoventApp) getApplicationContext();
        StorageBinAdapter storageBinAdapter = new StorageBinAdapter(app, app.getStorageBins());

        locatorSpinner = (Spinner) findViewById(R.id.spinner6);
        locatorSpinner.setAdapter(storageBinAdapter);
        locatorSpinner.setPopupBackgroundResource(R.drawable.spinner);
        locatorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                bin = (StorageBin) parent.getItemAtPosition(position);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        setCategorySpinner();
        quantitySpinner = (Spinner) findViewById(R.id.spinner8);
        HashMap<Integer,String> map = new HashMap<Integer, String>();
        map.put(1,"0");
        map.put(2,"< 0");
        map.put(3,"> 0");
        map.put(4,"no 0");

        List<QuantityHelper> lista = new ArrayList<QuantityHelper>();
        lista.add(new QuantityHelper(0,"Seleccionar"));
        lista.add(new QuantityHelper(1,"0"));
        lista.add(new QuantityHelper(2,"< 0"));
        lista.add(new QuantityHelper(3,"> 0"));
        lista.add(new QuantityHelper(4,"no 0"));

        ArrayAdapter<HashMap<Integer,String>> adapter = new ArrayAdapter<HashMap<Integer, String>>(this, android.R.layout.simple_list_item_1);
        QuantityAdapter quantityAdapter = new QuantityAdapter(this, lista);
        adapter.add(map);
        quantitySpinner.setAdapter(quantityAdapter);
        quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<QuantityHelper> adapter = (ArrayAdapter<QuantityHelper>) quantitySpinner.getAdapter();
                helper = adapter.getItem(position);
                currentCondition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductsTask task = new ProductsTask(InventoryCountListActivity.this, bin.getObId(), currentCategory.getObId(),null);
                task.setOnPostExecuteListener(new OnPostExecuteListener<List<Product>>() {
                    @Override
                    public void onPostExecute(List<Product> response, List<String> errorMessages) {
                        try{
                            if (response != null || !response.isEmpty()) {
                                for (Product p : response) {
                                    List<Product> products = Product.find(Product.class, "ob_id=?", p.getObId());

                                    if(products.isEmpty()){
                                        p.save();
                                    }
                                    else {
                                        p = products.get(0);
                                    }
                                    DeliveryListItem deliveryListItem = new DeliveryListItem();
                                    deliveryListItem.setProduct(p);
                                    deliveryListItem.setListId(app.getDeliveryList());
                                    deliveryListItem.setReceivedAmount(0);
                                    deliveryListItem.setOnHandAmount(0);
                                    deliveryListItem.setBin(bin.getObId());
                                    deliveryListItem.setBinId(bin.getIdentifier());
                                    deliveryListItem.save();
                                    app.getDeliveryList().getProducts().add(deliveryListItem);
                                    app.getDeliveryList().save();
                                }
                                navigateUpTo(getParentActivityIntent());
                                finish();
                            }
                        }
                        catch (Exception e){
                            Toast.makeText(InventoryCountListActivity.this,"Se agregaron "+0+" productos a la lista",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });
                task.execute();
            }
        });
    }



    private void setCategorySpinner(){
        categorySpinner = (Spinner) findViewById(R.id.spinner7);
        final CategoryAdapter categoryAdapter = new CategoryAdapter(this, new ArrayList<ProductCategory>());
        if(app.getCategoryList() == null){
            asyncCategoriesCall(categoryAdapter);
            setCategoryAdapter(categoryAdapter);
        }
        else{
            categoryAdapter.addAll(app.getCategoryList());
            setCategoryAdapter(categoryAdapter);
        }
    }

    private void setCategoryAdapter(final CategoryAdapter categoryAdapter){
        categorySpinner.setAdapter(categoryAdapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                ArrayAdapter<ProductCategory> adapter = (ArrayAdapter<ProductCategory>) categorySpinner.getAdapter();
                currentCategory = adapter.getItem(position);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void asyncCategoriesCall(final ArrayAdapter<ProductCategory> categoryAdapter){
        LoadCategoriesTask categoriesTask = new LoadCategoriesTask(InventoryCountListActivity.this);
        categoriesTask.setOnPostExecuteListener(new OnPostExecuteListener<List<ProductCategory>>() {

            public void onPostExecute(List<ProductCategory> response,
                                      List<String> errorMessages) {
                ProductCategory p = new ProductCategory();
                p.setName("");
                app.setCategoryList(response);
                categoryAdapter.addAll(response);

            }
        });
        categoriesTask.execute();
    }



    class ProductsTask extends TaskWithCallback<Void, Void, List<Product>> {

        private String storageBin, productCategory,productIdentifier;
        private int quantity;

        public ProductsTask(Context context) {
            super(context);
            // TODO Auto-generated constructor stub
        }

        public ProductsTask(Context context, String storageBin, String productCategory, String productIdentifier){
            super(context);
            this.storageBin=storageBin;
            this.productCategory=productCategory;
            this.productIdentifier=productIdentifier;
        }

        @Override
        protected List<Product> doInBackground(Void... params) {
            // TODO Auto-generated method stub
            List<Product> products;
            query.append("SELECT p FROM MaterialMgmtStorageDetail e " +
                         "INNER JOIN e.storageBin s " +
                         "INNER JOIN e.product p " +
                         "INNER JOIN p.productCategory c ");
            query.append("WHERE s.id='"+storageBin+"' ");
            if(productCategory!=null)
                query.append("AND c.id='"+productCategory+"' ");
            if(productIdentifier!=null)
                query.append("AND p.searchKey LIKE '"+productIdentifier+"%' ");
            switch (currentCondition){
                case 1:
                    query.append("AND e.quantityOnHand = "+helper.getValue());
                    break;
                case 2:
                case 3:
                    query.append("AND e.quantityOnHand "+helper.getValue());
                    break;
                case 4:
                    query.append("AND e.quantityOnHand <> 0");
                    break;
            }
            StringBuilder builder = new StringBuilder();
            builder.append("as e where e in(");
            builder.append(query.toString());
            builder.append(")");
            try {
                queryParams.put("_where",builder.toString());
                queryParams.put("_selectedProperties","id,name,description,uOM,image,uPCEAN,searchKey,storageBin");
                Response<List<Product>> response = app.getObService().getProductList(queryParams).execute();
                products = response.body();
                Log.d("FIRST RESPONSE", response.raw().request().urlString());
                if(products == null || products.isEmpty()){
                    builder = new StringBuilder();
                    builder.append("e in(");
                    builder.append(query.toString());
                    builder.append(")");
                    queryParams.put("_where",builder.toString());
                    response = app.getObService().getProductList(queryParams).execute();
                    products = response.body();
                    Log.d("SECOND RESPONSE", response.raw().request().urlString());

                }
                if(products == null || products.isEmpty()){
                    String errorMsg = "Se agregaron 0 productos";
                    throw new Exception(errorMsg);
                } else {
//                    errorMessages.add("Se agregaron "+products.size()+" productos");
                    return products;
                }
            }
            catch (Exception e){
//                errorMessages.add(e.getMessage());
                Log.e("CONTEO", e.getMessage(), e);
                return null;
            }
        }

        @Override
        public void setOnPostExecuteListener(OnPostExecuteListener<List<Product>> onPostExecuteListener) {
            super.setOnPostExecuteListener(onPostExecuteListener);
        }
    }
}

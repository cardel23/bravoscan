package com.peoplewalking.warehouse.openbravo.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.QuantityHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carlos on 03-21-16.
 */
public class QuantityAdapter extends ArrayAdapter<QuantityHelper> {

    private NovoventApp app;
    private TextView title;
    private List<QuantityHelper> params;

    public QuantityAdapter(Context context, List<QuantityHelper> params) {
        super(context, android.R.layout.simple_spinner_dropdown_item,params);
        this.params = params;
//        this.params.put(0,"Seleccionar");
//        this.params.putAll(params);
        app = (NovoventApp) context.getApplicationContext();
        // TODO Auto-generated constructor stub
//            if(productCategories.isEmpty() || !productCategories.get(0).getName().equals("") && productCategories.get(0).getName() != app.getString(R.string.select_item) ){
//                ProductCategory blankOption = new ProductCategory();
//                blankOption.setName(app.getString(R.string.select_item));
//                productCategories.add(0, blankOption);
//            }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(app).inflate(android.R.layout.simple_list_item_1, parent, false);
//        layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        title = (TextView) convertView.findViewById(android.R.id.text1);
        title.setText(params.get(position).getValue());
        title.setTextColor(Color.BLACK);
        return convertView;
    }

//        public int getPosition(String item){
//            int position = 0;
//            try {
//                for (ProductCategory s : productCategories){
//                    if(s.getObId() == null)
//                        continue;
//                    else
//                    if(s.getIdentifier().equals(item)){
//                        position = getPosition(s);
//                        break;
//                    }
//                }
//                return position;
//            } catch (Exception e) {
//                // TODO: handle exception
//                e.printStackTrace();
//                return position;
//            }
//        }
}

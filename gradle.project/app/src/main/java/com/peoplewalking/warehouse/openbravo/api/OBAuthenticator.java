package com.peoplewalking.warehouse.openbravo.api;

import java.io.IOException;
import java.net.Proxy;

import com.squareup.okhttp.Authenticator;
import com.squareup.okhttp.Credentials;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class OBAuthenticator implements Authenticator{
	private String username;
	private String password;
	
	public OBAuthenticator(String username, String password){
		this.username = username;
		this.password = password;
	}

	public Request authenticate(Proxy proxy, Response response) throws IOException {
		String credential = Credentials.basic(this.username, this.password);
		return response.request().newBuilder().header("Authorization", credential).build();
	}

	public Request authenticateProxy(Proxy proxy, Response response)
			throws IOException {
		return null;
	}

}

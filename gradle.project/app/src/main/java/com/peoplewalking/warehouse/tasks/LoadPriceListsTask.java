package com.peoplewalking.warehouse.tasks;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.PriceList;

import android.content.Context;

/**
 * Tarea asíncrona para cargar tarifas de productos
 * @author Carlos Delgadillo
 * fecha: 30/07/2015
 *
 */
public class LoadPriceListsTask extends TaskWithCallback<Void, Void, List<PriceList>> {

	
	public LoadPriceListsTask(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LoadPriceListsTask(Context context, String loadingMessage) {
		super(context, loadingMessage);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}
	
	@Override
	protected List<PriceList> doInBackground(Void... params) {
		// TODO Auto-generated method stub
		return executeSynchronous(params);
	}
	
	@Override
	public List<PriceList> executeSynchronous(Void... params) {
		HashMap<String, String> queryParams = new HashMap<String, String>();
		StringBuilder query = new StringBuilder("organization='"+app.getUser().getDefaultOrganizationId()+"'");
//		queryParams.put("_where", query.toString());
		queryParams.put("_selectedProperties","id,name,currency");
		queryParams.put("_sortBy", "name");
		List<PriceList> priceList = null;
		try {
			priceList = OBServiceClient.getOBService().getPriceLists(queryParams).execute().body();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return priceList;
	}
	
	@Override
	protected void onPostExecute(List<PriceList> response) {
		// TODO Auto-generated method stub
		super.onPostExecute(response);
	}

}

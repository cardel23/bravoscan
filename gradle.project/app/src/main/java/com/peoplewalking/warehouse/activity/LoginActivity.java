package com.peoplewalking.warehouse.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage.MessageClass;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.tasks.LoginTask;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.viewhelpers.LoadingViewHelper;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements OnPostExecuteListener<Boolean>, OnClickListener{
	public static final String TAG = "LoginActivity";
	
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private LoginTask mAuthTask = null;
	
	// UI references.
	private EditText mUsernameView;
	private Button mSignInButton;
	private EditText mPasswordView;
	private View mLoginFormView;
	private NovoventApp app;
	private LoadingViewHelper loadingViewHelper;
	private CheckBox checkUser, checkPass;
	
	Thread timeoutThread;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initLoginActivity();
	}
	
	
	private void initLoginActivity(){
	    setContentView(R.layout.activity_login);
		if(app == null){
			app = (NovoventApp) getApplicationContext();
		}
		
		// Set up the login form.
		mLoginFormView= (LinearLayout) findViewById(R.id.email_login_form);
		mUsernameView = (EditText) findViewById(R.id.username);

		mPasswordView = (EditText) findViewById(R.id.password);
		
		mUsernameView.setText(app.getSavedUser());
		mPasswordView.setText(app.getSavedPass());
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mSignInButton = (Button) findViewById(R.id.sign_in_button);
		mSignInButton.setOnClickListener(this);
		checkUser = (CheckBox) findViewById(R.id.checkBox1);
		checkPass = (CheckBox) findViewById(R.id.checkBox2);
		
		
		checkPass.setEnabled(false);
		checkUser.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				checkPass.setEnabled(isChecked);
				if(!isChecked){
					checkPass.setChecked(isChecked);
				}
			}
		});
		checkPass.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			}
		});
		if(app.getSavedUser() != null)
			checkUser.setChecked(true);
		if(app.getSavedPass() != null)
			checkPass.setChecked(true);
		loadingViewHelper = new LoadingViewHelper(this, (ViewGroup) mLoginFormView);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    getMenuInflater().inflate(R.menu.settings, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    if(item.getItemId() == R.id.action_settings){
		Intent settingIntent = new Intent(app, SettingsActivity.class);
		startActivity(settingIntent);
		return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() {
//	    invalidateOptionsMenu();
	    app.setLocaleConfig();
	    initLoginActivity();
	    super.onResume();	    
	}
	
	/**
	 * Attempts to sign in the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mUsernameView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		
		String username = mUsernameView.getText().toString();
		String password = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.

		if (TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(username)) {
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		} else if (!isUsernameValid(username)) {
			mUsernameView.setError(getString(R.string.error_invalid_username));
			focusView = mUsernameView;
			cancel = true;
		}

		if (cancel) {
			focusView.requestFocus();
		} else {			
			 mAuthTask = new LoginTask(this, username, password);
			 mAuthTask.setOnPostExecuteListener(this);
			if(checkUser.isChecked()){
				app.editSharedPreference("savedUser", username);
				app.editSharedPreference("savedPass", null);
			}
			else{
				app.editSharedPreference("savedUser", null);
				app.editSharedPreference("savedPass", null);
			}
			if(checkPass.isChecked())
				app.editSharedPreference("savedPass", password);
			if(app.isServerSet()){				
				mAuthTask.execute((Void) null);
			}
			else{
				// Show a progress spinner, and kick off a background task to
				// perform the user login attempt.
				LayoutInflater layoutInflater = LayoutInflater.from(LoginActivity.this);
				View dialogEdit = layoutInflater.inflate(R.layout.dialog_set_url, null);
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
				alertDialogBuilder.setView(dialogEdit);
				final EditText URL = (EditText) dialogEdit.findViewById(R.id.editText1);
				// setup a dialog window

				alertDialogBuilder.setMessage(R.string.set_server_url)
						.setCancelable(false)
						.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								String url = URL.getText().toString();
								if(url.equals("")){
									dialog.cancel();
									Toast.makeText(app, "wawawawa", Toast.LENGTH_SHORT).show();
								}
								else{
									app.setServer(url);
									mAuthTask.execute((Void) null);	
								}
							}
						})
						.setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								mAuthTask = null;
								dialog.cancel();
							}
						});
				
				alertDialogBuilder.create().show();
			}
		}
	}
	
	

	private boolean isUsernameValid(String username) {
		return username != null && !username.isEmpty();
	}

	private boolean isPasswordValid(String password) {
		return password.length() > 0;
	}

	public void onPostExecute(Boolean success, List<String> errorMessages) {
		this.mAuthTask = null;
		loadingViewHelper.showProgress(false);
//		if(timeoutThread.isAlive())
//			timeoutThread.interrupt();
		if(errorMessages != null){
			for(String errorMsg : errorMessages){
			    if(errorMsg.contains(getString(R.string.error_bad_credentials))){
				mUsernameView.setError(errorMsg);
			    }else{
				Toast.makeText(this, errorMsg, Toast.LENGTH_LONG).show();
			    }
			}
		}
		
		if (success) {
			Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);				
			startActivity(mainIntent);
			finish();
		}
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}


	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == R.id.sign_in_button){
			mAuthTask = null;
			attemptLogin();
		}
	}
	
}

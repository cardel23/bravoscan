package com.peoplewalking.warehouse.tasks;

import android.content.Context;
import android.util.Log;

import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.Organization;

import java.util.List;

/**
 * Created by Carlos Delgadillo on 10-20-15.
 */
public class OrganizationsTask extends TaskWithCallback<Void, Void, List<Organization>> {


    public OrganizationsTask(Context context) {
        super(context);
    }

    @Override
    protected List<Organization> executeSynchronous(Void... params) {
        List<Organization> organizationList;
        try {
            OpenbravoService service = app.getObService();

            StringBuilder sb = new StringBuilder();
//            query.append();
            query.append("SELECT DISTINCT o from ADUser e ")
                    .append("LEFT JOIN e.aDUserRolesList rl ")
                    .append("LEFT JOIN rl.role r ")
                    .append("LEFT JOIN r.aDRoleOrganizationList ol ")
                    .append("LEFT JOIN ol.organization o ")
                    .append("LEFT JOIN o.organizationType t ")
                    .append("WHERE e.id='"+app.getUser().getObId()+"' ")
//                    .append("AND t.transactionsAllowed=true ")
                    .append("AND t.transactionsAllowed=true AND o.active=true");
            queryParams.put("_where", query.toString());
            queryParams.put("_selectedProperties", "id,name");
            sb.append("e in (")
                    .append(query.toString())
                    .append(")");
            organizationList = service.getOrganizationList(sb.toString()).execute().body();
            if(organizationList == null|| organizationList.isEmpty()){
                sb = new StringBuilder();
                sb.append("as o where o in (")
                        .append(query.toString())
                        .append(")");
                organizationList = service.getOrganizationList(sb.toString()).execute().body();
                if(organizationList == null|| organizationList.isEmpty()){
                    this.errorMessages.add("No org found");
                    Log.d("TASK", "No org found");
                }
            }
            return organizationList;
        }
        catch (Exception e){
            return null;
        }
    }

    @Override
    protected List<Organization> doInBackground(Void... params) {
       return executeSynchronous(params);
    }
}

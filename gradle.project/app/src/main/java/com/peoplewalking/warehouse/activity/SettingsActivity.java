package com.peoplewalking.warehouse.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.EditTextPreference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.interfaces.OnClickConfirmDialogListener;
import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.api.OBServiceClient;
import com.peoplewalking.warehouse.openbravo.model.DocumentType;
import com.peoplewalking.warehouse.openbravo.model.Organization;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.Warehouse;
import com.peoplewalking.warehouse.tasks.SyncTask;
import com.peoplewalking.warehouse.util.CustomAlertDialog;
import com.peoplewalking.warehouse.util.NovoventApp;
import com.peoplewalking.warehouse.util.preferences.NumberPickerPreference;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity implements OnPreferenceChangeListener {
	/**
	 * Determines whether to always show the simplified settings UI, where
	 * settings are presented in a single list. When false, settings are shown
	 * as a master/detail two-pane view on tablets. When true, a single pane is
	 * shown on tablets.
	 */
	private static final boolean ALWAYS_SIMPLE_PREFS = false;
	
	private ListPreference appLangPreference, orgPreference, warehousePreference, inboundPreference, outboundPreference;
	private EditTextPreference namePreference, clientPreference, serverPreference;
	private NumberPickerPreference portPreference;
	private static NovoventApp app;
	private static final String namePref = "namePref",
								orgPref = "orgPref",
								warehousePref = "warehousePref",
								langPref = "langPref",
								clientPref = "clientPref",
								serverPref = "serverPref",
								inboundPref = "inboundPref",
								outboundPref = "outboundPref",
								portPref = "portPref",
								syncPref = "syncPref";
	private Preference syncPreference;
	private Toast toast;
	private ProgressDialog progressDialog;
	AlertDialog.Builder builder;
	AlertDialog dialog;
	
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		app = (NovoventApp) getApplicationContext();
		setupSimplePreferencesScreen(app.isLogged());
		this.setTitle(R.string.title_activity_settings);
		setInitialPreferences(app.isLogged());
	}
	
	private void setInitialPreferences(boolean login){
	    
		namePreference = (EditTextPreference) findPreference(namePref);
		orgPreference = (ListPreference) findPreference(orgPref);
		clientPreference = (EditTextPreference) findPreference(clientPref);
		warehousePreference = (ListPreference) findPreference(warehousePref);
		appLangPreference = (ListPreference) findPreference(langPref);
		serverPreference = (EditTextPreference) findPreference(serverPref);
//		inboundPreference = (ListPreference) findPreference(inboundPref);
//		outboundPreference = (ListPreference) findPreference(outboundPref);
		portPreference = (NumberPickerPreference) findPreference(portPref);
		
		
		try {
			if(login){
				namePreference.setSummary(app.getUser().getName());
				namePreference.setEnabled(false);
				orgPreference.setSummary(app.getUser().getDefaultOrganizationIdentifier());
				orgPreference.setEnabled(false);
				clientPreference.setEnabled(false);
			if(app.getOrganization() == null){
				SettingsTask task = new SettingsTask();
				task.execute();
			} else {
				clientPreference.setSummary(app.getOrganization().getClientIdentifier());
				setUserPreferences(login);
			}
			if(progressDialog != null)
				progressDialog.dismiss();
		    }
		    else{
		    	setUserPreferences(login);
		    }
		}
//		catch (RetrofitError e){
//			if(progressDialog != null)
//				progressDialog.dismiss();
//			setUserPreferences(login);
//		}
		catch (Exception e) {
			orgPreference.setSummary(null);
			setUserPreferences(login);
			if(progressDialog != null)
				progressDialog.dismiss();
		}
		
	}
	
	private void setUserPreferences(boolean login){
	    if(login){
			warehousePreference.setSummary(app.getUser().getDefaultWarehouseIdentifier());
			warehousePreference.setEnabled(false);
	    }
//	    setDocumentPreferences(login);
	    if(appLangPreference.getValue() != app.getLocale().getLanguage()){
	    	appLangPreference.setValue(app.getLocale().getLanguage());
	    }
		
	    appLangPreference.setSummary(appLangPreference.getEntries()[appLangPreference.findIndexOfValue(app.getLocale().getLanguage())]);
	    appLangPreference.setOnPreferenceChangeListener(this);
	    serverPreference.setOnPreferenceChangeListener(this);
	    serverPreference.setDefaultValue(app.getServer());
	    serverPreference.setSummary(app.getServer());
	    serverPreference.setText(app.getServer());
	    portPreference.setTitle(getString(R.string.port));
//	    portPreference.setmNumberPicker((NumberPicker) findViewById(R.id.numberPicker2));
	    portPreference.setValue(app.getApplicationPort());
	    portPreference.setSummary(String.valueOf(app.getApplicationPort()));
	    portPreference.setMinValue(0);
	    portPreference.setMaxValue(65535);
	    portPreference.setPickerLayout(R.layout.numberpicker_dialog);
	    portPreference.setOnPreferenceChangeListener(this);
	    try {
	    	syncPreference = (Preference) findPreference(syncPref);
		    syncPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				
				public boolean onPreferenceClick(Preference preference) {
					try {
						SyncTask syncTask = new SyncTask(SettingsActivity.this);
						syncTask.setOnPostExecuteListener(new OnPostExecuteListener<Boolean>() {
							
							public void onPostExecute(Boolean response, List<String> errorMessages) {
								// TODO Auto-generated method stub
								if(response)
									Toast.makeText(app, app.getString(R.string.operation_success), Toast.LENGTH_LONG).show();
								else
									Toast.makeText(app, errorMessages.get(0), Toast.LENGTH_SHORT).show();
							}
						});
						syncTask.execute();
						return true;
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(app, app.getString(R.string.operation_failed), Toast.LENGTH_SHORT).show();
						return false;
					}			
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
			//nada
		}
	    
	    if(toast != null){
	    	toast.cancel();
	    }
	    
	    

	}
	
	/**
	 * @deprecated No se usa en este momento, se debe definir la forma de acceso a la aplicación y los permisos que tendrá el usuario
	 * @param warehouseList
	 * @param wEntries
	 * @param wEntriesValues
	 */
	private void setWarehousePreference(List<Warehouse> warehouseList, List<String> wEntries, List<String> wEntriesValues){
		try {
			if(app.isLogged()){
				for(Warehouse w : app.getWarehouseList()){
					wEntries.add(w.getName());
					wEntriesValues.add(w.getObId());
				}
				warehousePreference.setEntries(wEntries.toArray(new CharSequence[wEntries.size()]));
				warehousePreference.setEntryValues(wEntriesValues.toArray(new CharSequence[wEntriesValues.size()]));
				if(warehousePreference.getValue() != app.getWarehouse())
					warehousePreference.setValue(app.getWarehouse());
				warehousePreference.setSummary(warehousePreference.getEntries()[warehousePreference.findIndexOfValue(app.getWarehouse())]);
//				setDocumentPreferences(app.isLogged());
			}else{
			    warehousePreference.setEnabled(app.isLogged());
			}
		} catch (Exception e) {
			// TODO: handle exception
			finish();
		}
		
	}
	
	/**
	 * Asigna y carga las preferencias de tipos de documento por defecto o para la creacíon de albaranes de entrada y salida
	 * @param login
	 * @author Carlos Delgadillo
	 * @fecha 29/05/2015
	 */
	
	private void setDocumentPreferences(boolean login){
		try {
			if (login) {
				List<String> dTEntries = new ArrayList<String>();
				List<String> dTEntriesValues = new ArrayList<String>();
				for(DocumentType docType : app.getInboundDocList()){
					dTEntries.add(docType.toString());
					dTEntriesValues.add(docType.getObId());
				}
				
				String defaultPreference =  app.getDefaultInboundPreference();
				
				inboundPreference.setEntries(dTEntries.toArray(new CharSequence[dTEntries.size()]));
				inboundPreference.setEntryValues(dTEntriesValues.toArray(new CharSequence[dTEntriesValues.size()]));
				if(inboundPreference.getValue() != defaultPreference){
					inboundPreference.setValue(defaultPreference);
				}
				inboundPreference.setSummary(inboundPreference.getEntries()[inboundPreference.findIndexOfValue(defaultPreference)]);
				
				dTEntries = new ArrayList<String>();
				dTEntriesValues = new ArrayList<String>();
				for(DocumentType docType : app.getOutboundDocList()){
					dTEntries.add(docType.toString());
					dTEntriesValues.add(docType.getObId());
				}
				
				defaultPreference =  app.getDefaultOutboundPreference();
				
				outboundPreference.setEntries(dTEntries.toArray(new CharSequence[dTEntries.size()]));
				outboundPreference.setEntryValues(dTEntriesValues.toArray(new CharSequence[dTEntriesValues.size()]));
				if(outboundPreference.getValue() != defaultPreference){
					outboundPreference.setValue(defaultPreference);
				}
				outboundPreference.setSummary(outboundPreference.getEntries()[outboundPreference.findIndexOfValue(defaultPreference)]);
				
				inboundPreference.setOnPreferenceChangeListener(this);
				outboundPreference.setOnPreferenceChangeListener(this);
			}
			inboundPreference.setEnabled(login);
			outboundPreference.setEnabled(login);

		} catch (Exception e) {
			inboundPreference.setEnabled(false);
			outboundPreference.setEnabled(false);
		}
		
	}
	
	@Override
	public boolean onNavigateUp() {
		// TODO Auto-generated method stub
		return super.onNavigateUp();
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
          // Respond to the action bar's Up/Home button
          case android.R.id.home:
          NavUtils.navigateUpFromSameTask(this);
         return true;
       }
       return super.onOptionsItemSelected(item);
    }
	@Override
	public void onBackPressed() {
		super.onNavigateUp();
//		moveTaskToBack(true);
//		navigateUpTo(getParentActivityIntent());
//		this.finish();
	}

	/**
	 * Shows the simplified settings UI if the device configuration if the
	 * device configuration dictates that a simplified, single-pane UI should be
	 * shown.
	 */
	private void setupSimplePreferencesScreen(boolean login) {
		if (!isSimplePreferences(this)) {
			return;
		}
		
		addPreferencesFromResource(R.xml.pref_container);
		// In the simplified UI, fragments are not used at all and we instead
		// use the older PreferenceActivity APIs.
		PreferenceCategory fakeHeader = new PreferenceCategory(this);
		if(login){
			fakeHeader.setTitle(app.getString(R.string.pref_header_general));
			getPreferenceScreen().addPreference(fakeHeader);
			// Add 'general' preferences.
			addPreferencesFromResource(R.xml.pref_general);
		}
		fakeHeader = new PreferenceCategory(this);
		fakeHeader.setTitle(app.getString(R.string.pref_header_options));
		getPreferenceScreen().addPreference(fakeHeader);
		addPreferencesFromResource(R.xml.pref_options);
//		addPreferencesFromResource(R.xml.pref_document_types);
		// Add 'notifications' preferences, and a corresponding header.
//		fakeHeader = new PreferenceCategory(this);
//		fakeHeader.setTitle(R.string.pref_header_notifications);
//		getPreferenceScreen().addPreference(fakeHeader);
//		addPreferencesFromResource(R.xml.pref_notification);
//
//		// Add 'data and sync' preferences, and a corresponding header.
		if(login){
			fakeHeader = new PreferenceCategory(this);
			fakeHeader.setTitle(R.string.pref_header_data_sync);
			getPreferenceScreen().addPreference(fakeHeader);
			addPreferencesFromResource(R.xml.pref_data_sync);
		}
		// Bind the summaries of EditText/List/Dialog/Ringtone preferences to
		// their values. When their values change, their summaries are updated
		// to reflect the new value, per the Android Design guidelines.
		bindPreferenceSummaryToValue(findPreference(namePref));
//		bindPreferenceSummaryToValue(findPreference("example_user"));
//		bindPreferenceSummaryToValue(findPreference("example_pass"));
//		bindPreferenceSummaryToValue(findPreference("example_list"));
//		bindPreferenceSummaryToValue(findPreference("example_list_2"));
		bindPreferenceSummaryToValue(findPreference(langPref));
//		bindPreferenceSummaryToValue(findPreference(serverPref));
//		bindPreferenceSummaryToValue(findPreference(portPref));
//		bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//		bindPreferenceSummaryToValue(findPreference("sync_frequency"));
	}

	/** {@inheritDoc} */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this) && !isSimplePreferences(this);
	}

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/**
	 * Determines whether the simplified settings UI should be shown. This is
	 * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
	 * doesn't have newer APIs like {@link PreferenceFragment}, or the device
	 * doesn't have an extra-large screen. In these cases, a single-pane
	 * "simplified" settings UI should be shown.
	 */
	private static boolean isSimplePreferences(Context context) {
		return ALWAYS_SIMPLE_PREFS
				|| Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
				|| !isXLargeTablet(context);
	}

	/** {@inheritDoc} */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onBuildHeaders(List<Header> target) {
		if (!isSimplePreferences(this)) {
			loadHeadersFromResource(R.xml.pref_headers, target);
		}
	}
	
	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		
		Intent intent;
		public boolean onPreferenceChange(final Preference preference, Object value) {
			final String stringValue = value.toString();

			if (preference instanceof ListPreference) {
				// For list preferences, look up the correct display value in
				// the preference's 'entries' list.
				ListPreference listPreference = (ListPreference) preference;
				int index = listPreference.findIndexOfValue(stringValue);

				// Set the summary to reflect the new value.
				preference
						.setSummary(index >= 0 ? listPreference.getEntries()[index]
								: null);

			} else if (preference instanceof RingtonePreference) {
				// For ringtone preferences, look up the correct display value
				// using RingtoneManager.
				if (TextUtils.isEmpty(stringValue)) {
					// Empty values correspond to 'silent' (no ringtone).
					preference.setSummary(R.string.pref_ringtone_silent);

				} else {
					Ringtone ringtone = RingtoneManager.getRingtone(
							preference.getContext(), Uri.parse(stringValue));

					if (ringtone == null) {
						// Clear the summary if there was a lookup error.
						preference.setSummary(null);
					} else {
						// Set the summary to reflect the new ringtone display
						// name.
						String name = ringtone
								.getTitle(preference.getContext());
						preference.setSummary(name);
					}
				}

			} else if(preference instanceof NumberPickerPreference){
				
				
			}
			else if(preference instanceof EditTextPreference){
				
			}
			else {
				// For all other preferences, set the summary to the value's
				// simple string representation.
				preference.setSummary(stringValue);
			}
			return true;
		}
	};

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 *
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	
	private void logout(Intent intent){
		startActivity(intent);
	}
	private void bindPreferenceSummaryToValue(Preference preference) {
	    if(preference != null){
		// Set the listener to watch for value changes.
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(
				preference,
				PreferenceManager.getDefaultSharedPreferences(
						preference.getContext()).getString(preference.getKey(),
						""));
	    }
	}

	/**
	 * This fragment shows general preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public static class GeneralPreferenceFragment extends PreferenceFragment {
//		@Override
//		public void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);
//			addPreferencesFromResource(R.xml.pref_general);
//
//			// Bind the summaries of EditText/List/Dialog/Ringtone preferences
//			// to their values. When their values change, their summaries are
//			// updated to reflect the new value, per the Android Design
//			// guidelines.
//			bindPreferenceSummaryToValue(findPreference("example_text"));
////			bindPreferenceSummaryToValue(findPreference("example_user"));
////			bindPreferenceSummaryToValue(findPreference("example_pass"));
////			bindPreferenceSummaryToValue(findPreference("example_list"));
////			bindPreferenceSummaryToValue(findPreference("example_list_2"));
//			bindPreferenceSummaryToValue(findPreference("example_list_3"));
//		}
//	}

	public boolean onPreferenceChange(final Preference preference, Object newValue) {
		final String stringValue = newValue.toString();

		if (preference instanceof ListPreference) {
			// For list preferences, look up the correct display value in
			// the preference's 'entries' list.
			ListPreference listPreference = (ListPreference) preference;
			int index = listPreference.findIndexOfValue(stringValue);

			// Set the summary to reflect the new value.
			preference.setSummary(index >= 0 ? listPreference.getEntries()[index]
							: null);
			if(listPreference.getKey().equals(langPref)){
				Resources res = app.getResources();
				Configuration config = res.getConfiguration(); 
		        app.setLocale(new Locale(stringValue));
		        config.locale = app.getLocale();
		        res.updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		        Intent i = new Intent(getBaseContext(), SettingsActivity.class);
		        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		        super.onConfigurationChanged(config);
		        this.recreate();
			}
			else
			if(listPreference.getKey().equals(warehousePref)){
//				app.editSharedPreference(preferenceName, val);
				return false;
			}
			else
			if(listPreference.getKey().equals(inboundPref)){
				app.setDefaultInboundPreference(listPreference.getEntryValues()[index].toString());
//				preference.setSummary(stringValue);
				return true;
			}
			else
			if(listPreference.getKey().equals(outboundPref)){
				app.setDefaultOutboundPreference(listPreference.getEntryValues()[index].toString());
//				preference.setSummary(stringValue);
				return true;
			}
			return true;
		} else if(preference instanceof EditTextPreference){
			if(preference.getKey().equals(serverPref)){
				if(app.isLogged()){
					dialog = new AlertDialog.Builder(this)
					.setTitle(getString(R.string.action_settings))
				    .setMessage(getString(R.string.change_server_port_confirm))
				    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
				        	
				        	preference.setSummary(stringValue);
						    app.setServer(stringValue);
						    
						    app.logout();
				        	Intent intent = new Intent(app, LoginActivity.class);
				        	intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
				        	intent.putExtra("EXIT", true);
				        	startActivity(intent);
						    
				        	dialog.dismiss();
				        }
				     })
				     .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // do nothing
				        	dialog.dismiss();
					        }
					     })
					 .setIcon(android.R.drawable.ic_dialog_alert)
					 .show();
					}
					else{
						preference.setSummary(stringValue);
					    app.setServer(stringValue);
					}
				}
		    
		    return true;
		} else if(preference instanceof NumberPickerPreference){
			if(preference.getKey().equals("portPref")){
				if(app.isLogged()){
					builder = new AlertDialog.Builder(SettingsActivity.this)
					.setTitle(getString(R.string.action_settings))
				    .setMessage(getString(R.string.change_server_port_confirm))
				    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
//				        	((NumberPickerPreference) preference).onConfirmDialogClosed(true);
				        	preference.setSummary(stringValue);
							app.setApplicationPort(Integer.valueOf(stringValue));
						    app.logout();
				        	Intent intent = new Intent(app, LoginActivity.class);
				        	intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
				        	intent.putExtra("EXIT", true);
				        	startActivity(intent);
				        	dialog.dismiss();				        	
				        }
				    })
				    .setNegativeButton(android.R.string.no, new  DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // do nothing
				        	dialog.dismiss();
				        }
				    })
				    .setIcon(android.R.drawable.ic_dialog_alert);
					dialog = builder.create();
					dialog.show();
				}
				else{
		        	((NumberPickerPreference) preference).setValue(Integer.valueOf(stringValue));
		        	preference.setDefaultValue(Integer.valueOf(stringValue));
					preference.setSummary(stringValue);
					app.setApplicationPort(Integer.valueOf(stringValue));
				}
				
				return true;
			}
			
		}
		return false;
	}
	
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public static class OptionsPreferenceFragment extends PreferenceFragment {
//		@Override
//		public void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);
//			addPreferencesFromResource(R.xml.pref_options);
//
//			// Bind the summaries of EditText/List/Dialog/Ringtone preferences
//			// to their values. When their values change, their summaries are
//			// updated to reflect the new value, per the Android Design
//			// guidelines.
//			bindPreferenceSummaryToValue(findPreference("example_list"));
//			bindPreferenceSummaryToValue(findPreference("example_list_2"));
//			bindPreferenceSummaryToValue(findPreference(serverPref));
//		}
//		
//		
//	}
	/**
	 * This fragment shows notification preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public static class NotificationPreferenceFragment extends
//			PreferenceFragment {
//		@Override
//		public void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);
//			addPreferencesFromResource(R.xml.pref_notification);
//
//			// Bind the summaries of EditText/List/Dialog/Ringtone preferences
//			// to their values. When their values change, their summaries are
//			// updated to reflect the new value, per the Android Design
//			// guidelines.
//			bindPreferenceSummaryToValue(findPreference("notifications_new_message_ringtone"));
//		}
//	}

	/**
	 * This fragment shows data and sync preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
//	public static class DataSyncPreferenceFragment extends PreferenceFragment {
//		@Override
//		public void onCreate(Bundle savedInstanceState) {
//			super.onCreate(savedInstanceState);
//			addPreferencesFromResource(R.xml.pref_data_sync);
//
//			// Bind the summaries of EditText/List/Dialog/Ringtone preferences
//			// to their values. When their values change, their summaries are
//			// updated to reflect the new value, per the Android Design
//			// guidelines.
//			bindPreferenceSummaryToValue(findPreference("sync_frequency"));
//		}
//	}
	
	private class SettingsTask extends AsyncTask<Void, Void, Void>{
		
		ProgressDialog progress;
		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(SettingsActivity.this, "", app.getString(R.string.loading_prefs));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
//			toast.makeText(app, app.getString(R.string.loading), 100000).show();
			try {
				app.setOrganization(OBServiceClient.getOBService().getOrganization(app.getUser().getDefaultOrganizationId()).execute().body());
				runOnUiThread(new Runnable() {

					public void run() {
						clientPreference.setSummary(app.getOrganization().getClientIdentifier());
						setUserPreferences(app.isLogged());
					}
				});
			} catch (IOException e) {
				e.printStackTrace();
			}

			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			progress.dismiss();
		}
		
	}
}

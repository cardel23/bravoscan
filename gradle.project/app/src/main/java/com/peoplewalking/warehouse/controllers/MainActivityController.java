package com.peoplewalking.warehouse.controllers;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.view.View;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.activity.ListTabsActivity;
import com.peoplewalking.warehouse.activity.LoginActivity;
import com.peoplewalking.warehouse.activity.NewScanListActivity;
import com.peoplewalking.warehouse.activity.SettingsActivity;
import com.peoplewalking.warehouse.util.NovoventApp;

/**
 * Created by pwk04 on 07-28-16.
 */
public class MainActivityController extends BaseController {

    public MainActivityController(){}

    @SuppressLint("ValidFragment")
    public MainActivityController(NovoventApp app){
        this.app = app;
    }


    public void selectActivity(View button){
        Intent targetActivity = null;
        switch (button.getId()) {
            case R.id.btnNewList:
                targetActivity = new Intent(app, NewScanListActivity.class);
                break;
            case R.id.btnShowLists:
                targetActivity = new Intent(app, ListTabsActivity.class);

                break;
            case R.id.btnGenReceipt:
                targetActivity = new Intent(app, SettingsActivity.class);
                break;
            case R.id.btnExit:
                targetActivity = new Intent(app, LoginActivity.class);
                getActivity().finish();
                app.logout();
                break;
            default:
                break;
        }
        if(targetActivity != null){
            startActivity(targetActivity);
        }
    }
}

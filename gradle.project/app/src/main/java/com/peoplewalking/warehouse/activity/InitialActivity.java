/**
 * 
 */
package com.peoplewalking.warehouse.activity;

import java.util.TimerTask;
import java.util.Timer;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * @author Darlon Espinoza
 *	
 */
public class InitialActivity extends Activity {
	Timer timer;
	TimerTask task;
	private static final int TIME = 1200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_initial);

		timer = new Timer();
		task = new TimerTask() {
			@Override
			public void run() {			
				NovoventApp app = (NovoventApp) getApplicationContext();				
				app.setLocaleConfig();
				if (app.getUser() == null) {
					Intent intentLogin = new Intent(InitialActivity.this,
							LoginActivity.class);
					
					startActivity(intentLogin);
					finish();
				} else {
					Intent intentPantallaPrincipal = new Intent(InitialActivity.this,
							MainActivity.class);
					startActivity(intentPantallaPrincipal);
					finish();
				}

			}
		};

		timer.schedule(task, TIME);	
	}
}

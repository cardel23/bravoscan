package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OBOrderLine extends OBModel {

	@Expose
	@SerializedName("lineNo")
	private String lineNo;
	@Expose
	@SerializedName("orderedQuantity")
	private Float orderedQuantity;	
	@Expose
	@SerializedName("salesOrder")
	private String salesOrderId;
	@Expose
	@SerializedName("salesOrder$_identifier")
	private String salesOrderIdentifier;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("product")
	private String product;
	@Expose
	@SerializedName("product$_identifier")
	private String productIdentifier;
	@Expose
	@SerializedName("warehouse")
	private String warehouse;
	@Expose
	@SerializedName("warehouse$_identifier")
	private String warehouseIdentifier;
	@Expose
	@SerializedName(value = "uOM")
	private String uOMId;
	@Expose
	@SerializedName(value = "uOM$_identifier")
	private String uOMIdentifier;

	public OBOrderLine(){
		
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String id) {
		this.obId = id;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getLineNo() {
		return lineNo;
	}

	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}
	
	public Float getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(Float orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public String getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(String salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	public String getSalesOrderIdentifier() {
		return salesOrderIdentifier;
	}

	public void setSalesOrderIdentifier(String salesOrderIdentifier) {
		this.salesOrderIdentifier = salesOrderIdentifier;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getWarehouseIdentifier() {
		return warehouseIdentifier;
	}

	public void setWarehouseIdentifier(String warehouseIdentifier) {
		this.warehouseIdentifier = warehouseIdentifier;
	}

	public String getuOMId() {
		return uOMId;
	}

	public void setuOMId(String uOMId) {
		this.uOMId = uOMId;
	}

	public String getuOMIdentifier() {
		return uOMIdentifier;
	}

	public void setuOMIdentifier(String uOMIdentifier) {
		this.uOMIdentifier = uOMIdentifier;
	}

	@Override
	public String toString() {
		return "OrderLine [lineNo=" + lineNo + "]";
	}	
}

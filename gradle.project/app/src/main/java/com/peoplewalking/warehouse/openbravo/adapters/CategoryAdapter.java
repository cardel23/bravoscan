package com.peoplewalking.warehouse.openbravo.adapters;

/**
 * Created by pwk04 on 03-21-16.
 */

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.ProductCategory;
import com.peoplewalking.warehouse.util.NovoventApp;

import java.util.List;


public class CategoryAdapter extends ArrayAdapter<ProductCategory> {

    private NovoventApp app;
    private TextView title;
    private List<ProductCategory> productCategories;

    public CategoryAdapter(Context context, List<ProductCategory> productCategories) {
        super(context, 0, productCategories);
        app = (NovoventApp) context.getApplicationContext();
        // TODO Auto-generated constructor stub
        if(productCategories.isEmpty() || !productCategories.get(0).getName().equals("") && productCategories.get(0).getName() != app.getString(R.string.select_item) ){
            ProductCategory blankOption = new ProductCategory();
            blankOption.setName(app.getString(R.string.select_item));
            productCategories.add(0, blankOption);
        }
        this.productCategories = productCategories;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(app).inflate(android.R.layout.simple_list_item_1, parent, false);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        title = (TextView) convertView.findViewById(android.R.id.text1);
        title.setText(productCategories.get(position).toString());
        title.setTextColor(Color.BLACK);
        return convertView;
    }

    public int getPosition(String item){
        int position = 0;
        try {
            for (ProductCategory s : productCategories){
                if(s.getObId() == null)
                    continue;
                else
                if(s.getIdentifier().equals(item)){
                    position = getPosition(s);
                    break;
                }
            }
            return position;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            return position;
        }
    }
}


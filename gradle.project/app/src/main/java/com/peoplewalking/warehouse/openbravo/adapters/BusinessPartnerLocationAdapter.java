package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartnerLocation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class BusinessPartnerLocationAdapter extends ArrayAdapter<BusinessPartnerLocation>{
	private Context context;
	private List<BusinessPartnerLocation> bPartnerLocationList;
	private LayoutInflater layoutInflater;
	private TextView mTextView;
	
	public BusinessPartnerLocationAdapter(Context context, List<BusinessPartnerLocation> bPartnerList){
		super(context, android.R.layout.simple_spinner_dropdown_item);
		this.context = context;
		this.bPartnerLocationList = bPartnerList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setText(bPartnerLocationList.get(position).getIdentifier());
		return view;
	}
	
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
		mTextView = (TextView) view.findViewById(android.R.id.text1);
		mTextView.setText(bPartnerLocationList.get(position).getIdentifier());
		return view;
	}
	
	@Override
	public int getCount() {
		return bPartnerLocationList == null ? 0 : bPartnerLocationList.size();
	}
	
	@Override
	public BusinessPartnerLocation getItem(int position) {
		return (bPartnerLocationList == null || bPartnerLocationList.isEmpty()) ? null : bPartnerLocationList.get(position);
	}
	
}

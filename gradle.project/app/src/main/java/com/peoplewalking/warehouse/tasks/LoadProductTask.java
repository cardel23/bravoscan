package com.peoplewalking.warehouse.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.ProductImage;
import com.peoplewalking.warehouse.util.Constants;

import retrofit.Response;

public class LoadProductTask extends TaskWithCallback<String, Void, Product>{
    	private static final String TAG = "LoadProductTask";
	public LoadProductTask(Context context) {
	    super(context);
	}
	@Override
	protected Product doInBackground(String... productBarcodes) {
		app.setLocaleConfig();
	    return executeSynchronous(productBarcodes);
	}
	
	@Override
	protected void onPostExecute(Product resultProduct) {			
		super.onPostExecute(resultProduct);
		if(this.onPostExecuteListener != null){
		    this.onPostExecuteListener.onPostExecute(resultProduct, errorMessages);
		}
	}
	public ArrayList<String> getErrorMessages() {
	    return errorMessages;
	}
	public void setErrorMessages(ArrayList<String> errorMessages) {
	    this.errorMessages = errorMessages;
	}
	
	@Override
	public Product executeSynchronous(String... productBarcodes){
		errorMessages = new ArrayList<String>();
		try {
	    	if(productBarcodes == null || productBarcodes.length == 0){
	    	    throw new Exception(app.getString(R.string.no_barcode_provided));
	    	}
	    	String productBarcode = productBarcodes[0];
	    	
	    	// Search in local DDBB
        	Product product = null;
	    	List<Product> p = Product.find(Product.class, "upcean=?", productBarcode);
	    	
            	if(p.size() > 0){
            	    product =  p.get(0);
            	} 
            	else{
				    // Save product if not exists
				    product = new Product();
				    product.setuPCEAN(productBarcode);
				}
	            	
            	// If app is connected and product is not syncronized with OB,  get product from OB
            	if(app.isConnected() && app.getObService() != null && product.getObId() == null){	
            		query = new StringBuilder("uPCEAN='"+ productBarcode +"'");
            		queryParams = new HashMap<String, String>();
            		queryParams.put("_where", query.toString());
            		queryParams.put("_selectedProperties","id,name,description,uOM,image,uPCEAN,searchKey,storageBin");
                    Response response = app.getObService().getProductList(queryParams).execute();
            		List<Product> productList = (List<Product>) response.body();
                    Log.d("FIRST RESPONSE", response.raw().request().urlString());
					if(productList == null || productList.isEmpty()){
						query = new StringBuilder("SELECT e FROM Product e LEFT JOIN " +
								"e.approvedVendorList a WHERE a.uPCEAN = '"+productBarcode+"'");
						StringBuilder builder = new StringBuilder();
						builder.append("as o where o in(")
								.append(query.toString())
								.append(")");
						queryParams.put("_where", builder.toString());
                        response = app.getObService().getProductList(queryParams).execute();
                        productList = (List<Product>) response.body();
                        Log.d("SECOND RESPONSE", response.raw().request().urlString());
						if(productList == null || productList.isEmpty()){
							query = new StringBuilder("SELECT e FROM Product e LEFT JOIN " +
									"e.approvedVendorList a WHERE a.uPCEAN = '"+productBarcode+"'");
							builder = new StringBuilder();
							builder.append("e in(")
									.append(query.toString())
									.append(")");
							queryParams.put("_where", builder.toString());
                            response = app.getObService().getProductList(queryParams).execute();
                            productList = (List<Product>) response.body();
                            Log.d("THIRD RESPONSE", response.raw().request().urlString());
						}
					}
					if(productList == null || productList.isEmpty()){
						String errorMsg= app.getString(R.string.product_not_found, productBarcode);
						throw new Exception(errorMsg);
					} else {
						product.updateFields(productList.get(0));
						ProductImage obProductImage;
						Bitmap productImageBitmap = null;

						// Try get product image from OB
						try {
							response =  app.getObService().getProductImage(product.getImageId()).execute();
							obProductImage = (ProductImage) response.body();
							productImageBitmap = obProductImage.getBitmapImage();

							//try save the image in local storage
							if(productImageBitmap != null){
								product.setRelativeLocalImagePath(app.saveImageToInternalSorage(productImageBitmap,
									Constants.PRODUCT_IMAGES_DIRNAME, product.getuPCEAN() + ".jpg"));
							}
						}
						catch (Exception e) {
							errorMessages.add(app.getString(R.string.cant_dowload_product_image));
							Log.e(TAG, e.getMessage(), e);
						}
					}
				}
        	product.save();
        	return product;

		} catch (Exception e) {
			errorMessages.add(e.getMessage());
			Log.e(TAG, e.getMessage(), e);
			return null;
		}
		
	}
}

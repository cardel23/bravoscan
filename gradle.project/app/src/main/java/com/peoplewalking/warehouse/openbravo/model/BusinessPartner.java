package com.peoplewalking.warehouse.openbravo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BusinessPartner extends OBModel {
	
	/**
     * SerialVersionUID
     */
	private static final long serialVersionUID = 1029603039626026872L;
 
	@Expose
	@SerializedName("name")
	private String name;
	@Expose
	@SerializedName("description")
	private String description;
	@Expose
	@SerializedName("searchKey")
	private String searchKey;
	@Expose
	@SerializedName("priceList")
	private String priceList;
	@Expose
	@SerializedName("purchasePricelist")
	private String purchasePricelist;
	
	public String getObId() {
		return obId;
	}
	public void setObId(String id) {
		this.obId = id;
	}
	
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	
	public String getPriceList() {
		return priceList;
	}
	public void setPriceList(String priceList) {
		this.priceList = priceList;
	}
	public String getPurchasePricelist() {
		return purchasePricelist;
	}
	public void setPurchasePricelist(String purchasePricelist) {
		this.purchasePricelist = purchasePricelist;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}
	
}

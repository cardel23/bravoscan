package com.peoplewalking.warehouse.openbravo.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class StorageBin extends OBModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3310638078083673907L;
	@Expose
	@SerializedName("searchKey")
	private String searchKey;
	@Expose
	@SerializedName("rowX")
	private String rowX;
	@Expose
	@SerializedName("stackY")
	private String stackY;
	@Expose
	@SerializedName("levelZ")
	private String levelZ;
	@Expose
	@SerializedName("warehouse")
	private String warehouseId;
	@Expose
	@SerializedName("warehouse$_identifier")
	private String warehouseIdentifier;
	
	public String getObId() {
		return obId;
	}
	public void setObId(String id) {
		this.obId = id;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getRowX() {
		return rowX;
	}
	public void setRowX(String rowX) {
		this.rowX = rowX;
	}
	public String getStackY() {
		return stackY;
	}
	public void setStackY(String stackY) {
		this.stackY = stackY;
	}
	public String getLevelZ() {
		return levelZ;
	}
	public void setLevelZ(String levelZ) {
		this.levelZ = levelZ;
	}
	public String getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getWarehouseIdentifier() {
		return warehouseIdentifier;
	}
	public void setWarehouseIdentifier(String warehouseIdentifier) {
		this.warehouseIdentifier = warehouseIdentifier;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return searchKey;
	}
}

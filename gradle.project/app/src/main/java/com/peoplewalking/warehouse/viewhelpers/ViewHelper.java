package com.peoplewalking.warehouse.viewhelpers;

import com.peoplewalking.warehouse.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class ViewHelper {
    private static final String TAG = "ViewHelper";
    protected Context context;

    protected ViewHelper(Context context) {
	this.context = context;
    }

    public Context getContext() {
	return context;
    }

    public void setContext(Context context) {
	this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void replaceView(final View viewToHide, final View view) {
	// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
	// for very easy animations. If available, use these APIs to fade-in
	// the progress spinner.
	try {
	    	if(viewToHide == null || view == null){
		   throw new Exception("view and viewToHide cannot be null");
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
		    int shortAnimTime = context.getResources().getInteger(
			    android.R.integer.config_shortAnimTime);

		    viewToHide.setVisibility(View.GONE);
		    viewToHide.animate().setDuration(shortAnimTime).alpha(0)
			    .setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
				    viewToHide.setVisibility(View.GONE);
				}
			    });

		    view.setVisibility(View.VISIBLE);
		    view.animate().setDuration(shortAnimTime).alpha(1)
			    .setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
				    view.setVisibility(View.VISIBLE);
				}
			    });
		} else {
		    // The ViewPropertyAnimator APIs are not available, so simply show
		    // and hide the relevant UI components.
		    viewToHide.setVisibility(View.GONE);
		    view.setVisibility(View.VISIBLE);
		}
	} catch (Exception e) {
	    Toast.makeText(context, context.getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
	    Log.e(TAG, e.getMessage());
	}
    }
}

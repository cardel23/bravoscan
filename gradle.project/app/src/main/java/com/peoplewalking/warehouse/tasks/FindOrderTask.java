package com.peoplewalking.warehouse.tasks;

import android.content.Context;

import com.peoplewalking.warehouse.openbravo.api.OpenbravoService;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import com.peoplewalking.warehouse.util.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by pwk04 on 10-20-15.
 */
public class FindOrderTask extends TaskWithCallback<String, Void, List<OBOrder>> {

    private Map<String, Object> filterParams;

    public FindOrderTask(Context context, Map<String, Object> filterParams) {
        super(context);
        this.filterParams = filterParams;
    }


    @Override
    protected List<OBOrder> doInBackground(String... params) {
        service = app.getObService();
        query.append("documentNo='")
                .append(params[0].toUpperCase())
                .append("'");
        if(filterParams.containsKey(Constants.FILTER_ORDER_TYPE)){
            if(filterParams.get(Constants.FILTER_ORDER_TYPE).equals(OBOrder.PURCHASE_TYPE)) {
                query.append("AND salesTransaction = false ");
            } else{
                query.append("AND salesTransaction = true ");
            }
        }
        queryParams.put("_where", query.toString());
        queryParams.put("_selectedProperties", "id,documentNo,processed,summedLineAmount,businessPartner,documentType,currency");
        List<OBOrder> order = null;
        try {
            order = service.getOrderList(queryParams).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return order;

    }
}

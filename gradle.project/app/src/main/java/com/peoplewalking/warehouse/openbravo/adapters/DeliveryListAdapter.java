package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.DeliveryList;
import com.peoplewalking.warehouse.openbravo.model.DeliveryListItem;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Adaptador para mostrar listas de entrega y manejar su comportamiento
 * @author Carlos Delgadillo
 * fecha: 30/04/2015
 */

public class DeliveryListAdapter extends ArrayAdapter<DeliveryList> {
	
	private SparseBooleanArray mSelectedItemsIds;
	List<DeliveryList> deliveryLists;
	Context context;
	private TextView title, subtitle, type;
	public DeliveryListAdapter(Context context, List<DeliveryList> deliveryLists) {
		super(context, 0, deliveryLists);
		mSelectedItemsIds = new SparseBooleanArray();
		this.deliveryLists = deliveryLists;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Context context = this.getContext().getApplicationContext();
		DeliveryList deliveryList = getItem(position);    
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_delivery_list, parent, false);
		}       
		title = (TextView) convertView.findViewById(R.id.textItem1);
		subtitle = (TextView) convertView.findViewById(R.id.textItem2);
		type = (TextView) convertView.findViewById(R.id.textItem3);
		title.setText(deliveryList.toString());
		title.setTextColor(Color.BLACK);
		title.setTextSize(20);
//		subtitle.setTextSize(16);
		long numProducts = DeliveryListItem.count(DeliveryListItem.class, "list_id = ?", new String[]{ String.valueOf(deliveryList.getId()) });
		String listType;
		if(deliveryList.getListType() == DeliveryList.INBOUND)
			listType = context.getString(R.string.inbound_list);
		else if(deliveryList.getListType() == DeliveryList.OUTBOUND)
			listType = context.getString(R.string.outbound_list);
		else
			listType = context.getString(R.string.inventory);
		subtitle.setText(listType);
		type.setText(context.getString(R.string.n_products, numProducts));
		subtitle.setTextColor(Color.GRAY);
		type.setTextColor(Color.GRAY);
		convertView
		.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x9934B5E4
		: Color.TRANSPARENT);
		return convertView;
	}
	
	public void toggleSelection(int position){
		selectView(position, !mSelectedItemsIds.get(position));
	}
	
	public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
	
	public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
 
        notifyDataSetChanged();
    }
 
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }
 
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

}

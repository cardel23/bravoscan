package com.peoplewalking.warehouse.tasks;

import java.util.List;

import retrofit.Response;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.Product;
import com.peoplewalking.warehouse.openbravo.model.User;
import com.peoplewalking.warehouse.util.Constants;
import com.peoplewalking.warehouse.util.Utils;

import android.content.Context;
import android.util.Log;

public class SyncTask extends TaskWithCallback<Void, Void, Boolean> {
	private static final String TAG = "SyncTask";
	
	public SyncTask(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public SyncTask(Context context, String loadingMessage) {
		super(context, loadingMessage);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		// TODO Auto-generated method stub
		try {
			
			User user;
			Response<List<User>> response = app.getObService().getUserList("username='"+app.getUser().getUsername()+"'").execute();
			List<User> userList = response.body();
			if(userList == null || userList.isEmpty()){
				errorMessages.add(context.getString(R.string.error_incorrect_username));
				return false;
			}else{
				user = userList.get(0);
				user.setId(app.getUser().getId());
				user.save();
				app.setUser(user);
				app.updatePreferences();
				Product.deleteAll(Product.class);
//				app.setObService(service);
//				progress.dismiss();
//				return null;
				return true;
			}
			

		} catch (Exception e) {
		    if(e.getMessage() != null){
			errorMessages.add(Utils.subString(e.getMessage(), Constants.TOAST_MESSAGE_LENGTH));
		    }else{
			errorMessages.add(context.getString(R.string.unexpected_error));
		    }
		    Log.e(TAG, e.getMessage(), e);	
		    return false;
		} 
		
	}

}

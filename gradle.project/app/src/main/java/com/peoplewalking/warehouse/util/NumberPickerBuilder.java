package com.peoplewalking.warehouse.util;

import android.view.View;
import android.widget.NumberPicker;

/**
 * Constructor de NumberPickers
 * @author Carlos Delgadillo
 * @fecha 10/06/2015
 */
public class NumberPickerBuilder {
	
	private Integer mMin, mMax, mDefault, mPicker, mPickerLayout;

    private String mMaxExternalKey, mMinExternalKey;
    
	private View view;
	
	private NumberPicker mNumberPicker;
	
	public NumberPickerBuilder() {
		// TODO Auto-generated constructor stub
	}
	
	public NumberPickerBuilder(View view){
		this.view = view;
	}
	
	/**
	 * Setea el number picker
	 * @return el number picker con las propiedades establecidas
	 * @author Carlos Delgadillo
	 * @fecha 10/06/2015
	 */
	public NumberPicker create(){
		mNumberPicker = (NumberPicker) view;
		mNumberPicker.setMaxValue(getmMax());
        mNumberPicker.setMinValue(getmMin());
        mNumberPicker.setValue(getmDefault());
        mNumberPicker.setWrapSelectorWheel(false);
        mNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
			
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

				picker.setMaxValue(getmMax());
				if (newVal <= picker.getMinValue()) {
					newVal = picker.getMinValue() + 1;
					picker.setValue(newVal);
				}
				if (newVal >= picker.getMaxValue()) {
					newVal = picker.getMaxValue() - 1;
					picker.setValue(newVal);
				}
				
			}
		});
		return mNumberPicker;
	}

	public int getmMin() {
		if(mMin == null)
			mMin = 0;
		return mMin;
	}

	public void setmMin(int mMin) {
		this.mMin = mMin;
	}

	public int getmMax() {
		if(mMax == null)
			mMax = 2147483647;
		return mMax;
	}

	public void setmMax(int mMax) {
		this.mMax = mMax;
	}

	public int getmDefault() {
		if(mDefault == null)
			mDefault = 1;
		return mDefault;
	}

	public void setmDefault(int mDefault) {
		this.mDefault = mDefault;
	}

	public int getmPicker() {
		return mPicker;
	}

	public void setmPicker(int mPicker) {
		this.mPicker = mPicker;
	}

	public int getmPickerLayout() {
		return mPickerLayout;
	}

	public void setmPickerLayout(int mPickerLayout) {
		this.mPickerLayout = mPickerLayout;
	}

	public String getmMaxExternalKey() {
		return mMaxExternalKey;
	}

	public void setmMaxExternalKey(String mMaxExternalKey) {
		this.mMaxExternalKey = mMaxExternalKey;
	}

	public String getmMinExternalKey() {
		return mMinExternalKey;
	}

	public void setmMinExternalKey(String mMinExternalKey) {
		this.mMinExternalKey = mMinExternalKey;
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public NumberPicker getmNumberPicker() {
		return mNumberPicker;
	}

	public void setmNumberPicker(NumberPicker mNumberPicker) {
		this.mNumberPicker = mNumberPicker;
	}

}

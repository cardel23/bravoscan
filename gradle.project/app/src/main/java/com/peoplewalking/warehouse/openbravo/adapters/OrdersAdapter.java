package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.Collection;
import java.util.List;
import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.OBOrder;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ViewHolder")
public class OrdersAdapter extends ArrayAdapter<OBOrder>{
    	private static final String TAG = "OrdersAdapter";
	private Context context;
	private List<OBOrder> originalOrderList;
	private LayoutInflater layoutInflater;
	private TextView mTextView1;
	
	public OrdersAdapter(Context context, List<OBOrder> orderList){
		super(context, android.R.layout.simple_spinner_dropdown_item);
		this.context = context;
		this.originalOrderList = orderList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		OBOrder currentOrder = this.originalOrderList.get(position);
		layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		if(currentOrder != null){
			mTextView1 = (TextView) view.findViewById(android.R.id.text1);
			mTextView1.setText(currentOrder.getIdentifier()); 
		}
		return view;
	}
	
	@Override
	public int getCount() {
		return originalOrderList == null ? 0 : originalOrderList.size();
	}
	
	@Override
	public OBOrder getItem(int position) {
		return (originalOrderList == null || originalOrderList.isEmpty()) ? null : originalOrderList.get(position);
	}
	
	@Override
	public void addAll(Collection<? extends OBOrder> collection) {
    		this.originalOrderList.addAll(collection);
    		notifyDataSetChanged();
	}
	
	@Override
	public void clear() {
    	 	this.originalOrderList.clear();
    	 	notifyDataSetChanged();
	}
	
	public int getItemPosition(OBOrder order){
	    try {
		   if(order != null && order!= null){	
		       OBOrder currentBP;
        		for(int position = 0, lenght = originalOrderList.size(); position < lenght; position++){
        		    currentBP = originalOrderList.get(0);
        		    if(currentBP.getObId() != null && currentBP.getObId().equals(order.getObId())){
        			return position;
        		    }
        		}
		    }
	    } catch (Exception e) {
		Toast.makeText(context, context.getString(R.string.unexpected_error), Toast.LENGTH_LONG).show();
		Log.e(TAG, e.getMessage(), e);
	    }
	    return -1;
	}
	
}

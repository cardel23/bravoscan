package com.peoplewalking.warehouse.openbravo.adapters;

import java.util.List;

import com.peoplewalking.warehouse.R;
import com.peoplewalking.warehouse.openbravo.model.BusinessPartner;
import com.peoplewalking.warehouse.openbravo.model.PriceList;
import com.peoplewalking.warehouse.util.NovoventApp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PriceListAdapter extends ArrayAdapter<PriceList> {

	private NovoventApp app;
	private TextView title;
	private List<PriceList> objects;
	
	public PriceListAdapter(Context context, List<PriceList> objects) {
		super(context, 0, objects);
		app = (NovoventApp) context.getApplicationContext();
		// TODO Auto-generated constructor stub
		try {
			if(!objects.get(0).getIdentifier().equals(null)){
				PriceList blankOption = new PriceList();
				blankOption.setName(app.getString(R.string.select_item));
				objects.add(0, blankOption);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = LayoutInflater.from(app).inflate(android.R.layout.simple_spinner_item, parent, false);
		setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		title = (TextView) convertView.findViewById(android.R.id.text1);
		title.setText(objects.get(position).toString());
		title.setTextColor(Color.BLACK);
		return convertView;
	}
	
	@Override
	public int getPosition(PriceList item) {
		// TODO Auto-generated method stub
		return super.getPosition(item);
	}
	
	public int getPositionByBusinessPartner(BusinessPartner businessPartner){
		int position = 0;
		boolean inPriceList = false;
		try {
			for(PriceList p : this.objects){
				if(businessPartner.getPurchasePricelist().equals(p.getObId())){
					inPriceList = true;
					break;
				}
				else{
					position ++;
				}
			}
			if(inPriceList)
				return position;
			else
				return 0;
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
	}

}

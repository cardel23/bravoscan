package com.peoplewalking.warehouse.interfaces;

import android.content.DialogInterface;

/**
 * Listener que retorna un valor booleano si se hace click en un botón positivo o negativo
 * @author Carlos Delgadillo
 * @fecha 09/06/2015
 */
public interface OnClickConfirmDialogListener extends DialogInterface.OnClickListener{
	
	public boolean onClick(OnClickConfirmDialogListener dialog, int which);
}

package com.peoplewalking.warehouse.tasks;

import java.util.List;

import android.content.Context;
import android.widget.Button;

import com.peoplewalking.warehouse.interfaces.OnPostExecuteListener;
import com.peoplewalking.warehouse.openbravo.model.StorageDetail;

public class StorageDetailTask extends TaskWithCallback<Void, Void, Integer> {
	OnPostExecuteListener<Integer> onPostExecuteListener;
	Button button;
	String query;
	int bookQuantity;
	
	public StorageDetailTask(Context context, String query){
		super(context);
		this.query = query;
	}
		
	public StorageDetailTask(Context context, String loadingMessage, String query) {
		super(context, loadingMessage);
		this.query = query;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Integer doInBackground(Void... params) {
		// TODO Auto-generated method stub
		return executeSynchronous();
	}
	
	@Override
	public Integer executeSynchronous(Void... params) {
		// TODO Auto-generated method stub
		List<StorageDetail> detailList;
		try {
			detailList = app.getObService().getStorageDetail(query).execute().body();
			StorageDetail sdd = detailList.get(detailList.size()-1);
			bookQuantity = sdd.getQuantityOnHand();
		} catch (Exception e) {
//			e.printStackTrace();
			bookQuantity = 0;

		}
//		if(detailList.size() > 0){
//			StorageDetail sdd = detailList.get(detailList.size()-1);
//			bookQuantity = sdd.getQuantityOnHand();
//		}
//		else
//			bookQuantity = 0;
		return bookQuantity;
	}
	
	@Override
	protected void onPostExecute(Integer response) {
		// TODO Auto-generated method stub
		super.onPostExecute(response);
		if(this.onPostExecuteListener != null)
			this.onPostExecuteListener.onPostExecute(response, null);
	}

	
	public void setOnPostExecuteListener(OnPostExecuteListener<Integer> onPostExecuteListener){
		this.onPostExecuteListener = onPostExecuteListener;
	}
};